import { Constants } from "expo";
import { Alert } from "react-native";

interface AppConfig {
    appName: string;
    appId: string;
    appKey: string;
    appServerUrl: string;
}

export default (function getConfig() {
    const manifest = Constants.manifest;
    const appConfig = manifest && manifest.extra as AppConfig;
    return appConfig;
})()