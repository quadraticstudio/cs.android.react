import * as Parse from "parse";
import * as Model from "./";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";

class Storehouse extends LocalObject {
    constructor() {
        super("Storehouse");
    }

    get number(): number {
        return this.get("number");
    }

    set number(value: number) {
        this.set("number", value);
    }

    get name(): string {
        return this.get("name");
    }

    set name(value: string) {
        this.set("name", value);
    }

    get company(): Model.Company {
        return this.get("company");
    }

    set company(value: Model.Company) {
        this.set("company", value);
    }

    get parent(): Storehouse {
        return this.get("parent");
    }

    set parent(value: Storehouse) {
        this.set("parent", value);
    }

    get resUser() {
        return this.get("resUser");
    }

    set resUser(value: Parse.User) {
        this.set("resUser", value);
    }

    get storehouses(): Storehouse[] {
        return this.get("storehouses");
    }

    set storehouses(value: Storehouse[]) {
        this.set("storehouses", value);
    }

    get isShop(): boolean {
        return this.get("isShop");
    }

    set isShop(value: boolean) {
        this.set("isShop", value);
    }

    get address(): string {
        return this.get("address");
    }

    set address(value: string) {
        this.set("address", value);
    }

    get comment(): string {
        return this.get("comment");
    }

    set comment(value: string) {
        this.set("comment", value);
    }
    get cells() {
        return this.relation<Model.Manual>('cells');
    }
}

LocalAdapter.initLocalObject(Storehouse, 'Storehouse');

export { Storehouse }