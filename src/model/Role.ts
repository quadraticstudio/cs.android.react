import * as Parse from "parse";
import * as Model from "./";
import * as collection from 'collection';
import LocalObject from "../abstract/database/lib/object";
import LocalAdapter from "../abstract/database/lib/adapter";

export enum RoleTypeEnum {
    /* Уровни доступа к данным */
    account,
    /* Привилегии на изменение и чтение */
    admin, goods, seller,
    /* Пользовательская роль */
    custom
}

export const RoleTitlesDictionary = new collection.Dictionary<RoleTypeEnum, string>(
    new collection.KeyValuePair(RoleTypeEnum.admin, "Администратор"),
    new collection.KeyValuePair(RoleTypeEnum.goods, "Товаровед"),
    new collection.KeyValuePair(RoleTypeEnum.seller, "Кассир"),
    new collection.KeyValuePair(RoleTypeEnum.custom, "Индивидуальные настройки")
);

export const RoleIconsDictionary = new collection.Dictionary<RoleTypeEnum, string>(
    new collection.KeyValuePair(RoleTypeEnum.admin, 'battery full'),
    new collection.KeyValuePair(RoleTypeEnum.goods, 'battery high'),
    new collection.KeyValuePair(RoleTypeEnum.seller, "battery low"),
    new collection.KeyValuePair(RoleTypeEnum.custom, "setting")
);

export class Role extends LocalObject {
    constructor() {
        super("_Role");
    }

    get name(): string {
        return this.get('name');
    }

    set name(value: string) {
        this.set('name', value);
    }

    get type(): RoleTypeEnum {
        return RoleTypeEnum[this.get('type') as string];
    }

    set type(value: RoleTypeEnum) {
        this.set('type', RoleTypeEnum[value]);
    }

    get title(): string {
        return this.get('title');
    }

    set title(value: string) {
        this.set('title', value);
    }

    get default(): boolean {
        return this.get('default');
    }

    set default(value: boolean) {
        this.set('default', value);
    }

    //get company() {
    //    return this.g<string>('company');
    //}

    //set company(value: string) {
    //    this.s('company', value);
    //}

    get users() {
        return this.relation<Parse.User>('users');
    }

    get partnerGroup(): Model.Group {
        return this.get("partnerGroup");
    }

    set partnerGroup(value: Model.Group) {
        this.set("partnerGroup", value);
    }

    get cashboxGroup(): Model.Group {
        return this.get("cashboxGroup");
    }

    set cashboxGroup(value: Model.Group) {
        this.set("cashboxGroup", value);
    }

    get storehouseGroup(): Model.Group {
        return this.get("storehouseGroup");
    }

    set storehouseGroup(value: Model.Group) {
        this.set("storehouseGroup", value);
    }

    get usersGroup(): Model.Group {
        return this.get("usersGroup");
    }

    set usersGroup(value: Model.Group) {
        this.set("usersGroup", value);
    }

    get roles() {
        return this.relation<Role>('roles');
    }

    // static getRoleByName(name: string): Parse.Promise<Role> {
    //     return new Parse.Query(Role).equalTo("name", name).first<Role>();
    // }

    static getRoleName(type: RoleTypeEnum, id: string): string {
        return `${RoleTypeEnum[type]}${id}`;
    }
}
LocalAdapter.initLocalObject(Role, '_Role');