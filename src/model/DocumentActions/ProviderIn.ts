import * as Model from "../";
import { DocumentAction } from "./"
import { LINQ } from "linq";
import * as collection from "collection";
import Query from "../../abstract/database/lib/query";

export default class ProviderIn extends DocumentAction {
    do = async (doc: Model.Document) => {
        let entity = doc.documentEntity as Model.Documents.ProviderInDocument;
        let storehouse = await doc.toStorehouse.fetch();
        let productStores = await new Query(Model.ProductStore).find();
        productStores = productStores.where(m => (m.store && m.store.id) == storehouse.id);
        let cellsQueryTo = await storehouse.cells.find();
        let cellDefaultTo = cellsQueryTo.firstOrNull(m => m.system === true);

        let linqManual = await new Query(Model.Manual).find();
        let linqPriceManual = linqManual.where(m => m.type === Model.ManualTypeEnum.priceTypes).orderBy(m => m.number);
        let priceSaleManual = linqPriceManual.firstOrNull(m => m.keyType === Model.ManualPriceTypesTypeEnum[Model.ManualPriceTypesTypeEnum.sale]);
        let priceProviderManual = linqPriceManual.firstOrNull(m => m.keyType === Model.ManualPriceTypesTypeEnum[Model.ManualPriceTypesTypeEnum.providerIn]);
        for (let m of entity.elements || []) {
            let product = m.product && await new Query(Model.ProductTemplate).get(m.product.objectId);
            let nowPrices = product.prices.toLinq();
            let prices = linqPriceManual.select(m => {
                let value = nowPrices.firstOrNull(k => k.key.objectId === m.id)
                let kv = new collection.KeyValuePair(m, (value && value.value) || 0);
                return kv;
            }).select(k => {
                if (k.key.id == priceSaleManual.id && k.value !== m.price)
                    k.value = m.price;
                else if (k.key.id == priceProviderManual.id && k.value !== m.priceProviderIn)
                    k.value = m.priceProviderIn;
                return k;
            });
            product.prices = collection.Dictionary.fromArray(prices.select(m => new collection.KeyValuePair({ objectId: m.key.id }, m.value)).toArray());
            product = await product.save();
            for (let k of m.cells) {
                let cell = linqManual.firstOrNull(m => m.id == k.cell.objectId);
                let productStoreToCell = await product.getProductStore(cell);
                productStoreToCell.count += k.count;
                productStoreToCell = await productStoreToCell.save();
            }
        }
        return doc;
    }
}