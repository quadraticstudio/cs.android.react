import * as Model from "../";
import { DocumentAction } from "./"
import { LINQ } from "linq";
import * as collection from "collection";

export default class CashOut extends DocumentAction {
    do = async (doc: Model.Document) => {
        let company = doc.company && await doc.company.fetch();
        if (company == null)
            throw new Error(`Can't get company`)
        company.balance = (company.balance || 0) - doc.sum;
        company = await company.save();
        return doc;
    }
}