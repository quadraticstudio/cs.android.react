import * as Model from "../";
import { DocumentAction } from "./"
import { LINQ } from "linq";
import * as collection from "collection";
import Query from "../../abstract/database/lib/query";

export default class ProviderOut extends DocumentAction {
    do = async (doc: Model.Document) => {
        let entity = doc.documentEntity as Model.Documents.ProviderOutDocument;
        let storehouse = await doc.fromStorehouse.fetch();
        let productStores = await new Query(Model.ProductStore).find();
        productStores = productStores.where(m => (m.store && m.store.id) == storehouse.id);
        let cellsQueryFrom = await storehouse.cells.find();
        cellsQueryFrom = cellsQueryFrom.orderBy(m => m.system);
        for (let m of entity.elements || []) {
            let product = m.product && await new Query(Model.ProductTemplate).get(m.product.objectId);
            let count = m.count.valueOf();
            for (let k of cellsQueryFrom.toArray()) {
                if (count > 0) {
                    let productStoreCell = await product.getProductStore(k);
                    if (k.system) {
                        productStoreCell.count -= count;
                        count = 0;
                    }
                    if (count > productStoreCell.count) {
                        productStoreCell.count = 0;
                        count -= productStoreCell.count;
                    } else {
                        productStoreCell.count -= count;
                    }
                    productStoreCell = await productStoreCell.save();
                }
            }
        }
        return doc;
    }
}