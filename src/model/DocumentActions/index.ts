import * as Model from "../";
import * as collection from "collection";

export class DocumentAction {
    do: (newDoc: Model.Document, oldDoc?: Model.Document) => Promise<Model.Document> = null;
}

import CashIn from "./CashIn";
import CashOut from "./CashOut";
import Clear from "./Clear";
import ProviderIn from "./ProviderIn";
import ProviderOut from "./ProviderOut";
import TillSlip from "./TillSlip";
import OpenSession from "./OpenSession";
import CloseSession from "./CloseSession";
import Purchase from "./Purchase";
import ReturnOfPurchase from "./ReturnOfPurchase";


export enum DocumentActionTypeEnum {
    DO
}

export class DocumentController {
    private actions = new collection.Dictionary<Model.DocumentTypeEnum, DocumentAction>();
    private static _instance: DocumentController = null;

    public static get instance() {
        if (this._instance == null)
            this._instance = new DocumentController();
        return this._instance;
    }

    constructor() {
        this.init(Model.DocumentTypeEnum.cashIn, CashIn);
        this.init(Model.DocumentTypeEnum.cashOut, CashOut);
        this.init(Model.DocumentTypeEnum.clear, Clear);
        this.init(Model.DocumentTypeEnum.providerIn, ProviderIn);
        this.init(Model.DocumentTypeEnum.providerOut, ProviderOut);
        this.init(Model.DocumentTypeEnum.tillSlip, TillSlip);
        this.init(Model.DocumentTypeEnum.sessionOpen, OpenSession);
        this.init(Model.DocumentTypeEnum.sessionClose, CloseSession);
        this.init(Model.DocumentTypeEnum.purchase, Purchase);
        this.init(Model.DocumentTypeEnum.returnOfPurchase, ReturnOfPurchase);
    }

    private init = (type: Model.DocumentTypeEnum, typeofClass: typeof DocumentAction) => {
        if (typeofClass == null)
            throw new Error(`typeofClass can't be null in DocumentController. `);
        let obj = new typeofClass();
        this.actions.add(type, obj);
    }

    public run = async (doc: Model.Document) => {
        let done = !!doc.done;
        let doneOnServer = !!doc.doneOnServer;
        console.log('RUN');
        return this.checkAndDoAction(doc, DocumentActionTypeEnum.DO);
        // if (!doneOnServer && !done)
        //     return doc;
        // if (!doneOnServer && done)
        //     return this.checkAndDoAction(doc, DocumentActionTypeEnum.DO, user);
        // if (doneOnServer && !done && existed) {
        //     return this.checkAndDoAction(doc, DocumentActionTypeEnum.UNDO, user);
        // }
        // if (doneOnServer && done && existed) {
        //     let equals = await this.equals(doc);
        //     if (equals)
        //         return doc;
        //     else {
        //         let tempDoc = await this.checkAndDoAction(doc, DocumentActionTypeEnum.UNDO, user);
        //         return this.checkAndDoAction(tempDoc, DocumentActionTypeEnum.DO, user);
        //     }
        // }
        // return doc;
    }

    //Проверка действия (для текущего документа есть ли подходящее действие) и выполнение его
    public checkAndDoAction = async (doc: Model.Document, actionType: DocumentActionTypeEnum) => {
        let type = doc.type;
        if (this.actions.containsKey(type))
            return this.action(doc, actionType);
        else
            return doc;
    }

    //Происходит действие (проведение, изменения, либо откат)
    public action = async (doc: Model.Document, action: DocumentActionTypeEnum) => {
        let type = doc.type;
        let obj = this.actions.get(type);
        if (action === DocumentActionTypeEnum.DO) {
            let oldDoc: Model.Document = null;
            console.log("DO")
            let temp = (obj.do && await obj.do(doc, oldDoc) || doc);
            temp.doneOnServer = true;
            return temp;
        }
        // else if (action === DocumentActionTypeEnum.UNDO) {
        //     if (obj.undo) {
        //         let oldDoc = (await new Parse.Query(Model.Document.name).get(doc.id, )) as Model.Document;
        //         console.log("UNDO")
        //         oldDoc = await obj.undo(oldDoc, doc);
        //     }
        //     doc.doneOnServer = false;
        //     return doc;
        // }
        else return doc;
    }
}

