import * as Model from "../";
import { DocumentAction } from "./"
import { LINQ } from "linq";
import * as collection from "collection";

export default class OpenSession extends DocumentAction {
    do = async (doc: Model.Document) => {
        doc.sessionNumber = doc.number;
        return doc;
    }
}