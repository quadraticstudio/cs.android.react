import * as Parse from "parse";
import * as Model from "./";
import { LINQ } from "linq";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";

class Shop extends LocalObject {
    constructor() {
        super("Shop");
    }

    get name(): string {
        return this.get("name");
    }

    set name(value: string) {
        this.set("name", value);
    }

    get shopstore(): Model.Storehouse {
        return this.get("shopstore");
    }

    set shopstore(value: Model.Storehouse) {
        this.set("shopstore", value);
    }

    get company(): Model.Company {
        return this.get("company");
    }

    set company(value: Model.Company) {
        this.set("company", value);
    }

    get comment() {
        return this.get('comment')
    }

    set comment(value: string) {
        this.set('comment', value)
    }

    get address() {
        return this.get('address')
    }

    set address(value: string) {
        this.set('address', value)
    }

    get sellPrefix() {
        return this.get('sellPrefix')
    }

    set sellPrefix(value: string) {
        this.set('sellPrefix', value)
    }

    get accountBalances() {
        return this.get('accountBalances')
    }

    set accountBalances(value: boolean) {
        this.set('accountBalances', value)
    }

    get goodsChoice() {
        return this.get('goodsChoice')
    }

    set goodsChoice(value: boolean) {
        this.set('goodsChoice', value)
    }

    get allowDiscount() {
        return this.get('allowDiscount')
    }

    set allowDiscount(value: boolean) {
        this.set('allowDiscount', value)
    }

    get maxDiscount() {
        return this.get('maxDiscount')
    }

    set maxDiscount(value: number) {
        this.set('maxDiscount', value)
    }

    get alwRetOnEndExch() {
        return this.get('alwRetOnEndExch')
    }

    set alwRetOnEndExch(value: boolean) {
        this.set('alwRetOnEndExch', value)
    }

    get egais() {
        return this.get('egais')
    }

    set egais(value: boolean) {
        this.set('egais', value)
    }

    get isOn() {
        return this.get('isOn')
    }

    set isOn(value: boolean) {
        this.set('isOn', value)
    }
}

LocalAdapter.initLocalObject(Shop, 'Shop');

export { Shop }