import * as Parse from "parse";
import * as Model from "./";
import * as collection from "collection";
import QueryLINQ from "parse-query-to-linq";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";
import Query from "../abstract/database/lib/query";

export enum ManualTypeEnum {
    unit, expenditure, priceTypes, nds, roundingOrder, storehouseCell,
    /* Не является MANUAL */
    cashboxes
}

export enum ManualNdsTypeEnum {
    ndsNo, nds0, nds18, nds10, nds18On118, nds10On110, custom
}

export enum ManualUnitTypeEnum {
    sh, kg, gr, litr, tonna, custom
}

export enum ManualRoundingOrderTypeEnum {
    roundingNo, rounding1, rounding5, rounding10, rounding100, custom
}
export enum ManualExpenditureTypeEnum {
    return,
    purchaseOfGoods,
    taxesAndFees,
    moving,
    rent,
    salary,
    marketing,
    custom
}

export enum ManualPriceTypesTypeEnum {
    /**
    Оптовая
    **/
    saleOpt,
    /**
    Розничная
    **/
    sale,
    /**
    Закупочная
    **/
    providerIn,
    /**
    Частная
    **/
    custom
}

export const ManualTitlesDictionary: collection.Dictionary<ManualTypeEnum, string> = new collection.Dictionary<ManualTypeEnum, string>(
    new collection.KeyValuePair(ManualTypeEnum.unit, "Единицы измерения"),
    new collection.KeyValuePair(ManualTypeEnum.expenditure, "Статьи расходов"),
    new collection.KeyValuePair(ManualTypeEnum.priceTypes, "Типы цен"),
    new collection.KeyValuePair(ManualTypeEnum.nds, "НДС"),
    new collection.KeyValuePair(ManualTypeEnum.roundingOrder, "Порядок округления"),
    new collection.KeyValuePair(ManualTypeEnum.storehouseCell, "Ячейка склада")
);

export class Manual extends LocalObject {
    constructor() {
        super("Manual");
    }

    get type(): ManualTypeEnum {
        return ManualTypeEnum[this.get("type") as string];
    }

    set type(value: ManualTypeEnum) {
        this.set("type", ManualTypeEnum[value]);
    }

    get title(): string {
        return this.get("title");
    }

    set title(value: string) {
        this.set("title", value);
    }

    get key(): string {
        return this.get("key");
    }

    set key(value: string) {
        this.set("key", value);
    }

    get keyType() {
        return this.get('keyType');
    }

    set keyType(value: string) {
        this.set('keyType', value)
    }

    get value() {
        return this.get("value");
    }

    set value(value: any) {
        this.set("value", value);
    }

    get number() {
        return this.get('number');
    }

    set number(value: number) {
        this.set('number', value)
    }

    get system() {
        return this.get('system');
    }

    set system(value: boolean) {
        this.set('system', value)
    }

    static createStorehouseCell(storehouseId: string, cell: string, man?: Manual) {
        let manual = man || new Manual();
        manual.key = storehouseId;
        manual.keyType = cell;
        manual.value = {};
        manual.type = Model.ManualTypeEnum.storehouseCell;
        manual.title = ManualTitlesDictionary.get(Model.ManualTypeEnum.storehouseCell);
        manual.system = false;
        return manual;
    }

    static getNoNds = async () => {
        if (!Manual._noNdsManual) {
            let linq = await new Query(Model.Manual).find();
            Manual._noNdsManual = linq.firstOrNull(m => m.type === Model.ManualTypeEnum.nds && m.keyType === "ndsNo");
        }
        return Manual._noNdsManual;
    }

    static getDefaultUnit = async () => {
        if (!Manual._defaultUnitManual) {
            let linq = await new Query(Model.Manual).find();
            Manual._defaultUnitManual = linq.firstOrNull(m => m.type === Model.ManualTypeEnum.unit && m.keyType === "sh");
        }
        return Manual._defaultUnitManual;
    }

    private static _noNdsManual: Model.Manual = null;
    private static _defaultUnitManual: Model.Manual = null;
}

LocalAdapter.initLocalObject(Manual, 'Manual');