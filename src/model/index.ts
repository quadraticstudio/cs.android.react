import * as Documents from "./Documents";

export * from "./Company";
export * from "./Cashbox";
export * from "./Group";
export * from "./Manual";
export * from "./Partner";
export * from "./Role";
export * from "./Shop";
export * from "./Storehouse";
export * from "./ProductStore";
export * from "./ProductTemplate";
export * from "./Document";
export * from "./Cashbox";
export * from "./Counters";
//export * from "./User";
//export * from "./OperationJournal";
export * from "./Version"
export * from "./Config"
export * from "./HashTable"
export {
    Documents
}
