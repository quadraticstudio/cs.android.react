import Database from "../abstract/database"
import * as Parse from "parse";
import * as DbBerish from "db-berish";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";
import * as Model from "./"
import Query from "../abstract/database/lib/query";

export class Config extends LocalObject {
    constructor() {
        super("Config");
    }

    get cashbox() {
        return this.get('cashbox')
    }

    set cashbox(value: Model.Cashbox) {
        this.set('cashbox', value)
    }

    get shop() {
        return this.get('shop')
    }

    set shop(value: Model.Shop) {
        this.set('shop', value)
    }

    get company() {
        return this.get('company')
    }

    set company(value: Model.Company) {
        this.set('company', value)
    }

    get shopstore() {
        return this.get('shopstore')
    }

    set shopstore(value: Model.Storehouse) {
        this.set('shopstore', value)
    }

    get firstDownloadEnded() {
        return !!this.get('firstDownloadEnded')
    }

    set firstDownloadEnded(value: boolean) {
        this.set('firstDownloadEnded', value)
    }

    get users() {
        return this.get('users')
    }

    set users(value: {
        email: string,
        name: string,
        roleName: string,
        objectId: string
    }[]) {
        this.set('users', value)
    }

    static async loadNotAuth() {
        let query = await new Query(Config).find();
        let config = query.firstOrNull() || new Config();
        return config;
    }

    static async loadAuth() {
        let config = await this.loadNotAuth();
        let condition = LocalAdapter.instance.condition;
        if (config.cashbox) {
            if (condition) {
                config.cashbox = await config.cashbox.fetchParse();
                config.shop = config.cashbox.shop && await config.cashbox.shop.fetchParse();
                config.shopstore = config.shop && config.shop.shopstore && await config.shop.shopstore.fetchParse();
                config.company = config.cashbox.company && await config.cashbox.company.fetchParse();
            } else {
                config.cashbox = await config.cashbox.fetch();
                config.shop = config.cashbox.shop && await config.cashbox.shop.fetch();
                config.shopstore = config.shop && config.shop.shopstore && await config.shop.shopstore.fetch();
                config.company = config.cashbox.company && await config.cashbox.company.fetch();
            }
        }
        return config;
    }

    /*async saveToLocal(){
        this.cashbox = this.cashbox && await this.cashbox.saveToLocal();
        this.cashbox.shop = this.cashbox.shop && await this.cashbox.shop.saveToLocal();
        this.cashbox.company = this.cashbox.company && await this.cashbox.company.saveToLocal();
        return super.saveToLocal();
    }*/
}
LocalAdapter.initLocalObject(Config, 'Config', true);