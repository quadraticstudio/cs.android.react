import * as Parse from "parse";
import * as Model from "./";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";

class ProductStore extends LocalObject {
    constructor() {
        super("ProductStore");
    }

    get store() {
        return this.get("store");
    }

    set store(value: Model.Storehouse) {
        this.set("store", value);
    }

    get product() {
        return this.get("product");
    }

    set product(value: Model.ProductTemplate) {
        this.set("product", value);
    }

    get cell() {
        return this.get('cell')
    }

    set cell(value: Model.Manual) {
        this.set('cell', value)
    }

    get count(): number {
        return this.get("count");
    }

    set count(value: number) {
        this.set("count", value);
    }

    get reserv(): number {
        return this.get("reserv");
    }

    set reserv(value: number) {
        this.set("reserv", value);
    }

    //get endPrice(): number[] {
    //    return this.g<number[]>("endPrice");
    //}

    //set endPrice(value: number[]) {
    //    this.s("endPrice", value);
    //}

    //get purchasePrice(): number[] {
    //    return this.g<number[]>("purchasePrice");
    //}

    //set purchasePrice(value: number[]) {
    //    this.s("purchasePrice", value);
    //}

    //getEndPrice() {
    //    let percent = this.product.percent + 1;
    //}
}

LocalAdapter.initLocalObject(ProductStore, 'ProductStore');

export { ProductStore }