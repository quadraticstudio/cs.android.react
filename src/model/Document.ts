import * as React from "react";
import * as Parse from "parse";
import * as Model from "./";
import * as collection from "collection";
import * as moment from "moment";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";
import * as Documents from "./Documents";
import { DocumentController } from "./DocumentActions/index";
import Query from "../abstract/database/lib/query";
import { StorageController } from "../abstract/global/GlobalStorage";

export enum DocumentTypeEnum {
    invoice,
    disposal,
    taxInvoice,
    return,
    priceChange,
    tillSlip,
    retailSalesReport,
    supplyOrder,
    providerIn,
    providerOut,
    receivedInvoice,
    cardIn,
    cashIn,
    cardOut,
    cashOut,
    inventory,
    posting,
    move,
    clear,
    sessionOpen,
    sessionClose,

    // Новые типы документов
    purchase,
    returnOfPurchase,
    correct
}

export const DocumentLabels = new collection.Dictionary<DocumentTypeEnum, string>(
    new collection.KeyValuePair(DocumentTypeEnum.invoice, "Счета покупателям"),
    new collection.KeyValuePair(DocumentTypeEnum.disposal, "Реализация"),
    new collection.KeyValuePair(DocumentTypeEnum.return, "Возвраты покупателей"),
    new collection.KeyValuePair(DocumentTypeEnum.priceChange, "Установка цен"),
    new collection.KeyValuePair(DocumentTypeEnum.tillSlip, "Чеки ККМ"),
    new collection.KeyValuePair(DocumentTypeEnum.retailSalesReport, "Отчеты о розничных продажах"),
    new collection.KeyValuePair(DocumentTypeEnum.providerIn, "Поступления"),
    new collection.KeyValuePair(DocumentTypeEnum.supplyOrder, "Заказы поставщикам"),
    new collection.KeyValuePair(DocumentTypeEnum.providerOut, "Возвраты поставщикам"),
    new collection.KeyValuePair(DocumentTypeEnum.receivedInvoice, "Счет-фактура полученный"),
    new collection.KeyValuePair(DocumentTypeEnum.cardIn, "Входящие платежи"),
    new collection.KeyValuePair(DocumentTypeEnum.cashIn, "Приходные ордеры"),
    new collection.KeyValuePair(DocumentTypeEnum.cardOut, "Исходящие платежи"),
    new collection.KeyValuePair(DocumentTypeEnum.cashOut, "Расходные ордеры"),
    new collection.KeyValuePair(DocumentTypeEnum.move, "Перемещения"),
    new collection.KeyValuePair(DocumentTypeEnum.inventory, "Инвентаризация"),
    new collection.KeyValuePair(DocumentTypeEnum.posting, "Оприходование"),
    new collection.KeyValuePair(DocumentTypeEnum.clear, "Списание"),
    new collection.KeyValuePair(DocumentTypeEnum.taxInvoice, "Счет-фактура выданный"),
    new collection.KeyValuePair(DocumentTypeEnum.sessionOpen, "Открытие смены"),
    new collection.KeyValuePair(DocumentTypeEnum.sessionClose, "Закрытие смены"),
    new collection.KeyValuePair(DocumentTypeEnum.correct, "Чек коррекции"),
    new collection.KeyValuePair(DocumentTypeEnum.purchase, "Покупка"),
    new collection.KeyValuePair(DocumentTypeEnum.returnOfPurchase, "Возврат покупки")
);

export const DocumentEntityContructor = new collection.Dictionary<DocumentTypeEnum, new (...args) => Documents.BaseDocument>(
    new collection.KeyValuePair(DocumentTypeEnum.tillSlip, Documents.TillSlipDocument),
    new collection.KeyValuePair(DocumentTypeEnum.providerIn, Documents.ProviderInDocument),
    new collection.KeyValuePair(DocumentTypeEnum.providerOut, Documents.ProviderOutDocument),
    new collection.KeyValuePair(DocumentTypeEnum.cashIn, Documents.CashInDocument),
    new collection.KeyValuePair(DocumentTypeEnum.cashOut, Documents.CashOutDocument),
    new collection.KeyValuePair(DocumentTypeEnum.inventory, Documents.InventoryDocument),
    new collection.KeyValuePair(DocumentTypeEnum.clear, Documents.ClearDocument),
    new collection.KeyValuePair(DocumentTypeEnum.correct, Documents.CorrectDocument),
    new collection.KeyValuePair(DocumentTypeEnum.purchase, Documents.PurchaseDocument),
    new collection.KeyValuePair(DocumentTypeEnum.returnOfPurchase, Documents.ReturnOfPurchaseDocument)

);

export class Document<T extends Documents.BaseDocument = Documents.BaseDocument> extends LocalObject {
    constructor(className?: string, cstr?: new () => T) {
        super(className || "Document");
        this.documentEntity = (cstr ? new cstr() : new Documents.BaseDocument()) as T;
        if (cstr) {
            let types = DocumentEntityContructor.getByValue(cstr);
            if (types && types.length > 0)
                this.type = types[0];
        };
        if (this.done == null)
            this.done = true;
    }

    //Проставляет Front
    get type() {
        return DocumentTypeEnum[this.get("type") as string];
    }

    set type(value: DocumentTypeEnum) {
        this.set("type", DocumentTypeEnum[value]);
        this.title = DocumentLabels.get(value);
    }

    get done() {
        return this.get("done");
    }

    set done(value: boolean) {
        this.set("done", value);
    }

    get sessionNumber() {
        return this.get("sessionNumber");
    }

    set sessionNumber(value: number) {
        this.set("sessionNumber", value);
    }

    get cashbox() {
        return this.get("cashbox");
    }

    set cashbox(value: Model.Cashbox) {
        this.set("cashbox", value);
    }

    get user() {
        return this.get("user") as Parse.User;
    }

    set user(value: Parse.User) {
        this.set("user", value);
    }

    get sum() {
        return this.get("sum");
    }

    set sum(value: number) {
        this.set("sum", value);
    }

    get comment() {
        return this.get("comment");
    }

    set comment(value: string) {
        this.set("comment", value);
    }

    get company() {
        return this.get("company");
    }

    set company(value: Model.Company) {
        this.set("company", value);
    }

    get partner() {
        return this.get("partner");
    }

    set partner(value: Model.Partner) {
        this.set("partner", value);
    }

    get documentEntity() {
        return this.get("documentEntity");
    }

    set documentEntity(value: T) {
        this.set("documentEntity", value);
    }

    get date(): Date {
        return this.get("date");
    }

    set date(value: Date) {
        this.set("date", value);
    }

    //Проставляет afterSave

    get title() {
        return this.get("title");
    }

    set title(value: string) {
        this.set("title", value);
    }

    get number() {
        return this.get("number");
    }

    set number(value: number) {
        this.set("number", value);
    }

    get numberCashbox() {
        return this.get("numberCashbox");
    }

    set numberCashbox(value: number) {
        this.set("numberCashbox", value);
    }

    get numberFN() {
        return this.get("numberFN");
    }

    set numberFN(value: number) {
        this.set("numberFN", value);
    }

    get fromStorehouse() {
        return this.get("fromStorehouse");
    }

    set fromStorehouse(value: Model.Storehouse) {
        this.set("fromStorehouse", value);
    }

    get toStorehouse() {
        return this.get("toStorehouse");
    }

    set toStorehouse(value: Model.Storehouse) {
        this.set("toStorehouse", value);
    }

    get doneOnServer() {
        return this.get("doneOnServer");
    }

    set doneOnServer(value: boolean) {
        this.set("doneOnServer", value);
    }
}

export async function printZReport(doc: Model.Document) {

}

export async function closeAllOpenedSessions() {
    let { company, shop } = StorageController.instance.componentArgs.global;
    if (company == null || shop == null)
        return;
    let lastOpenNumber = await Model.getOpenSessionNumber();
    for (let num of lastOpenNumber.reverse().toArray()) {
        console.log('closeSession', num);
        let doc = new Model.Document();
        doc.type = Model.DocumentTypeEnum.sessionClose;
        doc.sessionNumber = num;
        doc = await doc.save();
        await printZReport(doc);
    }
}

export async function openSession() {
    let { company, shop } = StorageController.instance.componentArgs.global;
    if (company == null || shop == null)
        return;
    let openLastNumber = await getOpenSessionNumber();
    if (openLastNumber.count() == 0) {
        let doc = new Model.Document();
        doc.type = Model.DocumentTypeEnum.sessionOpen;
        doc = await doc.save();
        console.log('openSession', doc.sessionNumber);
    }
}

export async function getOpenSessionNumber() {
    let query = await new Query(Model.Document).find();
    let currentUser = LocalAdapter.instance.currentUser;
    let queryClose = query
        .where(m => m.type == DocumentTypeEnum.sessionClose)
        .where(m => (m.user && m.user.id || m.user && m.user['objectId']) == (currentUser && currentUser.id))
        .orderByDescending(m => m.number)
        .select(m => m.sessionNumber)
    return query
        .where(m => m.type == DocumentTypeEnum.sessionOpen)
        .where(m => (m.user && m.user.id || m.user && m.user['objectId']) == (currentUser && currentUser.id))
        .orderByDescending(m => m.number)
        .select(m => m.sessionNumber)
        .where(m => !queryClose.contains(m))
}

export async function openSessionExpires() {
    let query = await new Query(Model.Document).find();
    let currentUser = LocalAdapter.instance.currentUser;
    let addHours = (date: Date, hours: number) => {
        date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
        return date;
    }
    let queryClose = query
        .where(m => m.type == DocumentTypeEnum.sessionClose)
        .where(m => (m.user && m.user.id || m.user && m.user['objectId']) == (currentUser && currentUser.id))
        .orderByDescending(m => m.number)
        .select(m => m.sessionNumber)
    query = query
        .where(m => m.type == DocumentTypeEnum.sessionOpen)
        .where(m => (m.user && m.user.id || m.user && m.user['objectId']) == (currentUser && currentUser.id))
        .orderByDescending(m => m.number)
        .where(m => !queryClose.contains(m.sessionNumber))
        .where(m => addHours(new Date(m.date || new Date()), 24) <= new Date());
    if (query.count() > 0)
        return true;
    return false;
}

LocalAdapter.initLocalObject(Document, 'Document');

LocalObject.beforeSave('Document', async req => {
    if (req.typeTrigger == 'local') {
        let doc = req.object as Model.Document;
        let nowDate = new Date();
        let existed = !!doc.id;
        doc.user = doc.user || LocalAdapter.instance.currentUser;
        //doc.user = doc.user && await new Parse.Query(Model.User).get(doc.user.id || doc.user["objectId"], { useMasterKey: true });
        doc.date = doc.date || nowDate;
        doc.done = !!doc.done;
        doc.doneOnServer = true;
        doc.company = StorageController.instance.componentArgs.global.company;
        doc.cashbox = StorageController.instance.componentArgs.global.cashbox;
        if (doc.type != Model.DocumentTypeEnum.sessionOpen && doc.type != Model.DocumentTypeEnum.sessionClose) {
            let open = await getOpenSessionNumber();
            doc.sessionNumber = open.firstOrNull();
        }
        /*if (!doc.user)
            throw new Error(`Can't read property user: undefined.`)*/
        doc.number = doc.number || await Model.Counters.nextNumber(Model.CounterTypeEnum.Document, `${doc.company.id}${doc.cashbox && doc.cashbox.id || ''}`, Model.DocumentTypeEnum[doc.type]);
        doc.title = doc.title || Model.DocumentLabels.get(doc.type);
        doc = await DocumentController.instance.run(doc);
    }
})