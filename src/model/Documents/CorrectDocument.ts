import * as collection from "collection";
import { BaseDocument } from "./";
import * as Model from "../";

export enum CorrectTypeEnum {
    income, outcome
}

export const CorrectTypeLabels = new collection.Dictionary<CorrectTypeEnum, string>(
    new collection.KeyValuePair(CorrectTypeEnum.income, 'Приход денежных средств'),
    new collection.KeyValuePair(CorrectTypeEnum.outcome, 'Расход денежных средств')
);

export enum CorrectBaseTypeEnum {
    str, act, return, report, cashIn, cashOut, disposal, tillSlip, tillSlipReturn, bank
}

export const CorrectBaseTypeLabels = new collection.Dictionary<CorrectBaseTypeEnum, string>(
    new collection.KeyValuePair(CorrectBaseTypeEnum.str, 'Строка'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.act, 'Акт выполненных работ'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.return, 'Возврат товаров от клиента'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.report, 'Отчет о розничных продажах'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.cashIn, 'Приходный кассовый ордер'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.cashOut, 'Расходный кассовый ордер'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.disposal, 'Реализация товаров и услуг'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.tillSlip, 'Чек ККМ'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.tillSlipReturn, 'Чек ККМ на возврат'),
    new collection.KeyValuePair(CorrectBaseTypeEnum.bank, 'Эквайринговая операция')
);

export class CorrectDocument extends BaseDocument {
    type: string;
    base: string;
    cashType: string;
    numberTillSlip: number;
}