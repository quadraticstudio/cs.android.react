export class BaseDocument {

}

export * from "./TillSlipDocument";
export * from "./ProviderOutDocument";
export * from "./ProviderInDocument";
export * from "./CashInDocument";
export * from "./CashOutDocument";
export * from "./InventoryDocument";
export * from "./ClearDocument";
export * from "./CorrectDocument";
export * from "./PurchaseDocument";
export * from "./ReturnOfPurchaseDocument";