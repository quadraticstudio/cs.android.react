import { BaseDocument } from "./";
import * as Model from "../";

export class ReturnOfPurchaseDocument extends BaseDocument {
    sessionNumber: number;
    //Model.Shop
    shop: { objectId: string };
    sumCash: number;
    sumCard: number;
    saleType: string;
    nds: number;
    useNds: boolean;
    elements: ReturnOfPurchaseDocumentElement[];
    email: string;
    phone: string;
}

export class ReturnOfPurchaseDocumentElement {
    //Model.ProductTemplate
    product: { objectId: string };
    count: number;
    countStore: number;
    price: number;
    //Model.Manual
    ndsKey: { objectId: string };
    ndsValue: number;
    discount: number;
    sum: number;
}