import { BaseDocument } from "./";
import * as Model from "../";

export class PurchaseDocument extends BaseDocument {
    sessionNumber: number;
    //Model.Shop
    shop: { objectId: string };
    sumCash: number;
    sumCard: number;
    saleType: string;
    nds: number;
    useNds: boolean;
    elements: PurchaseDocumentElement[];
    email: string;
    phone: string;
}

export class PurchaseDocumentElement {
    //Model.ProductTemplate
    product: { objectId: string };
    count: number;
    countStore: number;
    price: number;
    //Model.Manual
    ndsKey: { objectId: string };
    ndsValue: number;
    discount: number;
    sum: number;
}