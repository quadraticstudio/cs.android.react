import { BaseDocument } from "./";
import * as Model from "../";

export class CashInDocument extends BaseDocument {
    nds: number;
    description: string;
}