import { BaseDocument } from "./";
import * as Model from "../";

export class ProviderOutDocument extends BaseDocument {
    nds: number;
    elements: ProviderOutDocumentElement[];
}

export class ProviderOutDocumentElement {
    //Model.ProductTemplate
    product: { objectId: string };
    count: number;
    countStore: number;
    price: number;
    //Model.Manual
    ndsKey: { objectId: string };
    ndsValue: number;
    sum: number;
}