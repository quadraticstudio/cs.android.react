import { BaseDocument } from "./";
import * as Model from "../";

export class TillSlipDocument extends BaseDocument {
    type: string;
    typeTitle: string;
    sessionNumber: number;
    //Model.Shop
    shop: { objectId: string };
    sumCash: number;
    sumCard: number;
    saleType: string;
    nds: number;
    useNds: boolean;
    elements: TillSlipDocumentElement[];
    email: string;
    phone: string;
}

export class TillSlipDocumentElement {
    //Model.ProductTemplate
    product: { objectId: string };
    count: number;
    countStore: number;
    price: number;
    //Model.Manual
    ndsKey: { objectId: string };
    ndsValue: number;
    discount: number;
    sum: number;
}