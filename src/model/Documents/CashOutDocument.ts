import { BaseDocument } from "./";
import * as Model from "../";

export class CashOutDocument extends BaseDocument {
    nds: number;
    description: string;
    //Model.Manual
    expenditureType: { objectId: string };
}