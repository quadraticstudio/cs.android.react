import { BaseDocument } from "./";
import * as Model from "../";

export class ClearDocument extends BaseDocument {
    elements: ClearDocumentElement[];
}

export class ClearDocumentElement {
    //ProductTemplate
    product: { objectId: string };
    count: number;
    countStore: number;
    price: number;
    sum: number;
    comment: string;
}