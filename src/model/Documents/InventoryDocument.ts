import { BaseDocument } from "./";
import * as Model from "../";

export class InventoryDocument extends BaseDocument {
    excess: number;
    shortage: number;
    actualBalance: number;
    accountingBalance: number;
    elements: InventoryDocumentElement[];
}

export class InventoryDocumentElement {
    product: { objectId: string };
    countStore: number;
    count: number;
    countDifference: number;
    price: number;
    sum: number;
}