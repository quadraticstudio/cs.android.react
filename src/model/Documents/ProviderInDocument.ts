import { BaseDocument } from "./";
import * as Model from "../";

export class ProviderInDocument extends BaseDocument {
    // Model.Manual
    priceType: { objectId: string };
    number: number;
    date: Date;
    sumProviderIn: number;
    nds: number;
    useNds: boolean;
    consumption: number;
    elements: ProviderInDocumentElement[];
}

export class ProviderInDocumentElement {
    //Model.ProductTemplate
    product: { objectId: string };
    count: number;
    cells: ProviderInDocumentCell[];
    countStore: number;
    price: number;
    priceProviderIn: number;
    //Model.Manual
    ndsKey: { objectId: string };
    ndsValue: number;
    sum: number;
}

export class ProviderInDocumentCell {
    //Model.Manual
    cell: { objectId: string };
    count: number;
}