import * as React from "react";
import * as Parse from "parse";
import * as Model from "./";
import * as collection from "collection";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";
import Query from "../abstract/database/lib/query";

export enum CounterTypeEnum {
    Document, Partner, Storehouse, Barcode, Session, Transaction, ProductTemplate
}

export class Counters extends LocalObject {
    constructor() {
        super("Counters");
    }

    get name() {
        return this.get('name');
    }

    set name(value: string) {
        this.set('name', value);
    }

    get value() {
        return this.get('value');
    }

    set value(value: number) {
        this.set('value', value);
    }

    static async number(type: CounterTypeEnum, objectId: string): Promise<number>;
    static async number(type: CounterTypeEnum, objectId: string, postfix: string): Promise<number>;
    static async number(type: CounterTypeEnum, objectId: string, postfix?: string): Promise<number> {
        if (!postfix)
            postfix = "";
        let name = `${CounterTypeEnum[type]}${objectId}${postfix}`;
        let counters = await new Query(Counters).find();
        let counter = counters.firstOrNull(m => m.name == name);
        if (!counter) {
            counter = new Counters();
            counter.name = name;
            counter.value = 0;
            counter = await counter.save();
        }
        return counter.value;
    }

    static async setNumber(type: CounterTypeEnum, objectId: string, value: number): Promise<number>;
    static async setNumber(type: CounterTypeEnum, objectId: string, value: number, postfix: string): Promise<number>;
    static async setNumber(type: CounterTypeEnum, objectId: string, value: number, postfix?: string): Promise<number> {
        if (!postfix)
            postfix = "";
        let name = `${CounterTypeEnum[type]}${objectId}${postfix}`;
        let counters = await new Query(Counters).find();
        let counter = counters.firstOrNull(m => m.name == name);
        if (!counter) {
            counter = new Counters();
            counter.name = name;
            counter.value = 0;
        }
        counter.value = value;
        counter = await counter.save();
        return counter.value;
    }

    static async nextNumber(type: CounterTypeEnum, objectId: string): Promise<number>;
    static async nextNumber(type: CounterTypeEnum, objectId: string, postfix: string): Promise<number>;
    static async nextNumber(type: CounterTypeEnum, objectId: string, postfix?: string): Promise<number> {
        if (!postfix)
            postfix = "";
        let name = `${CounterTypeEnum[type]}${objectId}${postfix}`;
        let counters = await new Query(Counters).find();
        let counter = counters.firstOrNull(m => m.name == name);
        if (!counter) {
            counter = new Counters();
            counter.name = name;
            counter.value = 0;
        }
        let value = counter.value;
        counter.value += 1;
        counter = await counter.save();
        return counter.value;
    }
}