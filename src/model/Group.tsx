import * as Parse from "parse";
import * as Model from "./";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";

export enum GroupItemTypeEnum {
    group, product, partner, storehouse, cashbox, user
}

class Group extends LocalObject {

    constructor() {
        super("Group");
    }

    get name(): string {
        return this.get("name");
    }

    set name(value: string) {
        this.set("name", value);
    }

    get nds() {
        return this.get("nds");
    }

    set nds(value: Model.Manual) {
        this.set("nds", value);
    }

    get markup(): number {
        return this.get("markup");
    }

    set markup(value: number) {
        this.set("markup", value);
    }

    //Relation на Group
    get childs() {
        return this.relation<Model.Group>("childs");
    }

    get type(): GroupItemTypeEnum {
        return GroupItemTypeEnum[this.get("type") as string];
    }

    set type(value: GroupItemTypeEnum) {
        this.set("type", GroupItemTypeEnum[value]);
    }

    get pointer(): Model.ProductTemplate {
        return this.get("pointer");
    }

    set pointer(value: Model.ProductTemplate) {
        this.set("pointer", value);
    }

    get partner(): Model.Partner {
        return this.get("partner");
    }

    set partner(value: Model.Partner) {
        this.set("partner", value);
    }

    get cashbox() {
        return this.get('cashbox');
    }

    set cashbox(value: Model.Cashbox) {
        this.set('cashbox', value)
    }

    get storehouse() {
        return this.get('storehouse')
    }

    set storehouse(value: Model.Storehouse) {
        this.set('storehouse', value)
    }

    get comment() {
        return this.get('comment');
    }

    set comment(value: string) {
        this.set('comment', value)
    }

    get user() {
        return this.get('user')
    }

    set user(value: Parse.User) {
        this.set('user', value)
    }

    get parent() {
        return this.get('parent')
    }

    set parent(value: Model.Group) {
        this.set('parent', value)
    }
}

LocalAdapter.initLocalObject(Group, 'Group');
LocalObject.afterSave('Group', async req => {
    if (req.typeTrigger == 'local') {
        let group = req.object as Group;
        let childs = await group.childs.find();
        for (let m of childs.where(m => (m.parent && m.parent.id) != group.id).toArray()) {
            m.parent = group;
            m = await m.save();
        }
    }
})
export { Group }