import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";

export class HashTable extends LocalObject {

    constructor() {
        super("HashTable");
    }

    get refClassName() {
        return this.get('refClassName');
    }

    set refClassName(value: string) {
        this.set('refClassName', value);
    }

    get refId() {
        return this.get('refId');
    }

    set refId(value: string) {
        this.set('refId', value);
    }

    get hash() {
        return this.get('hash');
    }

    set hash(value: string) {
        this.set('hash', value);
    }
}

LocalAdapter.initLocalObject(HashTable, 'HashTable');