import * as Parse from "parse";
import * as DbBerish from "db-berish";
import * as Model from "./";
import * as React from "react";
import * as collection from "collection";
import { LINQ } from "linq";
import QueryLINQ from "parse-query-to-linq";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";
import Query from "../abstract/database/lib/query";
import { toLocal } from "../abstract/database/lib/converter";
import { Alert } from "react-native";

export enum ProductTypeEnum {
    product, service, set
}

export const ProductTitlesDictionary = new collection.Dictionary<ProductTypeEnum, string>(
    new collection.KeyValuePair(ProductTypeEnum.product, 'Товар'),
    new collection.KeyValuePair(ProductTypeEnum.service, 'Услуга'),
    new collection.KeyValuePair(ProductTypeEnum.set, 'Комплект')
);

class ProductTemplate extends LocalObject {
    constructor() {
        super("ProductTemplate");
        this.barcodes = [];
        this.type = ProductTypeEnum.product;
        this.products = [];
        this.needToDoPriceDocument = false;
    }
    fetched: boolean = false;

    get type() {
        return ProductTypeEnum[this.get("type") as string];
    }

    set type(value: ProductTypeEnum) {
        this.set("type", ProductTypeEnum[value]);
    }

    get number(): number {
        return this.get("number");
    }

    set number(value: number) {
        this.set("number", value);
    }

    get name(): string {
        return this.get("name");
    }

    set name(value: string) {
        this.set("name", value);
    }

    get vendorCode(): string {
        return this.get("vendorCode");
    }

    set vendorCode(value: string) {
        this.set("vendorCode", value);
    }

    get unit() {
        return this.get("unit");
    }

    set unit(value: Model.Manual) {
        this.set("unit", value);
    }

    get nds() {
        return this.get('nds');
    }

    set nds(value: Model.Manual) {
        this.set('nds', value);
    }

    get markup() {
        return this.get('markup');
    }

    set markup(value: number) {
        this.set('markup', value);
    }

    get comment() {
        return this.get('comment');
    }

    set comment(value: string) {
        this.set('comment', value);
    }

    get images() {
        return this.get('images');
    }

    set images(value: Parse.File[]) {
        this.set('images', value);
    }

    get minPrice() {
        return this.get('minPrice');
    }

    set minPrice(value: number) {
        this.set('minPrice', value);
    }

    get prices() {
        return collection.Dictionary.fromArray(this.get('prices') || []);
    }

    set prices(value: collection.Dictionary<{ objectId: string }, number>) {
        this.set('prices', value.asArray())
    }

    get barcodes() {
        return this.get('barcodes');
    }

    set barcodes(value: string[]) {
        this.set('barcodes', value);
    }

    get products() {
        return this.get('products');
    }

    set products(value: collection.KeyValuePair<ProductTemplate, number>[]) {
        this.set('products', value)
    }

    get partner() {
        return this.get('partner');
    }

    set partner(value: Model.Partner) {
        this.set('partner', value);
    }

    get isWeight(): boolean {
        return this.get("isWeight");
    }

    set isWeight(value: boolean) {
        this.set("isWeight", value);
    }

    get minBalance() {
        return this.get('minBalance');
    }

    set minBalance(value: number) {
        this.set('minBalance', value);
    }

    get minLot() {
        return this.get('minLot');
    }

    set minLot(value: number) {
        this.set('minLot', value);
    }

    get needToDoPriceDocument() {
        return this.get('needToDoPriceDocument');
    }

    set needToDoPriceDocument(value: boolean) {
        this.set('needToDoPriceDocument', value);
    }

    get isAlcohol(): boolean {
        return this.get("isAlcohol");
    }

    set isAlcohol(value: boolean) {
        this.set("isAlcohol", value);
    }

    get groupPath() {
        return this.get('groupPath');
    }

    set groupPath(value: string) {
        this.set('groupPath', value);
    }

    get company() {
        return this.get('company')
    }

    set company(value: Model.Company) {
        this.set('company', value)
    }

    get fullName() {
        let name = this.name || "-";
        let type = Model.ProductTypeEnum[Model.ProductTypeEnum[this.type] || "product"];
        let typeName = type == ProductTypeEnum.service
            ? "Услуга"
            : type == ProductTypeEnum.set
                ? "Комплект"
                : "Товар";
        return `${typeName} ${name}`;
    }

    getAllCount = async (store?: Model.Storehouse) => {
        let query = await new Query(Model.ProductStore).find();
        query = query.where(m => (m.product && m.product.id) === (this.id));
        if (store != null)
            query = query.where(m => (m.store && m.store.id) == (store && store.id));
        return query.sum(m => m.count || 0);
    }


    getAllReserv = async (store?: Model.Storehouse) => {
        let query = await new Query(Model.ProductStore).find();
        query = query.where(m => (m.product && m.product.id) === (this.id));
        if (store != null)
            query = query.where(m => (m.store && m.store.id) == (store && store.id));
        return query.sum(m => m.reserv || 0);
    }

    private getPrices = async (type: string) => {
        let prices = this.prices;
        let linq: LINQ<Model.Manual> = null;
        if (!(ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.keyType === type)) {
            linq = await new Query(Model.Manual).find();
            ProductTemplate._purchasePrice = linq.firstOrNull(m => m.type === Model.ManualTypeEnum.priceTypes && m.keyType === type);
        }
        let pricesKvs = await Promise.all(prices.toLinq().select(async m => {
            if (linq == null)
                linq = await new Query(Model.Manual).find();
            let manual = m.key && linq.firstOrNull(k => k.id == m.key.objectId);
            return new collection.KeyValuePair(manual, m.value || 0);
        }).toArray());
        let priceValue = LINQ.fromArray(pricesKvs).firstOrNull(m => m.key.id === (ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.id));
        return priceValue;
    }

    getPurchasePrice = async (isOpt?: string) => {
        let type = isOpt ? "saleOpt" : "sale";
        /*if (!(ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.keyType === type)) {
            let linq = await new Query(Model.Manual).find();
            ProductTemplate._purchasePrice = linq.firstOrNull(m => m.type === Model.ManualTypeEnum.priceTypes && m.keyType === type);
        }
        this.prices = await Promise.all(LINQ.fromArray(this.prices || []).select(async m => {
            m.key = m.key && await m.key.fetch();
            return m;
        }).toArray());
        let priceValue = LINQ.fromArray(this.prices).firstOrNull(m => m.key.id === (ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.id));
        return (priceValue && priceValue.value) || 0;*/
        let price = await this.getPrices(type);
        return (price && price.value) || 0;
    }

    getProviderPrice = async () => {
        let type = "providerIn";
        // if (!(ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.keyType === type)) {
        //     let linq = await new Query(Model.Manual).find();
        //     ProductTemplate._purchasePrice = linq.firstOrNull(m => m.type === Model.ManualTypeEnum.priceTypes && m.keyType === type);
        // }
        // this.prices = await Promise.all(LINQ.fromArray(this.prices || []).select(async m => {
        //     m.key = m.key && await m.key.fetch();
        //     return m;
        // }).toArray());
        // let priceValue = LINQ.fromArray(this.prices).firstOrNull(m => m.key.id === (ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.id));
        // return (priceValue && priceValue.value) || 0;
        let price = await this.getPrices(type);
        return (price && price.value) || 0;
    }

    setProviderPrice = async (value: number) => {
        let type = "providerIn";
        /*if (!(ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.keyType === type)) {
            let linq = await new Query(Model.Manual).find();
            ProductTemplate._purchasePrice = linq.firstOrNull(m => m.type === Model.ManualTypeEnum.priceTypes && m.keyType === type);
        }
        this.prices = await Promise.all(LINQ.fromArray(this.prices || []).select(async m => {
            m.key = m.key && await m.key.fetch();
            return m;
        }).toArray());
        let priceValue = LINQ.fromArray(this.prices).firstOrNull(m => m.key.id === (ProductTemplate._purchasePrice && ProductTemplate._purchasePrice.id));*/
        let price = await this.getPrices(type);
        if (price)
            price.value = value;
    }

    getProductStore = async (cell: Model.Manual) => {
        cell = cell && await new Query(Model.Manual).get(cell.id || cell['objectId']);
        if (cell.type !== Model.ManualTypeEnum.storehouseCell)
            throw new Error('Manual is not a StorehouseCell');
        let queryProductStores = await new Query(Model.ProductStore).find();
        let productStoreCell = queryProductStores.firstOrNull(m => ((m.product && m.product.id) == this.id) && ((m.cell && m.cell.id) == cell.id));
        if (productStoreCell != null)
            return productStoreCell;
        let store = await new Query(Model.Storehouse).get(cell.key);
        productStoreCell = new Model.ProductStore();
        productStoreCell.product = this;
        productStoreCell.cell = cell;
        productStoreCell.store = store;
        productStoreCell.count = 0;
        productStoreCell = await productStoreCell.save();
        return productStoreCell;
    }

    private static _purchasePrice: Model.Manual = null;

    static generateBarcode = async () => {
        return `${(await Parse.Cloud.run("barcode:generate")).number as number}`;
    }
}

LocalAdapter.initLocalObject(ProductTemplate, 'ProductTemplate');

export { ProductTemplate }