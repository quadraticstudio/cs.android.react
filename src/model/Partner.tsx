import { Company } from './Company';
import * as Parse from "parse";
import * as React from "react"
import * as Model from "./";
import * as collection from "collection";
import LocalAdapter from '../abstract/database/lib/adapter';
import LocalObject from '../abstract/database/lib/object';

export enum PartnerTypeEnum {
    IP, UL, FL
}

export const PartnerTitlesDictionary = new collection.Dictionary<PartnerTypeEnum, string>(
    new collection.KeyValuePair(PartnerTypeEnum.UL, 'ООО'),
    new collection.KeyValuePair(PartnerTypeEnum.IP, 'ИП'),
    new collection.KeyValuePair(PartnerTypeEnum.FL, 'Физическое лицо')
);

export const PartnerIconsDictionary = new collection.Dictionary<PartnerTypeEnum, string>(
    new collection.KeyValuePair(PartnerTypeEnum.UL, "building"),
    new collection.KeyValuePair(PartnerTypeEnum.IP, "fax"),
    new collection.KeyValuePair(PartnerTypeEnum.FL, "user")
);

class Partner extends LocalObject {

    constructor() {
        super("Partner");
        this.rs = [];
        this.contacts = [];
        this.type = PartnerTypeEnum.UL;
    }

    get number(): number {
        return this.get('number');
    }

    set number(value: number) {
        this.set('number', value);
    }

    get name(): string {
        return this.get('name');
    }

    set name(value: string) {
        this.set('name', value);
    }

    get type() {
        return PartnerTypeEnum[this.get('type') as string];
    }

    set type(value: PartnerTypeEnum) {
        this.set('type', PartnerTypeEnum[value]);
    }

    get flid(): string {
        return this.get('flid');
    }

    set flid(value: string) {
        this.set('flid', value);
    }

    get inn(): string {
        return this.get('inn');
    }

    set inn(value: string) {
        this.set('inn', value);
    }

    get kpp(): string {
        return this.get('kpp');
    }

    set kpp(value: string) {
        this.set('kpp', value);
    }

    get ogrn(): string {
        return this.get('ogrn');
    }

    set ogrn(value: string) {
        this.set('ogrn', value);
    }

    get okpo(): string {
        return this.get('okpo');
    }

    set okpo(value: string) {
        this.set('okpo', value);
    }

    get faddress(): string {
        return this.get('faddress');
    }

    set faddress(value: string) {
        this.set('faddress', value);
    }

    get uaddress(): string {
        return this.get('uaddress');
    }

    set uaddress(value: string) {
        this.set('uaddress', value);
    }


    get uAddressEqual() {
        return this.get('uAddressEqual')
    }

    set uAddressEqual(value: boolean) {
        this.set("uAddressEqual", value)
    }

    get phone(): string {
        return this.get('phone');
    }

    set phone(value: string) {
        this.set('phone', value);
    }

    get email(): string {
        return this.get('email');
    }

    set email(value: string) {
        this.set('email', value);
    }

    get head(): string {
        return this.get('head');
    }

    set head(value: string) {
        this.set('head', value);
    }

    get accountant(): string {
        return this.get('accountant');
    }

    set accountant(value: string) {
        this.set('accountant', value);
    }

    get contacts(): PartnerContact[] {
        return this.get('contacts');
    }

    set contacts(value: PartnerContact[]) {
        this.set('contacts', value);
    }

    get rs(): Model.CompanyRS[] {
        return this.get('rs');
    }

    set rs(value: Model.CompanyRS[]) {
        this.set('rs', value);
    }

    get accountRole(): Model.Role {
        return this.get('accountRole');
    }

    set accountRole(value: Model.Role) {
        this.set('accountRole', value);
    }

    get sex(): string {
        return this.get('sex');
    }

    set sex(value: string) {
        this.set('sex', value);
    }

    get comment() {
        return this.get('comment')
    }

    set comment(value: string) {
        this.set('comment', value)
    }

    get flDate() {
        return this.get('flDate')
    }

    set flDate(value: Date) {
        this.set('flDate', value)
    }
}

export class PartnerContact {
    name: string;
    phone: string;
    email: string;
    comment: string;
}

LocalAdapter.initLocalObject(Partner, 'Partner');

export { Partner }