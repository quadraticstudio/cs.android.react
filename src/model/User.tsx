// import * as Model from "./"
// import LocalAdapter from "../abstract/database/lib/adapter";
// import LocalObject from "../abstract/database/lib/object";

// export class User extends LocalObject {
//     constructor() {
//         super("_User");
//     }

//     get name(): string {
//         return this.get('name');
//     }

//     set name(value: string) {
//         this.set('name', value);
//     }

//     get username(): string {
//         return this.get('username');
//     }

//     set username(value: string) {
//         this.set('username', value);
//     }

//     get email(): string {
//         return this.get('email') || this.get('mail');
//     }

//     set email(value: string) {
//         this.set('email', value);
//         this.set('mail', value);
//     }

//     get emailVerified(): boolean {
//         return this.get('emailVerified');
//     }

//     set emailVerified(value: boolean) {
//         this.set('emailVerified', value);
//     }

//     get accountRole(): Parse.Role {
//         return this.get('accountRole');
//     }

//     set accountRole(value: Parse.Role) {
//         this.set('accountRole', value);
//     }

//     get access(): boolean {
//         return this.get('access');
//     }

//     set access(value: boolean) {
//         this.set('access', value);
//     }

//     get role(): Parse.Role {
//         return this.get("role");
//     }

//     set role(value: Parse.Role) {
//         this.set("role", value);
//     }

//     get company(): Model.Company {
//         return this.get("company");
//     }

//     set company(value: Model.Company) {
//         this.set("company", value);
//     }

//     get comment(): string {
//         return this.get("comment");
//     }

//     set comment(value: string) {
//         this.set("comment", value);
//     }

//     get partnerGroup(): Model.Group {
//         return this.get("partnerGroup");
//     }

//     set partnerGroup(value: Model.Group) {
//         this.set("partnerGroup", value);
//     }

//     get companies() {
//         return this.get('companies')
//     }

//     set companies(value: Model.Company[]) {
//         this.set('companies', value)
//     }

//     get phone() {
//         return this.get('phone')
//     }

//     set phone(value: string) {
//         this.set('phone', value)
//     }
// }
// LocalAdapter.initLocalObject(User, "_User");