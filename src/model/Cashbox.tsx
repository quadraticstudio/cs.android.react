import * as Parse from "parse";
import * as Model from "./";
import LocalObject from "../abstract/database/lib/object";
import LocalAdapter from "../abstract/database/lib/adapter";

export class Cashbox extends LocalObject {
    constructor() {
        super("Cashbox");
    }

    get shop(): Model.Shop {
        return this.get("shop");
    }

    set shop(value: Model.Shop) {
        this.set("shop", value);
    }

    get company(): Model.Company {
        return this.get("company");
    }

    set company(value: Model.Company) {
        this.set("company", value);
    }

    get serialNumber(): string {
        return this.get("serialNumber");
    }

    set serialNumber(value: string) {
        this.set("serialNumber", value);
    }

    get user() {
        return this.get("user");
    }

    set user(value: Parse.User) {
        this.set("user", value);
    }
}
LocalAdapter.initLocalObject(Cashbox, 'Cashbox');
//Parse.Object.registerSubclass('Cashbox', Cashbox)