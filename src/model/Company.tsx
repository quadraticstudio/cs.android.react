import * as React from "react";
import * as Parse from "parse";
import * as Model from "./";
import * as collection from "collection";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";

export enum CompanyTypeEnum {
    IP, UL
}

export const CompanyTitlesDictionary = new collection.Dictionary<CompanyTypeEnum, string>(
    new collection.KeyValuePair(CompanyTypeEnum.UL, 'ООО'),
    new collection.KeyValuePair(CompanyTypeEnum.IP, 'ИП')
);

export const CompanyIconsDictionary = new collection.Dictionary<CompanyTypeEnum, string>(
    new collection.KeyValuePair(CompanyTypeEnum.UL, "building"),
    new collection.KeyValuePair(CompanyTypeEnum.IP, "fax")
);

export class Company extends LocalObject {

    constructor() {
        super("Company");
        this.rs = [];
    }

    get name() {
        return this.get('name');
    }

    set name(value: string) {
        this.set('name', value);
    }

    get balance() {
        return this.get('balance');
    }

    set balance(value: number) {
        this.set('balance', value);
    }

    get phone() {
        return this.get("phone");
    }

    set phone(value: string) {
        this.set('phone', value);
    }

    get clickedDate(){
        return this.get('clickedDate')
    }

    set clickedDate(value: Date){
        this.set('clickedDate', value)
    }

    get ndsPayer() {
        return this.get("ndsPayer");
    }

    set ndsPayer(value: boolean) {
        this.set('ndsPayer', value);
    }

    get asHead() {
        return this.get('asHead');
    }

    set asHead(value: string) {
        this.set('asHead', value);
    }

    get email() {
        return this.get('email');
    }

    set email(value: string) {
        this.set('email', value);
    }

    get fax() {
        return this.get('fax');
    }

    set fax(value: string) {
        this.set('fax', value);
    }

    get fAddress() {
        return this.get('fAddress');
    }

    set fAddress(value: string) {
        this.set('fAddress', value);
    }

    get uAddress() {
        return this.get('uAddress');
    }

    set uAddress(value: string) {
        this.set('uAddress', value);
    }

    get type() {
        return CompanyTypeEnum[this.get('type') as string];
    }

    set type(value: CompanyTypeEnum) {
        this.set('type', CompanyTypeEnum[value]);
    }

    get head() {
        return this.get('head');
    }

    set head(value: string) {
        this.set('head', value);
    }

    get accountant() {
        return this.get('accountant');
    }

    set accountant(value: string) {
        this.set('accountant', value);
    }

    get uAddressEqual() {
        return this.get('uAddressEqual')
    }

    set uAddressEqual(value: boolean) {
        this.set("uAddressEqual", value)
    }

    get inn() {
        return this.get('inn');
    }

    set inn(value: string) {
        this.set('inn', value);
    }

    get kpp() {
        return this.get('kpp');
    }

    set kpp(value: string) {
        this.set('kpp', value);
    }

    get orgn() {
        return this.get('orgn');
    }

    set orgn(value: string) {
        this.set('orgn', value);
    }

    get okpo() {
        return this.get('okpo');
    }

    set okpo(value: string) {
        this.set('okpo', value);
    }

    get comments() {
        return this.get('comments');
    }

    set comments(value: string) {
        this.set('comments', value);
    }

    get rs() {
        return this.get('rs');
    }

    set rs(value: CompanyRS[]) {
        this.set('rs', value);
    }

    get stamp() {
        return this.get('stamp');
    }

    set stamp(value: Parse.File) {
        this.set('stamp', value);
    }

    get accountantSign() {
        return this.get('accountantSign');
    }

    set accountantSign(value: Parse.File) {
        this.set('accountantSign', value);
    }

    get headSign() {
        return this.get('headSign');
    }

    set headSign(value: Parse.File) {
        this.set('headSign', value);
    }

    get accountRole() {
        return this.get('accountRole');
    }

    set accountRole(value: Model.Role) {
        this.set('accountRole', value);
    }

    get productGroup() {
        return this.get("productGroup");
    }

    set productGroup(value: Model.Group) {
        this.set("productGroup", value);
    }
}

export class CompanyRS {
    bankName: string;
    bankAddress: string;
    bik: string;
    kor: string;
    rs: string = '';
    primary: boolean;
}

LocalAdapter.initLocalObject(Company, 'Company');