import Database from "../abstract/database"
import * as Parse from "parse";
import LocalAdapter from "../abstract/database/lib/adapter";
import LocalObject from "../abstract/database/lib/object";

export enum VersionTypeEnum {
    silent, modal, native
}

export class Version extends Parse.Object {
    constructor() {
        super("Version");
    }

    get version() {
        return this.get('version')
    }

    set version(value: string) {
        this.set('version', value);
    }

    get appName() {
        return this.get('appName');
    }

    set appName(value: string) {
        this.set('appName', value)
    }

    get type() {
        return VersionTypeEnum[this.get('type') as string]
    }

    set type(value: VersionTypeEnum) {
        this.set('type', VersionTypeEnum[value]);
    }
}
Parse.Object.registerSubclass('Version', Version);
//LocalAdapter.initLocalObject(Version, 'Version');