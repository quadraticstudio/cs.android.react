// import * as Parse from "parse";
// import * as Model from "./";
// import LocalAdapter from "../abstract/database/lib/adapter";
// import LocalObject from "../abstract/database/lib/object";

// export enum OperationJournalTypeEnum {
//     authomatic,
//     return,
//     providerOut,
//     openTare,
//     unloadAtClosing,
//     backupUnload,
//     barcode,
//     inventory,
//     cashbox,
//     reports,
//     reassessment,
//     push,
//     providerIn,
//     sale,
//     manual,
//     system,
//     clear,
//     reference
// }

// export enum OperationJournalStatusTypeEnum {
//     ok,
//     error
// }

// export enum OperationJournalReferenceTypeEnum {
//     print, addGood, addMeasure, addBarcodeTemplate
// }

// class OperationJournal extends LocalObject {
//     constructor() {
//         super("OperationJournal");
//     }

//     get title(): string {
//         return this.get("title");
//     }

//     set title(value: string) {
//         this.set("title", value);
//     }

//     get type(): OperationJournalTypeEnum {
//         return OperationJournalTypeEnum[this.get("type") as string];
//     }

//     set type(value: OperationJournalTypeEnum) {
//         this.set("type", OperationJournalTypeEnum[value]);
//     }

//     get typeTitle(): string {
//         return this.get("typeTitle");
//     }

//     set typeTitle(value: string) {
//         this.set("typeTitle", value);
//     }

//     get status(): OperationJournalStatusTypeEnum {
//         return OperationJournalStatusTypeEnum[this.get("status") as string];
//     }

//     set status(value: OperationJournalStatusTypeEnum) {
//         this.set("status", OperationJournalStatusTypeEnum[value]);
//     }

//     get user(): Parse.User {
//         return this.get("user");
//     }

//     set user(value: Parse.User) {
//         this.set("user", value);
//     }

//     get cashbox(): Model.Cashbox {
//         return this.get("cashbox");
//     }

//     set cashbox(value: Model.Cashbox​​) {
//         this.set("cashbox", value);
//     }
// }

// LocalAdapter.initLocalObject(OperationJournal, 'OperationJournal');

// export {OperationJournal}