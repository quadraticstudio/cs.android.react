import * as React from "react"
import * as Parse from "parse";
import { ToastAndroid, NetInfo, Alert, BackHandler } from 'react-native';
import { AppLoading, Font, Constants, Contacts, Util } from "expo";
import { Container, View, Toast, Button, Text } from "native-base";
import Controllers from "./mvc/index";
import Abstract from "./abstract";
import { NavigationContainer } from "react-navigation";
import App from "./App";

import { AsyncStorage } from "react-native";
import { Job } from "./abstract/database/lib/worker";
import { StorageController } from "./abstract/global/GlobalStorage";
import BaseConfig from "./config"
import { VersionController } from "./abstract/global/index";
import DbAdapter from "db-berish/lib/adapter";
import LocalAdapter from "./abstract/database/lib/adapter";
import { logOut, parseLogOutSession } from "./abstract/database/lib/auth";
import { Config } from "./model/index";
import Query from "./abstract/database/lib/query";

export default class Main extends React.Component<{}, { isReadyCache: boolean, isOnlyLocal: boolean }> {
    constructor(props) {
        super(props)
        this.state = {
            isReadyCache: false,
            isOnlyLocal: false
        }

    }

    loadAssets = async () => {
        console.log('start')
        await Abstract.Global.cacheFonts({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
        });
        await Abstract.Database.Adapter.init(
            { dbPrefix: 'dblocal59', storage: AsyncStorage },
            { appId: BaseConfig.appId, jsKey: BaseConfig.appKey, serverUrl: BaseConfig.appServerUrl },
            {
                condition: async () => {
                    if (this.state.isOnlyLocal)
                        return false;
                    let cb = async () => {
                        let network = await NetInfo.isConnected.fetch();
                        let parseConnected = false;
                        if (network)
                            parseConnected = await Abstract.Global.VersionController.isConnected();
                        return {
                            network, parseConnected
                        };
                    }
                    let info = await cb();
                    //Abstract.Global.notification('success', `network ${info.network}. parse ${info.parseConnected}`)
                    //console.log(`network ${info.network}. parse ${info.parseConnected}`);
                    return info.network && info.parseConnected;
                }
            }
        );
        StorageController.instance.componentArgs.global = await Config.loadNotAuth();
        /* await Abstract.Database.Adapter.instance.worker.addJob(new Job('pingServer', async () => {
             try {
                 if (StorageController.instance.componentArgs.global.cashbox && LocalAdapter.instance.condition) {
                     let result = await Parse.Cloud.run('cashbox:ping', {
                         cashboxId: StorageController.instance.componentArgs.global.cashbox.id
                     });
                     //Abstract.Global.notification('success', 'Пинг сервера успешен');
                 }
             } catch (err) {
                 Abstract.Global.error(err);
             }
         }, 6000))*/
        await Abstract.Database.Adapter.instance.worker.addJob(new Job('checkUpdate', async () => {
            await VersionController.checkListener();
        }, 10000))
        await Abstract.Database.Adapter.instance.worker.restart();
        await parseLogOutSession();
        await logOut();
    }

    renderMainRouter = () => {
        return (
            <View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }}>
                <View
                    style={{
                        backgroundColor: '#1c3c51'
                    }}
                />
                {/* <Abstract.General.Button
                    label={`Включить только локальную: ${this.state.isOnlyLocal} `}
                    onClick={() => {
                        this.setState({
                            isOnlyLocal: !this.state.isOnlyLocal
                        })
                    }}
                />
                <Abstract.General.Button
                    label={`Показать очередь`}
                    onClick={async () => {
                        let tasks = await LocalAdapter.instance.queue.showTasks();
                        Alert.alert('tasks', JSON.stringify(tasks.toArray()))
                    }}
                />
                <Abstract.General.Button
                    label={`Выполнить очередь`}
                    onClick={async () => {
                        let tasks = await LocalAdapter.instance.queue.upload();
                        Alert.alert('tasks', JSON.stringify(tasks))
                    }}
                />
                <Abstract.General.Button
                    label={`Выполнить одну задачу`}
                    onClick={async () => {
                        let task = await LocalAdapter.instance.queue.uploadOneTask();
                        let tasks = await LocalAdapter.instance.queue.showTasks();
                        Alert.alert('tasks', JSON.stringify(tasks))
                    }}
                /> */}
                <App />
            </View>
        );
    }

    render() {
        let { isReadyCache } = this.state;
        if (!isReadyCache) {
            return (
                <AppLoading
                    startAsync={this.loadAssets}
                    onFinish={() => this.setState({ isReadyCache: true })}
                    onError={err => Abstract.Global.error(err)}
                />
            );
        }
        return this.renderMainRouter();
    }
}