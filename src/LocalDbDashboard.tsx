import * as React from "react";
import { Grid, Row, Col, Text, View as RView } from "native-base";
import Abstract from "./abstract";
import { LINQ } from "linq";
import LocalObject from "./abstract/database/lib/object";
import { ComponentProps } from "./abstract/global/ComponentController";
import Query from "./abstract/database/lib/query";
import LocalAdapter from "./abstract/database/lib/adapter";
import { Alert } from "react-native";
import Relation from "./abstract/database/lib/relation";

interface ViewProps {
    isClassView?: boolean;
    data: any[];
    onClick: (obj, index: number) => any;
}

class View extends React.Component<ViewProps, any>{
    constructor(props) {
        super(props);
    }

    renderTable() {
        return (
            <Abstract.General.ListCollection
                data={this.props.data}
                onClick={(index, obj) => this.props.onClick(obj, index)}
                header={[
                    {
                        title: 'Название класса',
                        render: (obj: string) => {
                            return obj;
                        }
                    }
                ]}
            />
        );
    }

    renderRow = (index: number, data: LocalObject) => {
        let keys = data && Object.keys(data['_collection']);
        let elements = LINQ.fromArray(keys).selectMany(m => {
            let key = <Text style={{ color: "white", fontFamily: "Roboto", fontWeight: "bold", fontSize: 22 }}>{m}</Text>;
            let obj = data && data.get(m);
            let value = <Text style={{ fontSize: 18, color: "white" }}>{JSON.stringify(obj)}</Text>;
            if (obj instanceof LocalObject) {
                value = <Abstract.General.Button
                    label={obj.id}
                    onClick={() => this.props.onClick(obj, index)}
                />
            } else if (obj && obj instanceof Object && 'targetClassName' in obj && 'id' in obj) {
                value = <Abstract.General.Button
                    label={`Элементов: ${(obj.ids || []).length}`}
                    onClick={() => this.props.onClick(data.relation(m), index)}
                />
            }
            return [
                key,
                value
            ];
        }).toArray();
        return (
            <RView
                style={{
                    flexDirection: "column"
                }}>
                {elements}
            </RView>
        );
    }

    renderList() {
        return (
            <Abstract.General.List
                items={this.props.data}
                renderRow={this.renderRow}
                onClick={() => { }}
            />
        );
    }

    render() {
        if (this.props.isClassView) {
            return this.renderTable();
        }
        return this.renderList();
    }
}

interface ControllerProps extends ComponentProps<{ className: string, obj: LocalObject, relation: Relation<any> }> {

}

interface ControllerState {
    data: any[];
}

export default class Controller extends React.Component<ControllerProps, ControllerState>{
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    async componentWillMount() {
        this.props.controller.tryLoad(this.load)
    }

    load = async () => {
        let params = this.props.controller.params;
        if (params.obj) {
            let obj = await params.obj.fetch();
            this.setState({
                data: [obj]
            })
        } else if (params.relation) {
            let linq = await params.relation.find();
            this.setState({
                data: linq.toArray()
            })
        }
        else if (params.className) {
            let linq = await new Query(params.className).find();
            this.setState({
                data: linq.toArray()
            })
        } else {
            let classNames = Object.keys(LocalAdapter.ctors);
            this.setState({
                data: classNames.concat('_User')
            })
        }
    }

    render() {
        let params = this.props.controller.params;
        //return <Text>test</Text>
        return <View
            isClassView={!params.className}
            data={this.state.data}
            onClick={(obj: string | LocalObject | Relation<any>) => {
                if (typeof obj == 'string')
                    this.props.controller.router.navigation.navigate('LocalDB', { className: obj })
                else if (obj instanceof LocalObject) {
                    this.props.controller.router.navigation.navigate('LocalDB', { className: obj.className, obj })
                }
                else if (obj instanceof Relation) {
                    this.props.controller.router.navigation.navigate('LocalDB', { className: obj._localRelation.targetClassName, relation: obj })
                }
            }}
        />
    }
}