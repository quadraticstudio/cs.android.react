import * as React from "react";
import { View, Text, TouchableHighlight } from 'react-native';

export interface ViewProps {
    onPress?: () => any;
}

class ViewComponent extends React.Component<ViewProps, any> {
    render() {
        return (
            <View>
                <Text>123</Text>
            </View>
        );
    }
}

export default function press<T>(ClassComponent: React.ComponentClass<T>) {
    return class PressComponent extends React.Component<T & ViewProps, any>{
        render() {
            return (
                <TouchableHighlight onPress={this.props.onPress}>
                    <View>
                        <ClassComponent {...this.props} />
                    </View>
                </TouchableHighlight>
            );
        }
    }
}