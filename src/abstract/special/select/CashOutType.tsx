import * as React from 'react';
import * as Parse from "parse";
import * as collection from "collection";
import { LINQ } from 'linq';
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import Global from "../../global"
import { AbstractComponent } from '../../general/AbstractComponent';
import SelectField, { SelectFieldProps } from '../../general/Select';
import Query from '../../database/lib/query';

interface CashOutTypeProps {
}

const CashOutputTitles = new collection.Dictionary<string, string>(
    new collection.KeyValuePair('cash', 'Наличные'),
    new collection.KeyValuePair('card', 'Пластиковая карта')
)

export default class CashOutType extends AbstractComponent<CashOutTypeProps & SelectFieldProps<'cash' | 'card'>> {
    constructor(props) {
        super(props);
    }

    renderData = () => {
        return CashOutputTitles.toLinq().select(m => {
            return {
                value: m.key,
                view: m.value
            }
        })
    }

    render() {
        return (
            <SelectField
                {...this.props}
                data={this.renderData()}
            />
        );
    }
}