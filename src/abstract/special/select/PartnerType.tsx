import * as React from 'react';
import * as Parse from "parse";
import { LINQ } from 'linq';
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import Global from "../../global"
import { AbstractComponent } from '../../general/AbstractComponent';
import SelectField, { SelectFieldProps } from '../../general/Select';
import Query from '../../database/lib/query';

interface NdsTypeProps {
    controller: Global.ComponentController
}

export default class NdsType extends AbstractComponent<NdsTypeProps & SelectFieldProps<Model.Partner>, { data: LINQ<Model.Partner> }> {
    constructor(props) {
        super(props);
        this.state = {
            data: LINQ.fromArray([])
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.onLoad);
    }

    onLoad = async () => {
        let data = await new Query(Model.Partner).find();
        this.setState({
            data
        });
    }

    renderData = () => {
        let {
            data
        } = this.state;
        return data
            .select(m => {
                return {
                    value: m,
                    view: m.name
                }
            })
    }

    render() {
        return (
            <SelectField
                {...this.props}
                data={this.renderData()}
                label="Контрагент"
            />
        );
    }
}