import * as React from 'react';
import * as Parse from "parse";
import { LINQ } from 'linq';
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import Global from "../../global"
import { AbstractComponent } from '../../general/AbstractComponent';
import SelectField, { SelectFieldProps } from '../../general/Select';
import Query from '../../database/lib/query';

interface UnitTypeProps {
    controller: Global.ComponentController
}

export default class UnitType extends AbstractComponent<UnitTypeProps & SelectFieldProps<Model.Manual>, { data: LINQ<Model.Manual> }> {
    constructor(props) {
        super(props);
        this.state = {
            data: LINQ.fromArray([])
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.onLoad);
    }

    onLoad = async () => {
        let linq = await new Query(Model.Manual).find();
        let data = linq
            .where(m => m.type === Model.ManualTypeEnum.unit);
        let defaultValue = data.singleOrNull(m => m.system && m.keyType === Model.ManualUnitTypeEnum[Model.ManualUnitTypeEnum.sh]);
        this.setState({
            data
        });
    }

    renderData = () => {
        let {
            data
        } = this.state;
        return data
            .select(m => {
                return {
                    value: m,
                    view: m.key
                }
            })
    }

    render() {
        return (
            <SelectField
                {...this.props}
                data={this.renderData()}
                label="Единица измерения"
            />
        );
    }
}