import * as React from 'react';
import * as Parse from "parse";
import { LINQ } from 'linq';
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import Global from "../../global"
import { AbstractComponent } from '../../general/AbstractComponent';
import SelectField, { SelectFieldProps } from '../../general/Select';
import Query from '../../database/lib/query';

interface CashOutTypeProps {
}

export default class CashOutType extends AbstractComponent<CashOutTypeProps & SelectFieldProps<string>> {
    constructor(props) {
        super(props);
    }

    renderData = () => {
        return Model.Documents.CorrectBaseTypeLabels.toLinq().select(m => {
            return {
                value: Model.Documents.CorrectBaseTypeEnum[m.key],
                view: m.value
            }
        })
    }

    render() {
        return (
            <SelectField
                {...this.props}
                data={this.renderData()}
            />
        );
    }
}