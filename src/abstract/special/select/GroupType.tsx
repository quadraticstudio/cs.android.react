import * as React from 'react';
import * as Parse from "parse";
import { LINQ } from 'linq';
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import Global from "../../global"
import { AbstractComponent } from '../../general/AbstractComponent';
import SelectField, { SelectFieldProps } from '../../general/Select';
import Query from '../../database/lib/query';

interface GroupTypeProps {
    controller: Global.ComponentController;
    //exclude?: Model.Group[];
    exclude?: string[];
}

type keyValueGroup = { key?: Model.Group, value?: keyValueGroup[] }

export default class GroupType extends AbstractComponent<GroupTypeProps & SelectFieldProps<Model.Group>, { data: keyValueGroup[] }> {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.onLoad);
    }

    onLoad = async () => {
        let company = this.props.controller.storage.componentArgs.global.company;
        let group = company.productGroup;
        group = group && await group.fetch();
        let allGroups = await new Query(Model.Group).find()
        let data = this.fetchGroup(group, allGroups);
        this.setState({
            data: [data]
        });
    }

    fetchGroup = (group: Model.Group, allGroups: LINQ<Model.Group>): keyValueGroup => {
        let ids = LINQ.fromArray(group.childs.ids || []);
        let groupsRaw = allGroups.where(m => ids.contains(m.id) && m.type == Model.GroupItemTypeEnum.group);
        if (groupsRaw.count() > 0) {
            let promise = groupsRaw.select(m => {
                return this.fetchGroup(m, allGroups);
            }).toArray();
            return {
                key: group,
                value: promise
            }
        }
        return {
            key: group,
            value: []
        };
    }

    renderOneOption = (kv: keyValueGroup): Model.Group[] => {
        let { exclude } = this.props;
        /*if (exclude && exclude.length > 0 && LINQ.fromArray(exclude).count(m => m.id == kv.key.id) > 0) {
            return null;
        }*/
        if (exclude && exclude.length > 0 && LINQ.fromArray(exclude).count(m => m == kv.key.id) > 0) {
            return null;
        }
        return LINQ.fromArray(kv.value).selectMany(m => this.renderOneOption(m)).concat(kv.key).toArray();
    }

    renderData = () => {
        let { exclude } = this.props;
        let {
            data
        } = this.state;
        return LINQ.fromArray(data).selectMany(m => this.renderOneOption(m)).notNull().distinct(m => m.id)
            .select(m => {
                return {
                    value: m,
                    view: m.name
                }
            })
    }

    render() {
        return (
            <SelectField
                {...this.props}
                data={this.renderData()}
                label="Родительская группа"
            />
        );
    }
}