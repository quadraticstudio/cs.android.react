import * as React from "react";
import List from "../general/List";
import { View, Text, Icon, Fab } from "native-base";
import { StyleSheet } from "react-native";

interface GoodListProps {
    items: GoodListItem[];
    onClick?: (index: number, object: GoodListItem) => any;
}

export interface GoodListItem<T = any> {
    object: T;
    title: string;
    isBack?: boolean;
    isGroup?: boolean;
    number?: number;
    count?: number;
    unit?: string;
    price?: number;
}

interface GoodListState {
    opened?: boolean;
}

export default class GoodList extends React.Component<GoodListProps, GoodListState> {

    constructor(props) {
        super(props);
        this.state = {
            opened: false
        }
    }

    renderGroupRow = (index: number, item: GoodListItem) => {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                }}
            >
                <Text style={styles.groupTitle}>
                    {item.title}
                </Text>
                <Icon style={styles.groupArrow} name="ios-arrow-forward" />
            </View>
        );
    }

    renderGoodRow = (index: number, item: GoodListItem) => {
        return (
            <View style={{ width: "100%" }}>
                <Text style={styles.goodTitle}>{item.title}</Text>
                <View style={styles.goodTable}>
                    <Text style={styles.goodNumber}>{item.number != null ? `#${item.number}` : '-'}</Text>
                    <Text style={styles.goodUnit}>{item.count != null ? `${item.count} шт.` : '-'}</Text>
                    <Text style={styles.goodPrice}>{this.formatPrice(item.price)}</Text>
                </View>
            </View>
        );
    }

    formatPrice(price: number) {
        return `${(Math.round(price * 100) / 100).toFixed(2)} руб.`;
    }

    renderRow = (index: number, item: GoodListItem) => {
        if (item.isGroup) {
            return this.renderGroupRow(index, item);
        } else {
            return this.renderGoodRow(index, item);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <List
                    items={this.props.items}
                    renderRow={this.renderRow}
                    onClick={(i, o: GoodListItem) => this.props.onClick(i, o)}
                />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    groupTitle: {
        fontSize: 25,
        color: "white"
    },
    groupArrow: {
        fontSize: 20,
        color: "white",
    },
    goodTitle: {
        width: "100%",
        color: "white",
        fontSize: 20
    },
    goodUnit: {
        color: "white"
    },
    goodTable: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%",
        paddingTop: 20,
        paddingBottom: 20
    },
    goodPrice: {
        color: "#2196F3",
        fontSize: 18
    },
    goodNumber: {
        color: "#4CAF50",
        fontSize: 18
    }
});