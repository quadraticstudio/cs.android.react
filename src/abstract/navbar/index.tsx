import * as React from "react";
import { View, Text, StyleProp } from 'react-native';

export interface ViewProps {
    title: string;
    onBack?: () => any;
    style?: StyleProp<any>;
}

export default class ViewComponent extends React.Component<ViewProps, any> {
    render() {
        return (
            <View style={this.props.style}>
                <View>
                    <Text>{this.props.children}</Text>
                </View>
            </View>
        );
    }
}