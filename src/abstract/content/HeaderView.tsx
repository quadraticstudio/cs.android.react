import * as React from "react";
import { HeaderProps } from "react-navigation";
import { Header, Left, Right, Body, Title, Button, Icon, Text } from "native-base";

export interface ViewProps extends HeaderProps {
    title: string;
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderBackButton = () => {
        let isNeed = this.props.scenes && this.props.scenes.length > 1;
        let style = isNeed ? { maxWidth: 30, minWidth: 30 } : { maxWidth: 0 };
        return (
            <Left style={style}>
                {isNeed
                    ? <Button transparent onPressOut={() => this.props.navigation.goBack()}>
                        <Icon name='arrow-back' />
                    </Button>
                    : null
                }
            </Left>
        );
    }

    renderBody = () => {
        return (
            <Body>
                {this.props.title
                    ? <Title><Text style={{color: "white"}}>{this.props.title}</Text></Title>
                    : null}
            </Body>
        );
    }

    render() {
        return (
            <Header style={{backgroundColor: "#070d19"}}>
                {this.renderBackButton()}
                {this.renderBody()}
            </Header>
        );
    }
}