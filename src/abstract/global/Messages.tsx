import * as React from "react";
import * as Parse from "parse";
import * as DbBerish from "db-berish";
import * as Global from "./";
import Spin from "../general/Spin";
import { ToastAndroid } from "react-native";
import { ComponentController } from "./ComponentController";
import * as Model from "../../model";
import LocalAdapter from "../database/lib/adapter";
import { logOut } from "../database/lib/auth";
import { Util } from "expo";
import Query from "../database/lib/query";
//import { FilterConfig } from "./components/General/Filter";

function isParseError(error: any): error is Parse.Error {
    return error.code != null;
}

export function getParseError<T>(error: T) {
    const childError = (error: Parse.Error): Parse.Error => {
        let message = error.message as string | Parse.Error;
        if (typeof message == 'string')
            return error;
        return childError(message);
    }
    if (isParseError(error)) {
        return childError(error);
    }
    return error;
}

export function error(error: Error | Parse.Error | string) {
    error = getParseError(error);
    let msg = notification("danger", error);
    console.log(msg);
}

export function message(text: string) {
    notification('success', text);
}

export function notification(type: 'danger' | 'success' | 'warning', message: Error | Parse.Error | string) {
    message = getParseError(message);
    let msg = type == 'danger' ? (
        message instanceof Error
            ? (
                message.name == 'Error' ? [`Ошибка: ${message.message}`] :
                    [
                        `Ошибка: ${message.message}`,
                        `Описание: ${message.name}`,
                    ]
            )
            : isParseError(message)
                ? [`Код ошибки: ${message.code}`, `Описание: ${message.message}`]
                : [`Описание ошибки: ${message}`])
        : [message];
    //Toast.show({ position: 'bottom', type, text: msg.join('. '), duration: 1500 })
    ToastAndroid.show(msg.join('. '), 0);
    return msg.join('. ');
}

export function loadingComponent<T>(Component: React.ComponentClass<T>) {
    return class Container extends React.Component<T & { loading?: boolean }> {
        render() {
            return loadingJSXElement(<Component {...this.props} />, this.props.loading);
        }
    }
}

export function loadingJSXElement(element: React.ReactNode | React.ReactNode[], loading?: boolean, title?: string) {
    //return element;
    return (
        <Spin spinning={!!loading} title={title}>
            {element}
        </Spin>
    );
}

/*type promiseOrObject<T> = T | Promise<T>;
type linqOrArray<T> = LINQ<T> | T[];

function getLinq<T>(obj: linqOrArray<T>) {
    if (obj instanceof LINQ)
        return obj;
    return LINQ.fromArray(obj || [])
}*/

/*export function filterApplyAsync<T, K>(data: promiseOrObject<linqOrArray<T>>, filterConfig: FilterConfig<T, K>) {
    let linq = data instanceof Promise ? new Promise<LINQ<T>>(async (resolve, reject) => {
        try {
            let d = await data;
            let linq = getLinq(d);
            if (!(filterConfig == null || filterConfig.filters == null || filterConfig.filters.length <= 0)) {
                let filters = getLinq(filterConfig.filters || []);
                let temp = await Promise.all(linq.select(async m => {
                    let res = await Promise.all(filters.select(k => k(m)).toArray());
                    return {
                        obj: m,
                        res
                    }
                }).toArray());
                linq = LINQ.fromArray(temp).where(m => LINQ.fromArray(m.res || []).all(k => k)).select(m => m.obj);
            }
            resolve(linq);
        } catch (err) {
            reject(err);
        }
    }) : getLinq(data);
    if (linq instanceof LINQ) {
        if (!(filterConfig == null || filterConfig.filters == null || filterConfig.filters.length <= 0)) {
            let filters = getLinq(filterConfig.filters || []);
            linq = linq.where(m => {
                return filters.all(k => k(m) as boolean);
            });
        }
    }
    return (cb: (data: promiseOrObject<linqOrArray<T>>) => React.ReactNode) => {
        return cb(linq);
    }
}*/

/*export function filterApplySync<T, K>(data: linqOrArray<T>, filterConfig: FilterConfig<T, K>) {
    let linq = getLinq(data);
    if (!(filterConfig == null || filterConfig.filters == null || filterConfig.filters.length <= 0)) {
        let filters = getLinq(filterConfig.filters || []);
        linq = linq.where(m => {
            return filters.all(k => k(m) as boolean);
        });
    }
    return (cb: (data: LINQ<T>) => React.ReactNode) => {
        return cb(linq);
    }
}*/

export async function openSession(changeTitle: (title: string) => any, controller: ComponentController) {
    let condition = LocalAdapter.instance.condition;
    if (condition) {
        if (!controller.storage.componentArgs.global.firstDownloadEnded) {
            if (changeTitle)
                changeTitle('Первичная настройка кассы')
            await LocalAdapter.instance.queue.firstDownload((all, current) => changeTitle(`Первичная настройка кассы ${Math.round(current / all * 100)}%`));
            await controller.storage.update(m => {
                m.global.firstDownloadEnded = true;
            })
        } else {
            if (changeTitle)
                changeTitle('Обновление локальной базы данных')
            if (changeTitle)
                changeTitle('Выгрузка изменений из очереди');
            //await LocalAdapter.instance.queue.upload(controller, (all, current) => changeTitle(`Выгрузка изменений из очереди ${Math.round(current / all * 100)}%`));
            /*await LocalAdapter.instance.queue.download((hash, all, current) => {
                let percent = `${Math.round(current / all * 100)}%`;
                let title = '';
                if (hash == 'hashParse')
                    title = 'Проверка удаленных изменений'
                else if (hash == 'hashLocal')
                    title = 'Получение локальной хэш-таблицы';
                else if (hash == 'hashCheck')
                    title = 'Проверка обновления данных';
                else
                    title = 'Обновление локальной базы данных';
                changeTitle(`${title} ${percent}`);
            });*/
        }
    }
    /*LocalAdapter.instance.worker.addJob(new Job('upload', async () => {
        await LocalAdapter.instance.queue.upload(controller);
    }, 500));*/
    /*LocalAdapter.instance.worker.addJob(new Job('download', async () => {
        if (controller.storage.componentArgs.global.firstDownloadEnded)
            await LocalAdapter.instance.queue.download(() => { });
    }, 2000));*/
    LocalAdapter.instance.worker.restart();
    if (changeTitle)
        changeTitle('Открываем смену')
    await Model.openSession();
    if (changeTitle)
        changeTitle(null);
}

export async function closeSession(createDocument?: boolean) {
    await logOut();
    LocalAdapter.instance.worker.removeJob('upload', 'download');
    Util.reload();
    if (createDocument)
        await Model.closeAllOpenedSessions();
}