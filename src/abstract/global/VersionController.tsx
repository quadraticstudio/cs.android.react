import * as Parse from "parse";
import { Version, VersionTypeEnum } from "../../model";
import { Constants } from "expo";
import { LINQ } from "linq";
import { Util } from "expo";
import { Alert, ToastAndroid } from "react-native";
import { StorageController } from "./index";
import Query from "../database/lib/query";
import LocalAdapter from "../database/lib/adapter";
import Config from "../../config";
import { parseLogOutSession } from "../database/lib/auth";

class _VersionController {

    private asked: string = null;

    constructor() {

    }

    private static _instance: _VersionController = null;

    public static get instance() {
        if (this._instance == null)
            this._instance = new _VersionController();
        return this._instance;
    }

    getLocalVersion = () => {
        const currentNumber = Constants.manifest.version && LINQ.fromArray(Constants.manifest.version.split('.')).select(m => parseInt(m)).toArray() || [];
        const appName = Constants.manifest.extra && Constants.manifest.extra['appName'] || '';
        return {
            currentNumber, appName
        };
    }

    getVersions = async () => {
        let local = this.getLocalVersion();
        let versions = await new Parse.Query(Version).find();
        let current = {
            major: local.currentNumber[0],
            minor: local.currentNumber[1],
            patch: local.currentNumber[2]
        };
        let versionsLinq = LINQ.fromArray(versions || []);
        let appVersion = versionsLinq.where(m => m.appName == local.appName).select(m => {
            let version = m.version || '';
            let splitter = LINQ.fromArray(version.split('.')).select(m => parseInt(m)).toArray();
            return {
                major: splitter[0],
                minor: splitter[1],
                patch: splitter[2],
                parse: m
            }
        }).where(m => {
            if (m.major > current.major || (m.major == current.major && m.minor > current.minor) || (m.major == current.major && m.minor == current.minor && m.patch > current.patch))
                return true;
            return false;
        }).orderByDescending(m => m.major * 10000 + m.minor * 100 + m.patch).firstOrNull();
        return appVersion;
    }

    async checkListener() {
        try {
            let version = await this.getVersions();
            if (!version)
                return;
            let type = version.parse.type != null ? version.parse.type : VersionTypeEnum.modal;
            if (this.asked == version.parse.version && type != VersionTypeEnum.silent)
                return;
            if (type == VersionTypeEnum.modal || type == VersionTypeEnum.native) {
                Alert.alert('Доступно новое обновление', 'Применить обновление сейчас или после перезагрузки приложения?', [
                    {
                        text: 'Сейчас',
                        onPress: () => Util.reload()
                    },
                    {
                        text: 'Позже'
                    }
                ]);
                this.asked = version.parse.version;
                return;
            } /*else if (type == VersionTypeEnum.native) {
    
            } */
            else if (type == VersionTypeEnum.silent) {
                Util.reload();
                return;
            }
        } catch (err) {

        }
    }

    isConnected = async () => {
        try {
            let query = await Parse.Cloud.run("cashbox:checkInternet", {});
            /*let response = await fetch(Config.appServerUrl.slice(0, Config.appServerUrl.indexOf('parse')));
            if (response.status !== 200)
                throw new Error('not response');*/
            return true;
        } catch (err) {
            /*if (err.code == 100) {
                let response = await fetch(Config.appServerUrl.slice(0, Config.appServerUrl.indexOf('parse')));
                if (response.status == 200) {
                    Util.reload();
                    return false;
                }
                return false;
            }*/
            return false;
        }
    }
}

export const VersionController = _VersionController.instance;