import * as Parse from "parse";
import * as Model from "../../model";
import { Font, Asset, RequireSource } from "expo";
import { Image, AsyncStorage, Alert } from "react-native";
import { LINQ } from "linq";
import { ComponentProps } from "./ComponentController";
import { AreaProps } from "../routers/ContentRoute";
import Query from "../database/lib/query";
import DbQuery from "db-berish/lib/query";
import { Config } from "../../model";

/*export class ConfigStorage {

    static async saveRaw(instance: ConfigObjectRaw) {
        return AsyncStorage.setItem('global', JSON.stringify(instance));
    }

    static async save(instance: ConfigObject) {
        let raw = await this.toRaw(instance);
        return this.saveRaw(raw);
    }


    static async loadRaw() {
        let instance: ConfigObjectRaw = {};
        try {
            let model = (await AsyncStorage.getItem('global')) || '';
            let args = JSON.parse(model) as ConfigObjectRaw;
            for (let key in args) {
                instance[key] = args[key];
            }
        } catch (err) {

        } finally {
            return instance;
        }
    }

    static async loadNotAuth() {
        let raw = await this.loadRaw();
        return this.fromRawNotAuth(raw);
    }

    static async loadAuth() {
        let raw = await this.loadRaw();
        return this.fromRawAuth(raw);
    }

    static async fromRawNotAuth(raw: ConfigObjectRaw) {
        let obj = new ConfigObject();
        if (raw.cashboxId) {
            obj.cashbox = new Model.Cashbox();
            obj.cashbox.id = raw.cashboxId;
        }
        obj.users = raw.users || [];
        return obj;
    }

    static async fromRawAuth(raw: ConfigObjectRaw) {
        let obj = await this.fromRawNotAuth(raw);
        obj.cashbox = obj.cashbox && await obj.cashbox.fetch();
        if (obj.cashbox) {
            obj.cashbox.company = obj.cashbox.company && await obj.cashbox.company.fetch();
            obj.cashbox.shop = obj.cashbox.shop && await obj.cashbox.shop.fetch();
        }
        return obj;
    }

    static async toRaw(instance: ConfigObject) {
        let raw: ConfigObjectRaw = {};
        raw.cashboxId = instance.cashbox && instance.cashbox.id;
        raw.users = instance.users || [];
        return raw;
    }
}*/

export interface SingletonProps {
    history: string[]
}

export interface ComponentArgs {
    area: AreaProps,
    global: Config
}

export class StorageController {
    private _listen: ((compArg: ComponentArgs) => void)[] = [];

    public componentArgs: ComponentArgs = {
        area: {},
        global: {}
    } as any;

    private static _instance: StorageController = null;
    public static get instance() {
        if (this._instance == null)
            this._instance = new StorageController();
        return this._instance;
    }

    public listen(cb: (compArg: ComponentArgs) => void) {
        this._listen.push(cb);
        return () => {
            let indexOf = this._listen.indexOf(cb);
            if (indexOf >= 0)
                this._listen.splice(indexOf, 1);
        }
    }

    private async  writeToListen() {
        this.componentArgs.global = await this.componentArgs.global.saveToLocal();
        //setImmediate(() => ConfigStorage.save(this.componentArgs.global));
        for (let l of this._listen)
            setImmediate(() => l(this.componentArgs))
    }

    async update(cb?: (compArg: ComponentArgs) => void | Promise<void>) {
        if (cb)
            await cb(this.componentArgs);
        this.writeToListen();
    }
}

export function cacheImages(images: (RequireSource | string)[]) {
    let promises = LINQ.fromArray(images || []).select(async m => typeof m === 'string' ? Image.prefetch(m) : Asset.fromModule(m).downloadAsync()).toArray();
    return Promise.all(promises);
}

type FontMap = { [name: string]: RequireSource };

export async function cacheFonts(fonts: FontMap) {
    return Font.loadAsync(fonts);
}