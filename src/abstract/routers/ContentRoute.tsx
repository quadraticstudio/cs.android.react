import * as React from 'react';
import { pure } from "recompose";
import * as Global from "../global"
import { LINQ } from 'linq';
import { NavigationSceneRendererProps } from 'react-navigation';
import { generateId } from 'db-berish/lib/util';
import { View, Content, Text } from "native-base";
import ArrayComponents from '../general/ArrayComponents';
import { BackHandler } from 'react-native';

export interface ContentRouteProps {
    Component?: React.ComponentType<Global.ComponentProps>
    navigationProps?: NavigationSceneRendererProps
}

interface ContentRouteState {
    loading: boolean;
    controller: Global.ComponentController
    componentArgs: Global.ComponentArgs,
    unlistener: () => void,
    hash: string;
    staticComponents: JSX.Element[];
}

export interface AreaProps {
    loadingHash?: string[];
    loading?: boolean;
    title?: string;
}

class ContentRoute extends React.Component<ContentRouteProps, ContentRouteState> {

    constructor(props: ContentRouteProps) {
        super(props);
        this.state = {
            controller: null,
            componentArgs: null,
            unlistener: null,
            loading: true,
            hash: null,
            staticComponents: []
        }
    }

    loading = (loading: boolean) => {
        return new Promise<void>(resolve => this.setState({ loading }, () => resolve()));
    }

    async componentDidMount() {
        try {
            await this.loading(true);
            let controller = this.initController();
            /*function backPress() {
                controller.storage.update(m => {
                    m.singleton.history = m.singleton.history || [];
                    if (m.singleton.history.length) {
                        m.singleton.history.pop();
                    }
                })
                return false;
            }
            BackHandler.addEventListener('hardwareBackPress', backPress)*/
        } catch (err) {
            Global.error(err);
        } finally {
            await this.loading(false);
        }
    }

    async componentWillUnmount() {
        try {
            if (this.state.unlistener)
                this.state.unlistener();
        } catch (err) {
            Global.error(err)
        }
    }

    async componentWillReceiveProps(nextProps: ContentRouteProps) {
        try {
            await this.loading(true);
            this.initController(nextProps);
        } catch (err) {
            Global.error(err)
        } finally {
            await this.loading(false);
        }
    }

    initController = (nextProps?: ContentRouteProps) => {
        let controller = this.state.controller || new Global.ComponentController((nextProps || this.props).navigationProps, this.createStaticComponent);
        let state = {};
        if (this.state.controller) {
            state = {
                controller: controller.receive((nextProps || this.props).navigationProps)
            };
        } else {
            let componentArgs = controller.storage.componentArgs;
            let unlistener = controller.storage.listen(componentArgs => this.setState({ componentArgs }));
            let hash = Global.ActivationFunction(generateId());
            state = {
                controller,
                componentArgs,
                unlistener,
                hash
            };
        }
        this.setState(state);
        return controller;
    }

    createStaticComponent = (element: JSX.Element) => {
        let { staticComponents } = this.state;
        staticComponents.push(element)
        this.setState({
            staticComponents
        })
        return () => {
            staticComponents = LINQ.fromArray(staticComponents).except([element]).toArray();
            this.setState({
                staticComponents
            })
        }
    }

    renderComponent = () => {
        let { Component } = this.props;
        let {
            controller, componentArgs, hash, staticComponents
        } = this.state;
        return (
            <Content style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }}>
                {
                    Global.loadingJSXElement(
                        <Component
                            controller={controller}
                            hash={hash}
                            global={componentArgs.global}
                        />,
                        componentArgs.area.loading || (componentArgs.area.loadingHash && componentArgs.area.loadingHash.length > 0), componentArgs.area.title)
                }
                {ArrayComponents.render({
                    elements: staticComponents,
                    template: (index, m) => (<View key={index}>{m}</View>)
                })}
            </Content>
        );
    }

    render() {
        let { loading } = this.state;
        if (loading)
            return <View style={{ width: '100%', height: '100%', backgroundColor: '#070d19' }} />;
        return this.renderComponent();
    }
}

export default ContentRoute