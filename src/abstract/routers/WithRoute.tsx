import * as React from "react";
import ContentRoute, { ContentRouteProps } from "./ContentRoute";
//import { RouteComponentProps, withRouter } from "react-router-native";
import { pure } from "recompose";
import { NavigationSceneRendererProps, HeaderProps, NavigationParams } from "react-navigation";
import Content from "../content"
import { Icon, View } from 'native-base';
import { Util } from "expo";
import { closeSession } from "../global/Messages";
import { ComponentProps } from "../global/ComponentController";
import { BackHandler } from "react-native";
import { StorageController } from "../global/index";

export function high<T extends ComponentProps>(ClassComponent: React.ComponentClass<T>) {
    return class NavigatorComponent extends React.Component<NavigationSceneRendererProps>{
        render() {
            return <ContentRoute
                Component={ClassComponent}
                navigationProps={this.props}
            />
        }
    }
}

interface ICreationType {
    name: string;
    component: React.ComponentClass<any>;
    headerTitle?: React.ReactNode;
}

export default function create(...config: ICreationType[]) {
    let obj: { [name: string]: any } = {};
    for (let item of (config || [])) {
        obj[item.name] = {
            screen: high(item.component),
            navigationOptions: (props: NavigationSceneRendererProps) => {
                let headerLeft = item.name == "Main"
                    ? { headerLeft: <Icon name="person" color="white" style={{ marginLeft: 15, color: 'white' }} onPress={() => { closeSession() }} /> }
                    : {};
                let main = {
                    headerTitle: item.headerTitle,
                    headerStyle: {
                        backgroundColor: "#070d19",
                        //backgroundColor: 'white'
                    },
                    headerTitleStyle: {
                        color: "white"
                    },
                    headerBackTitleStyle: {
                        color: "white"
                    },
                    headerTintColor: "white"
                    // header: (headerProps: HeaderProps) => {
                    //     return <Content.HeaderView {...headerProps} title={item.headerTitle} />;
                    // }
                };
                return Object.assign(main, headerLeft)
            }
        }
    }
    return obj;
}