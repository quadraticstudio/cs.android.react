import * as React from "react";
import { Grid, Col, View, Text, Button, Icon, Row } from "native-base";
import * as NativeBase from "native-base";
import { StyleSheet, TouchableHighlight, TouchableNativeFeedback } from 'react-native'
import High from "../high"

export interface ViewProps {
    items: TilesItem[];
    inverted?: boolean;
    countAtRow?: number;
    renderItem?: (item: TilesItem, index: number) => React.ReactNode;
}

export interface TilesItem {
    title: string;
    iconName?: string;
    onPress?: any;
    fontSize?: any;
    iconSize?: any;
}

interface ComponentState {
    disabled: boolean;
}

class ViewComponent extends React.Component<ViewProps, ComponentState> {

    constructor(props) {
        super(props);
        this.state = {
            disabled: false
        }
    }

    onPress = async (onPress: any) => {
        if (onPress) {
            await new Promise(resolve => this.setState({ disabled: true }, () => resolve()));
            await onPress();
            await new Promise(resolve => setTimeout(() => resolve(), 500));
            await new Promise(resolve => this.setState({ disabled: false }, () => resolve()));
        }
    }

    renderItem = (item: TilesItem, index: number) => {
        if (this.props.renderItem)
            return this.props.renderItem(item, index);
        return (
            <TouchableNativeFeedback
                background={TouchableNativeFeedback.Ripple("#019add")}
            >
                <Button
                    key={index}
                    disabled={this.state.disabled}
                    onPress={() => this.onPress(item.onPress)}
                    style={this.props.inverted ? [styles.item, styles.invertedItem] as any : styles.item}
                >
                    {item.iconName && <Icon name={item.iconName} style={this.props.inverted ? [{ fontSize: item.iconSize || 35 }, styles.invertedTextStyle] : { fontSize: item.iconSize || 35 }} />}
                    <Text style={this.props.inverted ? [{ fontSize: item.fontSize || 18, textAlign: 'center' }, styles.invertedTextStyle] : { fontSize: item.fontSize || 18, textAlign: 'center' }}>{item.title}</Text>
                </Button>
            </TouchableNativeFeedback>
        );
    }

    prepare = () => {
        let count = this.props.items.length;
        let countAtRow = this.props.countAtRow || 2;
        let rows = [];
        let rowCount = Math.round(count / countAtRow);
        let lastFull = count % countAtRow == 1;
        let counter = 0;
        for (let i = 0; i < rowCount; i++) {
            rows[i] = this.props.items.slice(i * countAtRow, i * countAtRow + countAtRow);
        }
        return rows;
    }

    render() {
        let list = this.prepare();
        return (
            <Grid style={{ backgroundColor: "#070d19" }}>
                {
                    list.map((row, i) => {
                        return (
                            <Row key={i} style={{ width: "100%" }}>
                                {
                                    row.map((item, i) => {
                                        return (
                                            <Col
                                                style={i !== row.length - 1 ? {
                                                    padding: 5,
                                                    paddingLeft: i % 2 == 0 ? 10 : 5,
                                                    paddingRight: i % 2 == 0 ? 5 : 10
                                                } : { padding: 5, paddingLeft: 10, paddingRight: 10 }}
                                                key={i}
                                            >
                                                {this.renderItem(item, i)}
                                            </Col>);
                                    })
                                }
                            </Row>
                        );
                    })
                }
            </Grid>
        );
    }
}

const styles = StyleSheet.create({
    item: {
        height: 100,
        width: "100%",
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: "#070d19",
        borderWidth: 1,
        borderColor: "#019add"
    },
    invertedItem: {
        borderColor: "transparent",
        backgroundColor: "white"
    },
    invertedTextStyle: {
        color: "#00a5e7"
    },
    itemText: {
        fontSize: 18
    },
    itemIcon: {
        fontSize: 35
    }
});

export default ViewComponent;