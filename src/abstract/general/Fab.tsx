import * as React from "react";
import { Button, Text, Icon, Fab, View } from "native-base";
import { AbstractComponent } from "./AbstractComponent";
import { Dimensions, StyleSheet, StyleProp } from "react-native";
import Debug from "../debug"

export interface ViewProps {
    onClick?: () => void;
    icon: string;
    position?: "topLeft" | "topRight" | "bottomLeft" | "bottomRight";
    fixed?: boolean;
    backgroundColor?: string;
    visible?: boolean;
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    createStyle = (...styles: StyleProp<any>[]) => {
        return StyleSheet.flatten(styles);
    }

    getPosition = () => {
        switch (this.props.position) {
            case "topLeft":
                return this.createStyle({ position: this.props.fixed ? "fixed" : "absolute", top: 10, left: 10 });
            case "topRight":
                return this.createStyle({ position: this.props.fixed ? "fixed" : "absolute", top: 10, right: 10 });
            case "bottomLeft":
                return this.createStyle({ position: this.props.fixed ? "fixed" : "absolute", bottom: 10, left: 10 });
            case "bottomRight":
                return this.createStyle({ position: this.props.fixed ? "fixed" : "absolute", bottom: 10, right: 10 });
        }
    }

    render() {
        let style = this.createStyle(styles.button, { backgroundColor: this.props.backgroundColor || "#00a5e7" }, this.getPosition()) as any;
        let visible = this.props.visible == null ? true : this.props.visible;
        return (
            <Button
                style={visible ? style : { ...style, display: 'none' }}
                onPress={this.props.onClick}
            >
                <View style={styles.iconWrapper}>
                    <Icon style={styles.icon} name={this.props.icon} />
                </View>
            </Button>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 100,
        width: 60,
        height: 60
    },
    iconWrapper: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },
    icon: {
        color: "white",
        fontSize: 24
    }
});