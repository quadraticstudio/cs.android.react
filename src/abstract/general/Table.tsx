import * as React from "react"
import { View, Text, List, ListItem } from "native-base"
import { StyleSheet, PixelRatio } from "react-native"
import { Grid, Row, Col } from "react-native-easy-grid"

interface TableProps {
    headers?: string[];
    items: any[][];
}

export interface TableHeader {
    title: string;
    onClick?: () => void;
}

export default class Table extends React.Component<TableProps> {

    renderHeaders = () => {
        return (
            this.props.headers &&
            <ListItem style={styles.listItem}>
                <View style={styles.row}>
                    {this.props.headers.map((v, i) => {
                        return <View style={styles.col} key={i}><Text style={styles.header}>{v}</Text></View>;
                    })}
                </View>
            </ListItem>
        );
    }

    renderItems = () => {
        return (
            this.props.items &&
            this.props.items.map((row, i) => {
                return (
                    <ListItem key={i} style={styles.listItem}>
                        <View style={styles.row}>
                            {row.map((v, i) => {
                                return <View style={styles.col} key={i}>{v}</View>;
                            })}
                        </View>
                    </ListItem>
                );
            })
        );
    }

    render() {
        return (
            <List>
                {this.renderHeaders()}
                {this.renderItems()}
            </List>
        );
    }

}

const styles = StyleSheet.create({
    table: {
        backgroundColor: "black"
    },
    listItem: {
        marginLeft: 0,
        paddingLeft: 17,
        height: "auto"
    },
    header: {
        fontWeight: "500",
        color: "#757575",
        fontSize: 16,
    },
    item: {

    },
    row: {
        flex: 1,
        alignItems: "center",
        flexDirection: "row"
    },
    col: {
        flex: 1,
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "center",
        minHeight: 80 / PixelRatio.get()
    }
});