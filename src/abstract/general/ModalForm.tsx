import * as React from "react";
import { LINQ } from "linq";
import { StaticComponentProps } from "../global/ComponentController";
import { Modal, TouchableOpacity, TouchableWithoutFeedback, StyleSheet } from "react-native";
import { View, Text, Card, CardItem } from "native-base";
import { ComponentController } from "../global/index";

type ObjectOrFuncToGetObject<T> = (() => T) | T;

function getObjectInOOFTGO<T>(obj: ObjectOrFuncToGetObject<T>) {
    if (obj instanceof Function)
        return obj();
    return obj;
}

export interface ModalFormProps {
    title?: ObjectOrFuncToGetObject<React.ReactNode>;
    footerBar?: ObjectOrFuncToGetObject<React.ReactNode>;
}

export default class ModalForm extends React.Component<ModalFormProps & StaticComponentProps<null>, { contentHeight: number }>{
    constructor(props) {
        super(props);
        this.state = {
            contentHeight: null
        };
    }

    renderTitle = () => {
        let { title } = this.props;
        return getObjectInOOFTGO(title);
    }

    renderFooterBar = () => {
        let { footerBar } = this.props;
        return getObjectInOOFTGO(footerBar);
    }

    render() {
        let title = this.renderTitle();
        let footerBar = this.renderFooterBar();
        return (
            <Modal
                {...this.props}
                animationType="fade"
                visible={true}
                transparent
                onRequestClose={() => {
                    if (this.props.reject)
                        this.props.reject();
                }}
            >
                <TouchableOpacity style={styles.modal} activeOpacity={1} onPressOut={() => {
                    if (this.props.reject)
                        this.props.reject();
                }}>
                    <View style={{ width: "80%", height: (this.state.contentHeight || 0) + 130, maxHeight: 400 }}>
                        <TouchableWithoutFeedback>
                            <Card style={styles.card}>
                                {this.props.title && <CardItem style={styles.header}>
                                    <Text style={styles.headerText}>{this.props.title}</Text>
                                </CardItem>}
                                <CardItem style={{ width: "100%" }}>
                                    <View onLayout={(e) => this.setState({ contentHeight: e.nativeEvent.layout.height })} style={styles.content}>
                                        {this.props.children}
                                    </View>
                                </CardItem>
                            </Card>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableOpacity>
            </Modal>
        );
    }
}



const styles = StyleSheet.create({
    button: {
        flex: 1,
        justifyContent: "flex-end",
        flexDirection: "row",
        width: "100%",
        position: "absolute",
        bottom: 0,
    },
    modal: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0, 0, 0, 0.3)"
    },
    header: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 15,
        paddingLeft: 15
    },
    card: {
        padding: 10,
        paddingBottom: 0,
        height: "auto"
    },
    headerText: {
        fontSize: 18,
        fontWeight: "900"
    },
    content: {
        width: "100%"
    }
});