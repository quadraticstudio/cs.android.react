import * as React from "react";
import Abstract from "../"
import { ActivityIndicator } from "react-native";
import { View, Text, Grid, Row, Col } from "native-base";

interface SpinProps {
    spinning: boolean;
    title?: string;
    color?: string;
}

export default class Spin extends React.Component<SpinProps, null> {

    // renderSpinner = () => {
    //     return (
    //         this.props.loading ? 
    //          : null
    //     );
    // }

    renderSpin = () => {
        return [
            <View
                style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    height: '100%',
                    width: '100%',
                    backgroundColor: 'rgba(0,0,0,.5)',
                    zIndex: 299
                }}
                key={0}
            />,
            <View
                style={{
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    height: '85%',
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 300
                }}
                key={1}
            >
                <View style={{ width: '80%', height: 100, backgroundColor: '#010e21', borderColor: "#2196F3", borderWidth: 1, borderRadius: 2 }}>
                    <Grid>
                        <Row>
                            <Col style={{ flex: 1, justifyContent: 'center' }}>
                                <ActivityIndicator
                                    animating={true}
                                    color="#2196F3"
                                    size="large"
                                />
                            </Col>
                            <Col style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={{ color: "white" }}>{this.props.title || 'Загрузка'}</Text>
                            </Col>
                        </Row>
                    </Grid>
                </View>
            </View>
        ];
    }

    render() {
        return (
            <View style={{ height: '100%', width: '100%', flex: 1 }}>
                <View style={{ height: '100%', width: '100%', zIndex: 1, flex: 1 }}>{this.props.children}</View>
                {!!this.props.spinning ? this.renderSpin() : null}
            </View >
        );
    }

}