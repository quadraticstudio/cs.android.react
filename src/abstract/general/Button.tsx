import * as React from "react";
import { Button, Text, NativeBase } from "native-base";
import { AbstractComponent } from "./AbstractComponent";
import { ViewStyle } from "react-native";

export interface ViewProps {
    label: string;
    onClick?: () => void;
    style?: ViewStyle;
    props?: NativeBase.Button;
    activeColor?: string;
}

export interface ViewState {
    disabled: boolean;
}

export default class ViewComponent extends AbstractComponent<ViewProps, ViewState> {

    constructor(props) {
        super(props);
        this.state = {
            disabled: false
        }
    }

    onClick = async () => {
        let onClick = this.props.onClick;
        if (onClick) {
            await new Promise(resolve => this.setState({ disabled: true }, () => resolve()));
            await onClick();
            await new Promise(resolve => this.setState({ disabled: false }, () => resolve()));
        }
    }

    render() {
        return (
            <Button style={{ width: "100%", justifyContent: "center", backgroundColor: "#0fa8eb", ...this.props.style }} onPress={this.onClick} {...this.props.props} disabled={this.state.disabled || (this.props.props && this.props.props.disabled || false)}>
                <Text>{this.props.label}</Text>
            </Button>
        );
    }
}