import Input from "./Input";
import Button from "./Button";
import Spin from "./Spin";
import List from "./List";
import ArrayComponents from "./ArrayComponents";
import Modal from "./Modal";
import Fab from "./Fab";
import Select from "./Select"
import NumericInput from "./NumericInput"
import Table from "./Table"
import Keyboard from "./Keyboard"
import Switch from "./Switch"
import ModalForm from "./ModalForm";
import TableBerish from "./TableBerish"
import ListCollection from "./ListCollection/Controller"
import NoNativeInput from "./NoNativeInput";

export default {
    Input, Button, Spin, List, ArrayComponents, Modal, Fab, Select, NumericInput, Table, Keyboard, Switch, ModalForm, TableBerish, NoNativeInput, ListCollection
}