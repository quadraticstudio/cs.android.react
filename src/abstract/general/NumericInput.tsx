import * as React from "react";
import { Item, Label, View, Text, Grid, Row, Col, Button } from "native-base";
import { AbstractComponent, ConfigPropsObject } from "./AbstractComponent";
import { Keyboard as NativeKeyboard, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, PixelRatio, ScrollView } from "react-native";
import Abstract from "../";
import Keyboard, { ViewProps as KeyboardProps } from "./Keyboard";
import Input, { ViewProps as InputProps } from "./Input";
import { generateId } from "db-berish/lib/util";

const dismissKeyboard = require("react-native-dismiss-keyboard");
export interface ViewProps {
    onEdit?: (path: string) => any;
}


interface ViewState {
    value: string
}

export default class ViewComponent extends AbstractComponent<ViewProps & InputProps, ViewState> {

    constructor(props) {
        super(props);
        this.state = {
            value: null
        }
    }

    getConfig = () => {
        let oldConfig = this.props.config || {} as ConfigPropsObject;
        return {
            path: oldConfig.path,
            object: oldConfig.object,
            afterSet: oldConfig.afterSet,
            beforeGet: oldConfig.beforeGet,
            afterGet: (m, p, v) => {
                if (oldConfig.afterGet)
                    v = oldConfig.afterGet(m, p, v);
                return this.state.value == null ? v != null ? `${v}` : null : this.state.value;
            },
            beforeSet: (m, p, v) => {
                if ((v as string || '').indexOf('.') == -1 || !(v as string || '').endsWith('.')) {
                    v = Number.parseFloat(v || '') || 0;
                    if (oldConfig.beforeSet)
                        v = oldConfig.beforeSet(m, p, v);
                    this.setState({
                        value: null
                    })
                    return v;
                } else {
                    this.setState({
                        value: v
                    })
                    return this.getValue();
                }
            }
        } as ConfigPropsObject
    }

    render() {
        return (
            <Abstract.General.Input
                {...this.props}
                keyboardType="numeric"
                config={this.getConfig()}
            />
        );
    }

    /*render() {
        let config = this.checkConfig();
        return <Input {...this.props}
            config={{
                object: config.object,
                path: `${config.path}keyboard`,
                afterGet: (m, p, v) => {
                    if (v == null) {
                        v = this.getValue();
                    } else if (config.afterGet)
                        v = config.afterGet(m, p, v);
                    return `${v || ''}`;
                },
                afterSet: config.afterSet,
                beforeGet: config.beforeGet,
                beforeSet: config.beforeSet
            }}
            onFocus={this.onFocus}
        />;
    }*/
}

const styles = StyleSheet.create({
    input: {
        width: "100%",
        height: 40 / PixelRatio.get(),
        borderBottomWidth: 2 / PixelRatio.get(),
        borderBottomColor: "#9e9e9e",
        backgroundColor: "black",
        flex: 1
    },
    inputLabel: {
        color: "#9e9e9e"
    },
    keyboardWrapper: {
        position: "absolute",
        bottom: 0,
        width: "100%",
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowColor: "black",
        shadowOpacity: 0.5,
        shadowRadius: 2
    },
    button: {
        width: "100%",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 0,
        height: 75
    },
    row: {
        borderBottomWidth: 1,
        borderBottomColor: "#e0e0e0"
    },
    col: {
        borderRightWidth: 1,
        borderRightColor: "#e0e0e0",
    },
    buttonText: {
        fontSize: 18
    }
});