import TableView from "./TableView"
import LINQTable from "./LINQTable";

export default {
    TableView, LINQTable
}