import * as React from "react";
import { LINQ } from "linq";
import { Label, Picker, View, Item } from "native-base";
import { AbstractComponent } from "./AbstractComponent";
import { PickerProperties } from "react-native";
import ArrayComponents from "./ArrayComponents";

export interface SelectFieldProps<T, K = T> extends PickerProperties {
    label?: string;
    validateStatus?: "success" | "error";
    disabled?: boolean;
    type?: "light";
    data?: linqOrArray<SelectFieldOption<T>>;
    select?: (data: T) => K;
}

type linqOrArray<T> = LINQ<T> | T[];

function isLinq<T>(data: linqOrArray<T>): data is LINQ<T> {
    return data instanceof LINQ;
}

function getLinq<T>(data: linqOrArray<T>) {
    if (isLinq(data))
        return data;
    return LINQ.fromArray(data || []);
}

export interface SelectFieldOption<T> {
    value: T;
    view: string;
    text?: string
}

export default class SelectField<T, K> extends AbstractComponent<SelectFieldProps<T, K>, {}> {

    constructor(props) {
        super(props);
    }

    onChange = (option) => {
        let data = this.convertValue(option);
        this.setValue(data.value);
    }

    convertValue = (value: string) => {
        let linq = getLinq(this.props.data);
        return linq.toArray()[parseInt(value) || 0];
    }


    renderOption = (index: number, value: SelectFieldOption<T>) => {
        return <Picker.Item key={index} value={`${index}`} label={value.view} color="#2196F3" />;
    }

    renderValue = (linq: LINQ<SelectFieldOption<T>>) => {
        let { select } = this.props;
        let value = this.getValue();
        if (value == null)
            return null;
        let linqData: LINQ<any> = linq.select(m => m.value);
        if (select == null && linq.all(m => m.value instanceof Object && 'id' in m.value && 'className' in m.value)) {
            select = (data => {
                return data['id'] as any;
            });
        }
        if (select != null) {
            value = select(value);
            linqData = linqData.select(select);
        }
        let index = linqData.toArray().indexOf(value);
        if (index == -1)
            return null;
        return `${index}`;
    }

    render() {
        let { label, select, type } = this.props;
        let linq = getLinq(this.props.data);
        let value = this.renderValue(linq);
        let status = this.props.validateStatus == "error" ? { error: true } : { success: true };
        return (
            <Item style={{ marginBottom: 15, borderColor: "#2196F3" }}>
                <View style={{ flex: 1, flexDirection: "column" }}>
                    <Label style={{ color: type == "light" ? "white" : "#616161" }}>{this.props.label}</Label>
                    <Picker
                        mode="dropdown"
                        onValueChange={this.onChange}
                        selectedValue={value}
                        placeholder={label}
                    >
                        {
                            ArrayComponents.render({
                                template: this.renderOption,
                                elements: linq
                            })
                        }
                    </Picker>
                </View>
            </Item>
        );
    }
}