import * as React from 'react';
import { LINQ } from "linq";

export type ObjectOrPromiseOfObject<T> = T | Promise<T>;

export type ConfigPropsObject = {
    object: any,
    path?: string,
    beforeGet?: (object: any, path: string) => ObjectOrPromiseOfObject<void>;
    afterGet?: (object: any, path: string, value: any) => any;
    beforeSet?: (object: any, path: string, value: any) => ObjectOrPromiseOfObject<any>;
    afterSet?: (object: any, path: string, value: any) => ObjectOrPromiseOfObject<void>;
};

export type ConfigPropsWithType = ConfigPropsObject & { type: number }

export interface AbstractComponentProps {
    config?: ConfigPropsObject;
    configs?: ConfigPropsWithType[];
}

export class AbstractComponent<P = {}, S = {}> extends React.Component<P & AbstractComponentProps, S>{

    private getConfigPropsWithType(attribute?: number) {
        let configProps = this.props.config;
        let configsProps = this.props.configs;
        let config: ConfigPropsWithType = null;
        if (attribute == null) {
            if (configsProps) {
                let linq = LINQ.fromArray(configsProps || []);
                let count = linq.count();
                if (count == 1)
                    config = configProps[0];
                else {
                    config = linq.single(m => m.type == null);
                }
            } else {
                config = configProps as ConfigPropsWithType;
            }
        } else {
            if (configsProps) {
                let linq = LINQ.fromArray(configsProps || []);
                config = linq.single(m => m.type == attribute);
            } else {
                throw new Error('you trying to get value by attribute - index of Array, but got Object in AbstractComponent');
            }
        }
        return config;
    }

    checkConfig() {
        try {
            return this.getConfigPropsWithType();
        } catch (err) {
            return null;
        }
    }

    async setValue(value: any, attrName?: number, path?: string) {
        let config = this.getConfigPropsWithType(attrName);
        if (config.beforeSet)
            value = await config.beforeSet(config.object, path || config.path, value);
        setValue(config.object, path || config.path, value);
        if (config.afterSet)
            await config.afterSet(config.object, path || config.path, value);
    }

    getValue(attrName?: number, path?: string) {
        let config = this.getConfigPropsWithType(attrName);
        if (config.beforeGet)
            config.beforeGet(config.object, path || config.path);
        let value = getValue(config.object, path || config.path);
        if (config.afterGet)
            value = config.afterGet(config.object, path || config.path, value);
        return value;
    }
}

// & {
//     [val: number]: accessorsType<S, V>;
// }



// Не доделано еще
/*export function FormItem<T>(Component: React.ComponentClass<T>) {
    let component = class FormItem extends React.Component<T & AbstractComponentProps, any>{
        constructor() {
            super();
        }
        render() {
            return <Item><Component {...this.props} /></Item>;
        }
    };
    return pure(component);
}*/

function setValue(obj: any, path: string, value: any) {
    if (!path) {
        obj = value;
        return;
    }
    let paths = path.split('.');
    let i = 0;
    for (i = 0; i < paths.length - 1; i++)
        obj = obj[paths[i]];
    obj[paths[i]] = value;
}

function getValue(obj: any, path: string) {
    if (!path)
        return obj;
    let paths = path.split('.');
    for (let i = 0; i < paths.length; i++)
        obj = obj[paths[i]];
    return obj;
}