import * as React from "react";
import { Item, Label, Input } from "native-base";
import { View, StyleSheet, TouchableOpacity, Text } from "react-native";
import { AbstractComponent } from "./AbstractComponent";
import { TextInputProperties, TouchableWithoutFeedback, Keyboard } from "react-native";

export interface ViewProps extends TextInputProperties {
    label?: string;
    onEdit?: (path: string) => void;
}

interface State {
    focused: boolean;
}

export default class ViewComponent extends AbstractComponent<ViewProps, State> {

    constructor(props) {
        super(props);
        this.state = {
            focused: false
        }
    }

    onFocus = () => {
        this.setState({ focused: true });
        this.props.onEdit && this.props.onEdit(this.props.config && this.props.config.path);
    }

    onBlur = () => {
        this.setState({ focused: false });
    }

    render() {
        let { label } = this.props;
        let config = this.checkConfig();
        let value = this.getValue(null, `${config.path}keyboard`);
        if (value == null) {
            value = this.getValue();
        }
        return (
            <TouchableOpacity onPressOut={this.onBlur} onPress={this.onFocus} activeOpacity={1}>
                <View style={styles.input}>
                    <Text style={styles.floatedLabel}>{label || ''}</Text>
                    <Text style={styles.value}>{`${value == null ? '' : value}`}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    floatedLabel: {
        color: "#616161",
        fontSize: 15,
        position: "absolute",
        top: 0
    },
    label: {
        color: "#616161",
        fontSize: 18,
        position: "absolute",
        bottom: 0
    },
    value: {
        fontSize: 18,
        color: "black",
        position: 'absolute',
        top: 18
    },
    input: {
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: "#ccc",
        position: "relative",
        marginBottom: 10,
        marginLeft: 12,
        marginRight: 12,
        marginTop: 10
    }
});