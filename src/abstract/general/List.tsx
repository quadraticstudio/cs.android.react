import * as React from "react";
import { View, List, ListItem, Content, Button, Icon } from 'native-base';
import { ViewStyle } from "react-native";
import { LINQ } from "linq";
import ArrayComponents from "./ArrayComponents";

export interface ViewProps<T> {
    style?: ViewStyle;
    items: T[];
    renderRow: (index: number, object: T) => React.ReactNode;
    onClick?: (index: number, object: T) => any;
    leftSwipe?: (index: number, object: T) => any;
    rightSwipe?: (index: number, object: T) => any;
}

export default class ViewComponent<T> extends React.Component<ViewProps<T>, any> {

    renderRow = (index: number, object: T) => {
        let { renderRow, onClick } = this.props;
        return (
            <ListItem
                key={index}
                itemDivider={object["divider"]}
                button={!!onClick}
                style={{ backgroundColor: "#070d19", borderBottomColor: "#00a5e7" }}
                onPress={onClick ? () => onClick(index, object) : null}
            >
                {renderRow(index, object)}
            </ListItem>
        );
    }

    // renderRightHiddenRow={(data, secId, rowId, rowMap) => {
    //     return <Button full onPress={() => console.log(data, secId, rowId, rowMap)}>
    //         <Icon active name="information-circle" />
    //     </Button>;
    // }}

    render() {
        let backColor = this.props.style && this.props.style.backgroundColor || '#070d19';
        return (
            <Content style={{ width: '100%', flex: 1, backgroundColor: backColor }}>
                <List
                    style={{
                        backgroundColor: backColor,
                        ...(this.props.style || {} as any)
                    }}
                >
                    {ArrayComponents.render({
                        elements: this.props.items || [],
                        template: this.renderRow
                    })}
                </List>
            </Content>
        );
    }
}