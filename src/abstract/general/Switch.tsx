import * as React from "react";
import { Item, Label, Input, Text, View } from "native-base";
import { AbstractComponent } from "./AbstractComponent";
import { TextInputProperties, TouchableWithoutFeedback, Keyboard, Switch } from "react-native";

export interface ViewProps extends TextInputProperties {
    label?: string;
    // preventKeyboard?: boolean;
    // validateStatus?: "success" | "error";
    disabled?: boolean;
    textStyle?: any;
}

export default class ViewComponent extends AbstractComponent<ViewProps, any> {

    onChange = (value: boolean) => {
        this.setValue(value);
    }

    render() {
        let value = this.getValue();
        return (
            <View style={{flex: 1, flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                {this.props.label && <Text style={{maxWidth: "80%", ...this.props.textStyle}}>{ this.props.label }</Text>}
                <Switch style={{backgroundColor: "black"}} tintColor="#9E9E9E" onTintColor="#42A5F5" thumbTintColor="#2196F3" {...this.props} value={value} onValueChange={this.onChange}/>
            </View>
        );
    }
}