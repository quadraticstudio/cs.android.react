import * as React from "react";
import { Item, Label, Input, View, Text, Grid, Row, Col, Button } from "native-base";
import { AbstractComponent } from "./AbstractComponent";
import { Keyboard, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, PixelRatio } from "react-native";
import Abstract from "../";
import { LINQ } from "linq";

const dismissKeyboard = require("react-native-dismiss-keyboard");

export interface ViewProps {
    additionalItems?: KeyboardItem[];
    header?: KeyboardItem[][];
}

interface ViewState {
}

interface KeyboardItem {
    color?: string;
    onClick?: () => void;
    label?: string | number;
}

export default class ViewComponent extends AbstractComponent<ViewProps, ViewState> {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    onChange = async (value: string) => {
        let config = this.checkConfig();
        await this.setValue(parseFloat(value || '0') || 0);
        await this.setValue(value, null, `${config.path}keyboard`);
    }

    prepare = (items: KeyboardItem[]) => {
        let { header } = this.props;
        let count = items.length;
        let cols: KeyboardItem[][] = [];
        let toCount = count > 12 ? 12 : count;
        let counter = 0;
        /*if (header) {
            for (let i = 0; i < header.length; i++) {
                if (header[i] && header[i].length > 0)
                    cols[i] = header[i].slice(0, header[i].length - 1);
            }
        }*/
        for (let i = 0; i < toCount; i += 4) {
            cols[i] = (cols[i] || []).concat(items.slice(i, i + 4));
        }
        for (let i = 0; i < (count - toCount); i++) {
            cols[toCount + i] = (cols[toCount + i] || []).concat(items.slice(toCount + i, toCount + i + 1));
        }
        return cols;
    }

    prepareCols = () => {
        let { additionalItems } = this.props;
        let items = [1, 4, 7, '.', 2, 5, 8, 0, 3, 6, 9, '<'];
        let keyboardItems = LINQ.fromArray(items).select(m => {
            return {
                label: `${m}`,
                onClick: () => this.onKeyClick(m)
            } as KeyboardItem;
        }).toArray();
        if (additionalItems)
            keyboardItems = keyboardItems.concat(additionalItems);
        return this.prepare(keyboardItems);
    }

    onKeyClick = (v: string | number) => {
        let config = this.checkConfig();
        console.log('a = ',this.getValue(),'type = ',typeof this.getValue())
        let value = `${(this.getValue() || this.getValue(null, `${config.path}keyboard`) || '')}`;
        if (typeof v == 'number') {
            value = `${value}${v}`;
        } else {
            if (v == '.' && value.indexOf('.') == -1) {
                value = `${value}.`;
            } else if (v == '<') {
                value = `${value.slice(0, value.length - 1)}`;
            }
        }
        this.onChange(value);
    }

    renderKeyboard = () => {
        // let items: KeyboardItem[] = [
        //     { label: "1", onClick: () => this.onKeyClick("1") },
        //     { label: "4", onClick: () => this.onKeyClick("4") },
        //     { label: "7", onClick: () => this.onKeyClick("7") },
        //     { label: ".", onClick: () => this.onKeyClick(".") },
        //     { label: "2", onClick: () => this.onKeyClick("2") },
        //     { label: "5", onClick: () => this.onKeyClick("5") },
        //     { label: "8", onClick: () => this.onKeyClick("8") },
        //     { label: "0", onClick: () => this.onKeyClick("0") },
        //     { label: "3", onClick: () => this.onKeyClick("3") },
        //     { label: "6", onClick: () => this.onKeyClick("6") },
        //     { label: "9", onClick: () => this.onKeyClick("9") },
        //     { label: "<", onClick: () => this.clear() },
        //     { label: "сброс", onClick: () => this.clear(), color: "#0091EA" },
        //     // { label: "+1", onClick: () => this.incrementValue(), color: "#0091EA" },
        //     // { label: "ввод", onClick: () => this.enter(), color: "#0091EA" },
        //     // { label: "ок", onClick: () => this.ok(), color: "#0091EA" }
        // ];
        let { header } = this.props;
        let cols = this.prepareCols();
        return (
            <TouchableWithoutFeedback>
                <View style={styles.keyboardWrapper}>
                    <Grid>
                        {header && header.length > 0 ?
                            <Row>
                                {header.map((col, i) => {
                                    return <Col key={i}>{
                                        col.map((item, i: number) => {
                                            return (
                                                <Button key={i} light style={styles.button} onPress={item.onClick}>
                                                    <Text
                                                        style={[styles.buttonText, { color: item.color || "black" }]}
                                                    >{item.label}</Text>
                                                </Button>
                                            );
                                        })
                                    }
                                    </Col>;
                                })}
                            </Row>
                            : null}
                        <Row>
                            {cols.map((col: KeyboardItem[], i: number) => {
                                return <Col key={i}>{
                                    col.map((item, i: number) => {
                                        return (
                                            <Button key={i} light style={styles.button} onPress={item.onClick}>
                                                <Text
                                                    style={[styles.buttonText, { color: item.color || "black" }]}
                                                >{item.label}</Text>
                                            </Button>
                                        );
                                    })
                                }
                                </Col>;
                            })}
                        </Row>
                    </Grid>
                </View>
            </TouchableWithoutFeedback>
        );
    }

    render() {
        return (
            this.renderKeyboard()
        );
    }
}

const styles = StyleSheet.create({
    input: {
        width: "100%",
        height: 40 / PixelRatio.get(),
        borderBottomWidth: 2 / PixelRatio.get(),
        borderBottomColor: "#9e9e9e",
        backgroundColor: "black",
        flex: 1
    },
    inputLabel: {
        color: "#9e9e9e"
    },
    keyboardWrapper: {
        position: "relative",
        width: "100%",
        shadowOffset: {
            width: 0,
            height: 5
        },
        flex: 1,
        shadowColor: "black",
        shadowOpacity: 0.5,
        shadowRadius: 2
    },
    button: {
        width: "100%",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 0,
        height: 50
    },
    row: {
        borderBottomWidth: 1,
        borderBottomColor: "#e0e0e0"
    },
    col: {
        borderRightWidth: 1,
        borderRightColor: "#e0e0e0",
    },
    buttonText: {
        fontSize: 18
    }
});