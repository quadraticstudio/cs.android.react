import * as React from 'react';
import { View } from "native-base";
import { LINQ } from "linq";

type elementType = JSX.Element | JSX.Element[];

interface ArrayComponentsProps<T> {
    template: (index?: number, object?: T, ...args) => elementType | Promise<elementType>
    elements: LINQ<T> | T[],
    parent?: JSX.Element,
    args?: any[]
}

interface ArrayComponentsState {

}

function getLINQ<T>(linqOrArray: LINQ<T> | T[]) {
    if (linqOrArray instanceof LINQ)
        return linqOrArray;
    return LINQ.fromArray(linqOrArray || []);
}

export default class ArrayComponents<T> extends React.Component<ArrayComponentsProps<T>, ArrayComponentsState> {

    private static renderTemplates<T>(props: ArrayComponentsProps<T>) {
        let { elements, template, args } = props;
        return getLINQ(elements).selectMany((m, i) => {
            let item = template(i, m, ...(args || [])) as JSX.Element | JSX.Element[];
            return item instanceof Array ? item : [item];
        }).toArray();
    }

    private static async renderTemplatesAsync<T>(props: ArrayComponentsProps<T>) {
        let { elements, template, args } = props;
        let temp = await Promise.all(getLINQ(elements).select(async (m, i) => {
            let item = await (template(i, m, ...(args || [])) as Promise<JSX.Element | JSX.Element[]>);
            return item instanceof Array ? item : [item];
        }).toArray());
        return getLINQ(temp).selectMany(m => m).toArray();
    }

    static render<T>(props: ArrayComponentsProps<T>) {
        return this.renderTemplates(props);
    }

    static async renderAsync<T>(props: ArrayComponentsProps<T>) {
        return this.renderTemplatesAsync(props);
    }

    private renderTemplates() {
        return ArrayComponents.renderTemplates(this.props);
    }

    render() {
        let { parent } = this.props;
        let renderData = ArrayComponents.render(this.props);
        if (parent)
            return React.cloneElement(parent, {}, renderData);
        else {
            return (
                <View>
                    {
                        renderData
                    }
                </View>);
        }
    }

}