import * as React from "react";
import { Item, Label, Input } from "native-base";
import { AbstractComponent } from "./AbstractComponent";
import { TextInputProperties, StyleSheet, TouchableWithoutFeedback, Keyboard, ViewStyle } from "react-native";

export interface ViewProps extends TextInputProperties {
    label?: string;
    preventKeyboard?: boolean;
    validateStatus?: "success" | "error";
    disabled?: boolean;
    itemStyle?: ViewStyle;
    type?: "light"
}

export default class ViewComponent extends AbstractComponent<ViewProps, any> {

    onChange = (value: string) => {
        this.setValue(value);
    }

    render() {
        let status = this.props.validateStatus == "error" ? { error: true } : { success: true };
        let value = this.getValue();
        value = value && `${value}`
        return (
            <Item floatingLabel={!!this.props.label} {...status} style={styles.item}>
                {this.props.label && <Label style={{ color: this.props.type == "light" ? "white" : "#757575" }}>{this.props.label}</Label>}
                <Input selectionColor="#2196F3" {...this.props} onChangeText={this.onChange} value={value} />
            </Item>
        );
    }
}

const styles = StyleSheet.create({
    light: {
        color: "white"
    },
    item: {
        marginBottom: 15,
        borderColor: "#2196F3"
    }
});