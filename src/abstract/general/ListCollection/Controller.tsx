import * as React from "react";
import * as Parse from "parse";
import { pure } from "recompose";
import { LINQ } from "linq";
import ListView, { RowTable } from "./View"

type objectOrPromise<T> = T | Promise<T>;
type linqOrArray<T> = LINQ<T> | T[];

function getLinq<T>(linqArray: linqOrArray<T>) {
    if (linqArray instanceof LINQ)
        return linqArray;
    return LINQ.fromArray(linqArray || []);
}

interface LINQProps<T, K> {
    data: objectOrPromise<linqOrArray<T>>;
    header: RowTable<T | K>[];
    prepareItem?: (object: T, index: number) => objectOrPromise<K>
    onClick?: (index: number, data: T) => void;
}

interface LINQState<T, K> {
    loading: boolean;
    allData: LINQ<T>;
    showData: K[];
}

class LINQTable_<T, K = T> extends React.Component<LINQProps<T, K>, LINQState<T, K>>{
    constructor(props: LINQProps<T, K>) {
        super(props);
        this.state = {
            loading: false,
            allData: LINQ.fromArray([]),
            showData: []
        };
    }

    componentDidMount() {
        this.loadData(this.load)
    }

    componentWillReceiveProps(nextProps: LINQProps<T, K>) {
        if (nextProps.data !== this.props.data)
            this.loadData(() => this.load(nextProps));
    }

    load = async (nextProps?: LINQProps<T, K>) => {
        let { data, prepareItem } = nextProps || this.props;
        if (data == null)
            return;
        let { allData, showData } = this.state;
        let temp = await data;
        allData = getLinq(temp);
        showData = await this.loadPage(allData, nextProps);
        this.setState({
            allData, showData
        })
    }

    loadPage = async (allData: LINQ<T>, nextProps?: LINQProps<T, K>) => {
        let { prepareItem } = nextProps || this.props;
        let { showData } = this.state;
        if (prepareItem)
            showData = await Promise.all(allData.select((m, i) => prepareItem(m, i)).toArray());
        else
            showData = allData.toArray() as any as K[];
        return showData;
    }

    async loadData<I>(promise: () => (I | Promise<I>)) {
        let loading = (loading: boolean) => new Promise<void>((resolve) => this.setState({ loading }, resolve));
        let result: I = null;
        await loading(true);
        result = await promise();
        await loading(false);
        return result;
    }

    // VIEW

    render() {
        let { showData, loading } = this.state;
        return (
            <ListView
                loading={loading}
                dataSource={showData || []}
                header={this.props.header || []}
                onClick={this.props.onClick}
            />
        );
    }
}

export default pure(LINQTable_);