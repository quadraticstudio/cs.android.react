import * as React from "react";
import { pure } from "recompose";
import { StyleSheet, PixelRatio, ViewStyle } from "react-native";
import { Text, View, List, ListItem } from "native-base";
import { LINQ } from "linq";
import * as moment from "moment";
require('moment/locale/ru');
import { loadingJSXElement } from "../../global/Messages";
import Abstract from "../../../abstract"
import ArrayComponents from "../ArrayComponents";

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{0,3}Z$/;

export interface RowTable<T> {
    title?: React.ReactNode;
    render?: (obj: T, index: number) => React.ReactNode;
    style?: ViewStyle
}

export interface TableViewProps<T> {
    dataSource?: T[];
    loading?: boolean;
    header?: RowTable<T>[];
    onClick?: (index: number, obj: T) => void;
}

interface TableViewState {
}

class TableView_<T> extends React.Component<TableViewProps<T>, TableViewState>{
    constructor(props: TableViewProps<T>) {
        super(props);
    }

    isValidDate = (value: Date) => {
        if (Object.prototype.toString.call(value) === "[object Date]") {
            if (isNaN(value.getTime())) {
                return false;
            }
            else {
                return true;
            }
        }
        return false;
    }

    prerender = (value: React.ReactNode, stringForce?: boolean): string | JSX.Element => {
        if (stringForce)
            return <Text style={styles.header}>{value}</Text>;
        if (value == null)
            return this.prerender('-', true);
        let type = typeof value;
        if (value instanceof Date)
            return this.prerender(moment(value as Date).locale("ru").format('DD MMM YYYY HH:mm'), true)
        else if (type === 'string') {
            if (dateFormat.test(value as string)) {
                let testDate = new Date(value as string);
                if (this.isValidDate(testDate))
                    return this.prerender(testDate);
            }
            return this.prerender(value, true);
        }
        else if (type === 'number')
            return this.prerender(`${value}`, true);
        else if (type === 'boolean')
            return this.prerender(value == true ? 'Да' : 'Нет', true);
        else
            return value as JSX.Element;
    }

    renderHeaderRow = (index: number, row: RowTable<T>, obj: T, indexItem: number) => {
        return (
            <View
                key={index}
                style={styles.rowItem}
            >
                <View style={styles.colItem}>
                    {this.prerender(row.title)}
                </View>
                <View style={styles.colItem}>
                    {this.prerender(row.render(obj, indexItem))}
                </View>
            </View>
        );
    }

    renderItems = () => {
        let { header, dataSource } = this.props;
        return <Abstract.General.List
            items={dataSource || []}
            onClick={this.props.onClick}
            renderRow={(i, obj) => {
                return (
                    <View style={{
                        flexDirection: "column",
                        width: '100%',
                        flex: 1
                    }}>
                        {
                            ArrayComponents.render({
                                elements: header || [],
                                template: this.renderHeaderRow,
                                args: [obj, i]
                            })
                        }
                    </View>
                )
            }}
        />
    }

    renderTable = () => {
        let height = (80 / PixelRatio.get()) * (this.props.header || []).length;
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: '#fff' }}>
                {loadingJSXElement(this.renderItems(), this.props.loading)}
            </View>
        );
    }

    render() {
        return this.renderTable();
    }
}

const styles = StyleSheet.create({
    table: {
        backgroundColor: "black"
    },
    listItem: {
        marginLeft: 0,
        paddingLeft: 17,
        height: "auto"
    },
    header: {
        fontWeight: "500",
        color: "white",
        fontSize: 16,
    },
    item: {

    },
    rowHeader: {
        flex: 1,
        alignItems: "flex-start",
        flexDirection: "row",
        height: 80 / PixelRatio.get(),
        backgroundColor: "#23344e",
        borderStyle: 'solid',
        borderColor: '#00a5e7'
    },
    colHeader: {
        flex: 1,
        alignItems: "flex-start",
        flexDirection: "column",
        justifyContent: "center",
        height: 80 / PixelRatio.get(),
        borderStyle: 'solid',
        borderColor: '#00a5e7',
        paddingLeft: 5
    },
    rowItem: {
        flex: 1,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: 'center'
    },
    colItem: {
        flex: 1,
        alignItems: "flex-start",
        flexDirection: "column",
        justifyContent: "center"
    }
});

export default pure(TableView_);