import * as React from "react";
import { Modal as ModalNative, ModalProperties, StyleSheet, TouchableOpacity, TouchableWithoutFeedback } from "react-native";
import { View, Text, Card, CardItem } from "native-base";
import Abstract from "../";

interface ModalProps extends ModalProperties {
    title?: React.ReactNode;
    content: React.ReactNode;
    footer?: React.ReactNode;
    anchor?: React.ReactNode;
}

interface ModalState {
    visible: boolean;
    contentHeight?: number;
}

export default class Modal extends React.Component<ModalProps, ModalState> {

    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }

    onRequestClose = () => {
        this.setState({ visible: false });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ visible: nextProps.visible });
    }

    setContentHeight = (layout) => {
        this.setState({ contentHeight: layout.height });
    }

    render() {
        let visible = this.props.visible;
        return (
            <ModalNative
                {...this.props}
                animationType="fade"
                visible={this.state.visible}
                transparent
                onRequestClose={this.onRequestClose}
            >
                <TouchableOpacity style={styles.modal} activeOpacity={1} onPressOut={this.onRequestClose}>
                    <View style={{ width: "80%", height: (this.state.contentHeight || 0) + 130, maxHeight: 400 }}>
                        <TouchableWithoutFeedback>
                            <Card style={styles.card}>
                                {this.props.title && <CardItem style={styles.header}>
                                    <Text style={styles.headerText}>{this.props.title}</Text>
                                </CardItem>}
                                <CardItem style={{width: "100%"}}>
                                    <View onLayout={(e) => this.setContentHeight(e.nativeEvent.layout)} style={styles.content}>
                                        {this.props.content}
                                    </View>
                                </CardItem>
                            </Card>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableOpacity>
            </ModalNative>
        );
    }

}

const styles = StyleSheet.create({
    button: {
        flex: 1,
        justifyContent: "flex-end",
        flexDirection: "row",
        width: "100%",
        position: "absolute",
        bottom: 0,
    },
    modal: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0, 0, 0, 0.3)"
    },
    header: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 15,
        paddingLeft: 15
    },
    card: {
        padding: 10,
        height: "auto"
    },
    headerText: {
        fontSize: 18,
        fontWeight: "900"
    },
    content: {
        width: "100%"
    }
});