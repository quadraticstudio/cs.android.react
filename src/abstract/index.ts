import NavBar from "./navbar/index"
import General from "./general"
import Routers from "./routers"
import Content from "./content"
import Debug from "./debug";
import * as Global from "./global"
import Tiles from "./tiles"
import Goods from "./goods"
import Database from "./database"
import Special from "./special";

export default {
    Tiles, NavBar, General, Routers, Global, Content, Debug, Goods, Database, Special
}