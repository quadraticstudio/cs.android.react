import * as DbBerish from "db-berish";
import * as Parse from "parse";
import { LINQ } from "linq";
import { DbConfig } from "db-berish/lib/adapter";
import LocalObject, { TypeLocalObject } from "./object";
import { Worker, Job } from "./worker";
import { Queue } from "./queue";
import Query from "./query";
import Task from "./task";
import { Emitter } from "./emitter";

export interface IParseConfig {
    serverUrl: string;
    appId: string;
    jsKey: string;
}
type poo<T> = T | Promise<T>;
type foo<T> = T | (() => T)
export interface IDbAdapterConfig {
    condition?: poo<boolean> | foo<boolean> | foo<poo<boolean>>
}

export type ctorType = {
    berish?: TypeLocalObject,
    parse?: typeof Parse.Object,
    isLocalOnly?: boolean
}

export default class LocalAdapter {
    public static ctors: { [className: string]: ctorType } = {};
    private static _instance: LocalAdapter = null;
    private static _emitter: Emitter = null;
    //private tasks: (() => poo<boolean>)[] = [];

    public dbAdapter: DbBerish.Adapter = null;
    public config: IDbAdapterConfig = null;
    public worker: Worker = null;
    public queue: Queue = null;
    public condition: boolean = false;
    public currentUser: Parse.User = null;

    constructor(berish: DbConfig, parse: IParseConfig, config: IDbAdapterConfig) {
        this.dbAdapter = DbBerish.Adapter.init(berish).instance;
        this.config = config;
        this.worker = new Worker(this);
        this.worker.addJob(new Job('checkCondition', this.checkCondition, 1000));
        //this.worker.addJob(new Job('task runner', this.taskRunner, 1000));
        this.worker.start();
        this.queue = new Queue();
        LocalAdapter.initLocalObject(Task, 'Task')
        Parse.initialize(parse.appId, parse.jsKey);
        Parse!.serverURL = parse.serverUrl;
    }

    static async init(berish: DbConfig, parse: IParseConfig, config: IDbAdapterConfig) {
        LocalAdapter._instance = new LocalAdapter(berish, parse, config || {});
        await LocalAdapter.instance.checkCondition();
        return LocalAdapter.instance;
    }

    /*addTask(cb: () => poo<boolean>) {
        this.tasks.push(cb);
    }*/

    private async checkCondition() {
        if (this.config.condition != null) {
            let result = typeof this.config.condition === 'function' ? await this.config.condition() : !!this.config.condition;
            this.condition = result;
        }
    }

    /*private async taskRunner() {
        let previousTasks = this.tasks || [];
        this.tasks = [];
        previousTasks = await Promise.all(LINQ.fromArray(previousTasks).select(async m => {
            let result = await m();
            result = result != null ? !!result : true;
            if (result) {
                return null;
            }
            return m;
        }).toArray());
        this.tasks = LINQ.fromArray(previousTasks).notNull().concat(this.tasks).toArray();
    }*/

    private static ctorParse(className: string) {
        return class ParseObject extends Parse.Object {
            constructor() {
                super(className);
            }
        }
    }

    static initLocalObject(ctor: typeof LocalObject, className?: string, isLocalOnly?: boolean) {
        className = className || ctor.name;
        let parseCtor = this.ctorParse(className);
        let berishCtor: TypeLocalObject = ctor;
        berishCtor.parse = parseCtor;
        DbBerish.Adapter.initDBObject(berishCtor, className);
        Parse.Object.registerSubclass(className, parseCtor);
        if (LocalAdapter.ctors[className] == null)
            LocalAdapter.ctors[className] = {
                berish: berishCtor,
                parse: parseCtor,
                isLocalOnly: !!isLocalOnly
            }
        return berishCtor;
    }

    public static get instance() {
        if (this._instance == null)
            throw new Error('instance DB not initialized')
        return this._instance;
    }

    public static get emitter() {
        if (this._emitter == null)
            this._emitter = new Emitter();
        return this._emitter;
    }

    // async reloadData() {
    //     let parseLoad = async () => {
    //         for (let className in LocalAdapter.ctors) {
    //             let query = await new Query(className).find();
    //         }
    //     }
    //     /*await parseLoad();
    //     await this.queue.do();*/
    //     return this.queue.showTasks();
    // }
}

