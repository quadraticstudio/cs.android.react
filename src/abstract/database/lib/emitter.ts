export type FunctionType = (...args) => any;

export interface EmitterListenConfig {
    isAsync?: boolean;
    cb: FunctionType;
}

export class Emitter {

    private events: { [property: string]: EmitterListenConfig[] } = {};

    constructor() {

    }

    listen(eventName: string, config: EmitterListenConfig) {
        let events = this.events[eventName] || [];
        events.push(config);
        this.events[eventName] = events;
        return () => {
            this.events[eventName] = this.events[eventName].filter(e => e != config);
        }
    }

    async emit(eventName: string, ...args) {
        const events = this.events[eventName];
        if (events && events.length) {
            for (let event of events) {
                if (event.isAsync)
                    await event.cb(...args);
                else
                    setImmediate(() => event.cb(...args));
            }
        }
        return;
    }
}