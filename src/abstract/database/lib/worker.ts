import { LINQ } from "linq";
import { Utils } from "db-berish";

export class Job {
    name: string;
    timer: NodeJS.Timer;
    cb: () => void;
    isBusy: boolean;
    period: number;

    constructor(name: string, cb: () => void, period?: number) {
        this.name = name || Utils.guid();
        this.cb = cb;
        this.period = period;
    }

    start(context?: any) {
        if (this.timer != null)
            return;
        this.timer = setInterval(async () => {
            if (this.isBusy)
                return;
            this.isBusy = true;
            if (this.cb) {
                let func = this.cb;
                if (context)
                    func = func.bind(context);
                try {
                    await func();
                } catch (err) {

                }
            }
            this.isBusy = false;
        }, this.period || 1000)
    }

    stop() {
        if (this.timer == null)
            return;
        clearInterval(this.timer);
        this.timer = null;
    }
}

export class Worker {
    private jobs: {
        [name: string]: Job
    } = {};
    private context = null;

    constructor(context: any) {
        this.context = context;
    }

    addJob(...jobs: Job[]) {
        for (let job of (jobs || [])) {
            if (Object.keys(this.jobs || {}).indexOf(job.name) != -1)
                this.removeJob(job.name);
            this.jobs[job.name] = job;
        }
    }

    removeJob(...jobs: string[]) {
        for (let name of (jobs || [])) {
            let job = this.jobs[name];
            if (!job)
                return;
            if (job.timer || job.isBusy)
                job.stop();
            this.jobs[name] = null;
        }
    }

    start() {
        let names = Object.keys(this.jobs);
        let jobs = LINQ.fromArray(names).select(m => this.jobs[m]);
        jobs.select(m => {
            m.start(this.context);
        })
    }

    restart() {
        this.stop();
        this.start();
    }

    stop() {
        let names = Object.keys(this.jobs);
        let jobs = LINQ.fromArray(names).select(m => this.jobs[m]);
        jobs.select(m => {
            m.stop();
        })
    }
}