import * as Parse from "parse";
import { LINQ } from "linq";
import LocalAdapter from "./adapter";
import { Job } from "./worker";
import { toLocal } from "./converter";
import Query from "./query";
import LocalObject from "./object";

interface LocalRelation {
    targetClassName: string;
    id: string[];
}

export default class Relation<T extends LocalObject> {
    parent: LocalObject = null;
    key: string = null;
    _localRelation: LocalRelation = null;

    constructor(parent: LocalRelation);
    constructor(parent: LocalObject, key: string);
    constructor(parent: LocalObject | LocalRelation, key?: string) {
        if (parent instanceof LocalObject) {
            this.parent = parent;
        } else
            this._localRelation = parent;
        this.key = key;
    }

    private get localRelation() {
        let value = this._localRelation || this.parent.get(this.key) as LocalRelation;
        if (!value) {
            value = {
                targetClassName: null,
                id: []
            };
            this.localRelation = value;
        }
        return value;
    }

    private set localRelation(value: LocalRelation) {
        if (this.parent != null)
            this.parent.set(this.key, value);
        this._localRelation = value;
    }

    get ids() {
        return this.localRelation.id || [];
    }

    add(object: T) {
        let localRelation = this.localRelation;
        let className = localRelation.targetClassName;
        if (object && object.id) {
            if (!className)
                className = object.className;
            localRelation.id = LINQ.fromArray(localRelation.id || []).concat(object.id).distinct().toArray();
            localRelation.targetClassName = className;
            this.localRelation = localRelation;
        }
    }

    //call as true
    remove(object: T) {
        let localRelation = this.localRelation;
        if (object && object.id) {
            localRelation.id = LINQ.fromArray(localRelation.id || []).where(m => m != object.id).toArray();
            this.localRelation = localRelation;
        }
    }

    async find() {
        let className = this.localRelation.targetClassName;
        if (!className)
            return LINQ.fromArray<T>([]);
        let localQuery = new Query(className);
        let items = await localQuery.find() as LINQ<T>;
        if (this.localRelation.id && this.localRelation.id.length > 0) {
            let ids = LINQ.fromArray(this.localRelation.id || []);
            items = items.notNull().where(m => ids.contains(m.id));
            return items;
        }
        return LINQ.fromArray<T>([]);
    }
}