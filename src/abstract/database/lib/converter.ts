import LocalObject from "./object";
import { LINQ } from "linq";
import LocalAdapter from "./adapter";
import * as DbBerish from "db-berish";
import * as Parse from "parse";
import Relation from "./relation";

export const ignoreKeys = [
    'id',
    'className'
];

export async function fetchLocal(item: LocalObject) {
    let keys = LINQ.fromArray(Object.keys(item['_collection'] || {}) || []);
    keys = keys.except(ignoreKeys);
    for (let key of keys.toArray()) {
        let value: LocalObject = item.get(key);
        if (value instanceof LocalObject && value.fetch != null) {
            value = await value.fetchLocal();
            item.set(key, value);
        }
    }
    return item;
}

export async function toLocal(from: Parse.Object, to?: LocalObject) {
    const _toLocal = async (from: Parse.Object, to: LocalObject, isRecursive: boolean) => {
        if (!to) {
            let ctor = LocalAdapter.ctors[from.className];
            to = (ctor && new ctor.berish()) || new LocalObject(from.className);
        }
        let keys = LINQ.fromArray(Object.keys(from.attributes || {}) || []);
        for (let key of keys.toArray()) {
            let oldValue = to.get(key);
            let newValue = from.get(key);
            if (newValue instanceof Parse.Object && isRecursive)
                newValue = await _toLocal(newValue, null, false);
            else if (newValue instanceof Parse.Relation) {
                let ids = await newValue.query().select('objectId').find() as Parse.Object[];
                newValue = {
                    targetClassName: newValue.targetClassName,
                    id: LINQ.fromArray(ids || []).select(m => m && m.id).notNull().toArray()
                };
                //newValue = new Relation(to, newValue.key);
            } else if (newValue instanceof Object && 'id' in newValue && 'className' in newValue) {
                let ctor = LocalAdapter.ctors[newValue.className];
                let local = (ctor && new ctor.berish()) || new LocalObject();
                for (let key in newValue) {
                    local.set(key, newValue[key]);
                }
                local.id = newValue.id;
                local.className = newValue.className;
                newValue = local;
            }
            if (oldValue != newValue) {
                to.set(key, newValue);
            }
        }
        to.id = from.id || from['objectId'];
        to.className = from.className || from['className'];
        return to;
    }
    return _toLocal(from, to, true);
}

export async function toParse(from: LocalObject, to?: Parse.Object, isNew?: boolean) {
    if (!to) {
        //let ctor = LocalAdapter.ctors[from.className];
        to = new Parse.Object(from.className);
    }
    if (!to.className) {
        to.className = from.className;
    }
    let keys = LINQ.fromArray(Object.keys(from.keyValueCollection || {}) || []);
    keys = keys.except(ignoreKeys);
    for (let key of keys.toArray()) {
        let oldValue = to.get(key);
        let newValue = from.get(key);
        if (newValue instanceof LocalObject) {
            let className = newValue.className;
            let id = newValue.id;
            newValue = new Parse.Object(className);
            newValue.set('objectId', id)
            //newValue = Parse.Object.createWithoutData(newValue.id);
            //newValue.className = className;
        } else if (newValue instanceof Relation) {
            let oldItems = await to.relation(key).query().find();
            for (let item of oldItems) {
                to.relation(key).remove(item);
            }
            let items = await newValue.find();
            items.select(m => {
                let obj = new Parse.Object(m.className);
                obj.set('objectId', m.id);
                to.relation(key).add(obj);
            })
            continue;
            //newValue = new Parse.Relation(to, newValue.key);
        } else if (newValue instanceof Object && 'targetClassName' in newValue && 'id' in newValue) {
            let oldItems = await to.relation(key).query().find();
            for (let item of oldItems) {
                to.relation(key).remove(item);
            }
            let localRelation = new Relation(newValue);
            let items = await localRelation.find();
            items.select(m => {
                let obj = new Parse.Object(m.className);
                obj.set('objectId', m.id);
                to.relation(key).add(obj);
            })
            continue;
        } else if (newValue instanceof Object && '_collection' in newValue) {
            let className = newValue['_collection'].className;
            let id = newValue['_collection'].id;
            newValue = new Parse.Object(className);
            newValue.set('objectId', id)
            //let parse = Parse.Object.createWithoutData(newValue['_collection'].id);
            //parse.id = newValue.id;
            // parse.className = newValue.className;
            // newValue = parse;
        }
        if (oldValue != newValue) {
            to.set(key, newValue);
        }
    }
    if (!isNew)
        to.set('objectId', from.id);
    return to;
}