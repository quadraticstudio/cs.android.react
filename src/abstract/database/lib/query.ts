import * as DbBerish from "db-berish";
import * as Parse from "parse";
import LocalObject from "./object";
import LocalAdapter from "./adapter";
import { LINQ } from "linq";
import { toLocal, fetchLocal } from "./converter";
import { Alert } from "react-native";
import DbAdapter from "db-berish/lib/adapter";
import QueryLINQ from "parse-query-to-linq"

export default class Query<T extends LocalObject> extends DbBerish.Query<T> {
    constructor(className: new (...args) => T);
    constructor(className: string);
    constructor(className: (new (...args) => T) | string) {
        super(className);
        let item = LINQ
            .fromArray(Object.keys(LocalAdapter.ctors))
            .select(m => {
                return {
                    key: m,
                    value: LocalAdapter.ctors[m] || {}
                }
            }).firstOrNull(m => {
                if (typeof className == 'string') {
                    return className == m.key;
                } else {
                    if (!m.value.berish)
                        return false;
                    if (m.value.berish === className)
                        return true;
                    return false;
                }
            });
        if (item) {
            this.ctor = item.value.berish;
            this.name = item.key;
            this.classChain = DbAdapter.instance.getClass(this.name);
        } else {
            this.ctor = typeof className == 'string' ? ((LocalAdapter.ctors[className] && LocalAdapter.ctors[className].berish) || LocalObject) : className;
            this.name = typeof className == 'string' ? className : className.name;
            this.classChain = DbAdapter.instance.getClass(this.name);
        }
    }

    // static toLocal(parseQuery: Parse.Query) {
    //     let query = new Query(parseQuery.className);
    //     return query;
    // }

    // static toParse<T extends LocalObject>(localQuery: Query<T>) {
    //     let query = new Parse.Query(localQuery.name);
    //     return query;
    // }

    async getLocal(id: string) {
        let localItem = await super.get(id);
        return fetchLocal(localItem) as Promise<T>;
    }

    async getParse(id: string) {
        let item = await new Parse.Query(this.name).get(id);
        let local = await toLocal(item);
        local = await local.saveToLocal() as T;
        return fetchLocal(local);
    }

    async getParseHashing(id: string) {
        let item = await new Parse.Query(this.name).get(id);
        let local = await toLocal(item);
        return await local.saveToLocal();
    }

    async get(id: string) {
        return this.getLocal(id);
    }

    // async get(localId: string) {
    //     let condition = LocalAdapter.instance.condition;
    //     let localItem: LocalObject = null;
    //     if (condition) {
    //         let item = await new Parse.Query(this.name).get(localId);
    //         //await DbBerish.Adapter.instance.clearClass(this.name);
    //         let local = await toLocal(item);
    //         localItem = await local.saveToLocal() as T;
    //     } else {
    //         localItem = await super.get(localId);
    //     }
    //     return fetchLocal(localItem) as Promise<T>;
    // }

    async getIds() {
        let classChain = await DbAdapter.instance.getClass(this.name);
        return classChain.value();
    }

    async findParse(func?: (query: Parse.Query) => Parse.Query) {
        let query = new Parse.Query(this.name);
        if (func)
            query = func(query);
        let items = await QueryLINQ(query);
        let localItems: T[] = [];
        for (let item of items.toArray()) {
            try {
                let local = await toLocal(item) as T;
                local = await local.saveToLocal();
                localItems.push(local);
            } catch (err) {
                console.log(err)
            }
        }
        /*let promise = await Promise.all(items.select(async m => {
            let local = await toLocal(m);
            return await local.saveToLocal() as T;
        }).toArray());*/
        //let localItems = LINQ.fromArray(promise || []);
        return LINQ.fromArray(localItems);
    }

    async findLocal() {
        let localItems = await super.find();
        let promises = localItems.select(m => fetchLocal(m)).toArray();
        let promise = await Promise.all(promises) as T[];
        return LINQ.fromArray(promise || []);
    }

    async find() {
        //return super.find();
        return this.findLocal();
    }

    // async find() {
    //     let condition = LocalAdapter.instance.condition;
    //     let localItems: LINQ<LocalObject> = null;
    //     if (condition) {
    //         let query = await new Parse.Query(this.name).find();
    //         //parse объекты
    //         let parse = LINQ.fromArray(query);
    //         //await DbBerish.Adapter.instance.clearClass(this.name);
    //         let objects = await Promise.all(parse.select(async m => {
    //             let local = await toLocal(m);
    //             let save = await local.saveToLocal() as T;
    //             return save;
    //         }).toArray());
    //         localItems = LINQ.fromArray(objects);
    //         /*let local = await super.find();
    //         let parseIds = parse.select(m => m.id);
    //         // Есть в удаленке и в локалке
    //         let localA = local.where(m => parseIds.count(k => k == m.id) > 0);
    //         // Есть в локалке, но нет в вебе
    //         let localB = local.where(m => parseIds.count(k => k == m.id) <= 0);
    //         // Есть только в вебе, но нет их в локалке
    //         let parseB = parse.where(m => localA.count(k => k.id == m.id) <= 0);
    //         let newParseB = await Promise.all(parseB.select(m => toLocal(m)).select(m => m.save() as Promise<T>).toArray());
    //         return localA.concat(localB).concat(newParseB).distinct(m => m.id);*/

    //     } else {
    //         localItems = await super.find();
    //     }
    //     let items = await Promise.all(localItems.select(m => fetchLocal(m) as Promise<T>).toArray());
    //     return LINQ.fromArray(items);
    // }
}