import * as DbBerish from "db-berish";
import * as Parse from "parse";
import { LINQ } from "linq";
import LocalAdapter from "./adapter";
import { Adapter } from "db-berish";
import { Job } from "./worker";
import { guid, generateId } from "db-berish/lib/util";
import { toLocal, toParse, fetchLocal } from "./converter";
import Query from "./query";
import Relation from "./relation";
import DbAdapter from "db-berish/lib/adapter";
import { EmitterListenConfig } from "./emitter";

export type TypeLocalObject = typeof LocalObject & { parse?: typeof Parse.Object };



export default class LocalObject extends DbBerish.Object {
    constructor(className?: string) {
        super(className);
    }

    get createdAt() {
        return this.get('createdAt');
    }

    get keyValueCollection() {
        return this['prepareDBOBject'](super.keyValueCollection) as { [property: string]: any };
    }

    set keyValueCollection(value: { [property: string]: any }) {
        this['_collection'] = value;
    }

    set createdAt(value: Date) {
        this.set('createdAt', value)
    }

    get updatedAt() {
        return this.get('updatedAt');
    }

    set updatedAt(value: Date) {
        this.set('updatedAt', value)
    }

    // getParseCtor() {
    //     let ctor = LocalAdapter.ctors[this.className];
    //     return ctor && ctor.parse || Parse.Object;
    // }

    relation<T extends LocalObject>(key: string) {
        return new Relation<T>(this, key);
    }

    /*async fetch() {
        let condition = LocalAdapter.instance.condition;
        if (this.id) {
            if (condition) {
                let obj = await new Parse.Query(this.getParseCtor()).get(this.id);
                return toLocal(obj, this).save();
            } else {
                return super.fetch();
            }
        } return this;
    }*/

    private async remoteExists(id: string) {
        try {
            let item = await new Parse.Query(this.className).get(id);
            if (item)
                return true;
            return false;
        } catch (err) {
            return false;
        }
    }

    async fetchLocal() {
        return super.fetch();
    }

    async fetchParse() {
        let parseObj: Parse.Object = null;
        if (this.id) {
            let exists = await this.remoteExists(this.id);
            if (!exists)
                return this;
            //parseObj = await new Parse.Query(this.getParseCtor()).get(this.id);
            parseObj = await new Parse.Query(this.className).get(this.id);
            let local = await toLocal(parseObj);
            let saved = await local.saveToLocal() as this;
            return saved;
        }
        return this;
    }

    log = (...log) => {
        if (this.className == 'Cashbox') {
            console.log(...log);
        }
    }

    async fetch() {
        let object = await this.fetchLocal();
        return fetchLocal(object) as Promise<this>;
    }

    // async fetch() {
    //     let condition = LocalAdapter.instance.condition;
    //     let object = this;
    //     if (condition)
    //         object = await this.fetchParse();
    //     else
    //         object = await this.fetchLocal();
    //     return fetchLocal(object) as Promise<this>;
    // }

    async saveToLocal() {
        await LocalObject.emit(
            {
                trigger: 'beforeSave',
                className: this.className
            },
            {
                object: this,
                user: null,
                typeTrigger: 'local'
            }
        );
        this.updatedAt = new Date();
        if (!this.createdAt)
            this.createdAt = this.updatedAt;
        let local = await super.save();
        await LocalObject.emit(
            {
                trigger: 'afterSave',
                className: this.className
            },
            {
                object: local,
                user: null,
                typeTrigger: 'local'
            }
        );
        return local;
        //return super.save();
        //return local.fetch();
    }

    async saveToParse() {
        await LocalObject.emit(
            {
                trigger: 'beforeSave',
                className: this.className
            },
            {
                object: this,
                user: null,
                typeTrigger: 'web'
            }
        );
        let parseObj: Parse.Object = null;
        let exists = this.id && await this.remoteExists(this.id) || false;
        if (!!exists) {
            //parseObj = await new Parse.Query(this.getParseCtor()).get(this.id);
            parseObj = await new Parse.Query(this.className).get(this.id);
        } else {
            //parseObj = new (this.getParseCtor())();
        }
        parseObj = await toParse(this, parseObj, !exists);
        parseObj = await parseObj.save();
        if (!exists) {
            await LocalAdapter.instance.queue.renameTasks(this.id, parseObj.id);
            await this.destroyLocal();
        }
        let local = await toLocal(parseObj);
        local = await local.saveToLocal();
        await LocalObject.emit(
            {
                trigger: 'beforeSave',
                className: this.className
            },
            {
                object: local,
                user: null,
                typeTrigger: 'web'
            }
        );
        return local as this;
    }

    async save() {
        let local = await this.saveToLocal();
        this.updatedAt = new Date();
        if (!this.createdAt)
            this.createdAt = this.updatedAt;
        //Добавить сохранение товара как задачу
        let task = await LocalAdapter.instance.queue.save(local);
        return local;
    }

    // async superSave() {
    //     let classChain = await DbAdapter.instance.getClass(this.className);
    //     let hasId = !!this.id;
    //     let ids = await classChain.value<string[]>([]);
    //     this.id = this.id || generateId();
    //     let prepare = this['prepareDBOBject'](this.keyValueCollection);
    //     const _save = (id: string) => {
    //         console.log(ids);
    //         ids.push(id);
    //         return classChain.write(ids).then(() => {
    //             console.log(ids);
    //             return classChain.chain(id).write(prepare)
    //         });
    //         //return classChain.chain(id).write(prepare);
    //     }
    //     if (hasId) {
    //         let isExists = ids.indexOf(this.id) != -1;
    //         if (isExists) {
    //             await classChain.chain(this.id).write(prepare).then(m => m);
    //         } else {
    //             await _save(this.id);
    //             console.log('superSave', {
    //                 hasId, ids, isExists, className: this.className, id: this.id, prepare
    //             })
    //         }
    //     } else {
    //         await _save(this.id);
    //     }
    //     return this;
    // }

    // async save() {
    //     let condition = LocalAdapter.instance.condition;
    //     this.updatedAt = new Date();
    //     if (!this.createdAt)
    //         this.createdAt = this.updatedAt;
    //     if (condition) {
    //         return this.saveToParse();
    //     }
    //     //Добавить сохранение товара как задачу
    //     return this.saveToLocal();
    // }

    async destroyLocal() {
        if (!this.id)
            return;
        //remove item
        let classChain = await DbBerish.Adapter.instance.getClass(this.className);
        await classChain.chain(this.id).remove();
        //remove id in ids list of class
        let ids = await classChain.value<string[]>([]);
        if (ids.indexOf(this.id) != -1) {
            ids.splice(ids.indexOf(this.id), 1);
            await classChain.write(ids);
        }
    }

    async destroyParse() {
        let parseObj: Parse.Object = null;
        let exists = this.id && await this.remoteExists(this.id);
        if (!!exists) {
            parseObj = await new Parse.Query(this.getParseCtor()).get(this.id);
        } else {
            parseObj = new (this.getParseCtor())();
        }
        parseObj = await toParse(this, parseObj);
        parseObj = await parseObj.destroy();
        return this.destroyLocal();
    }

    async destroy() {
        let condition = LocalAdapter.instance.condition;
        if (condition) {
            return this.destroyParse();
        }
        return this.destroyLocal();
    }

    /*async save() {
        let condition = LocalAdapter.instance.condition;
        //await super.save();
        let cb = async () => {
            let parseObj: Parse.Object = null;
            let exists = this.id && await this.remoteExists(this.id);
            if (!!exists) {
                parseObj = await new Parse.Query(this.getParseCtor()).get(this.id);
            } else {
                parseObj = new (this.getParseCtor())();
            }
            parseObj = toParse(this, parseObj);
            return await parseObj.save();
        }
        if (condition) {
            let parseObj = await cb();
            this.id = parseObj.id;
            this.className = parseObj.className;
            //return this.fetch();
            return super.save();
        } else {
            let self = this;
            LocalAdapter.instance.addTask(async () => {
                if (LocalAdapter.instance.condition) {
                    try {
                        let parseObj = await cb();
                        self.id = parseObj.id;
                        self.className = parseObj.className;
                        //await this.fetch();
                        await super.save();
                    } catch (err) {
                        return false;
                    }
                } else {
                    return false;
                }
            })
        }
        return this;
    }*/

    private static emit(config: { trigger: string, className: string }, req: TriggerRequest) {
        return LocalAdapter.emitter.emit(this.generateTriggerName(config.trigger, config.className), req);
    }

    private static generateTriggerName(trigger: string, className: string) {
        return `${trigger}:${className}`;
    }

    private static trigger(config: { trigger: string, className: string } & EmitterListenConfig) {
        return LocalAdapter.emitter.listen(this.generateTriggerName(config.trigger, config.className), config);
    }

    static beforeSave(className: string, cb: (req: TriggerRequest) => any) {
        return this.trigger({
            trigger: 'beforeSave',
            isAsync: true,
            className,
            cb
        });
    }

    static afterSave(className: string, cb: (req: TriggerRequest) => any) {
        return this.trigger({
            trigger: 'afterSave',
            isAsync: true,
            className,
            cb
        });
    }
}

interface TriggerRequest {
    object: LocalObject;
    user: LocalObject;
    typeTrigger: 'local' | 'web';
}