//import * as Parse from "parse";
import { LINQ } from "linq";
import LocalAdapter from "./adapter";
import LocalObject, { TypeLocalObject } from "./object";

type CollectionType<T = any> = { [key: string]: T };

export default class Task extends LocalObject {
    constructor() {
        super("Task");
    }

    get taskName() {
        return this.get('taskName')
    }

    set taskName(value: string) {
        this.set('taskName', value);
    }

    get refClassName() {
        return this.get('refClassName')
    }

    set refClassName(value: string) {
        this.set('refClassName', value);
    }

    private getCtor(className: string) {
        let ctor: TypeLocalObject = null;
        let name: string = null;
        let item = LINQ
            .fromArray(Object.keys(LocalAdapter.ctors))
            .select(m => {
                return {
                    key: m,
                    value: LocalAdapter.ctors[m] || {}
                }
            }).firstOrNull(m => {
                return className == m.key;
            });
        if (item) {
            ctor = item.value.berish;
            name = item.key;
        } else {
            ctor = ((LocalAdapter.ctors[className] && LocalAdapter.ctors[className].berish) || LocalObject);
            name = className;
        }
        return {
            ctor, name
        }
    }

    private get refCollection() {
        return this.get('refCollection')
    }

    private set refCollection(value: CollectionType) {
        this.set('refCollection', value)
    }

    get localObject() {
        let collection = this.refCollection || {} as CollectionType;
        let className = this.refClassName || collection.className;
        let ctor = this.getCtor(className);
        let object = new ctor.ctor(ctor.name);
        object["_collection"] = collection;
        return object;
    }

    set localObject(value: LocalObject) {
        this.refCollection = value && value.keyValueCollection;
        this.refClassName = value && value.className;
    }

    get custom() {
        return this.get('custom');
    }

    set custom(value: any) {
        this.set('custom', value);
    }
}
