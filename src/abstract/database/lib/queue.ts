import Task from "./task";
import LocalObject from "./object";
import * as Parse from "parse";
//import * as DbBerish from "db-berish";
import { parseAuth, parseAuthSession, parseLogOutSession } from "./auth";
import LocalAdapter from "./adapter";
import { LINQ } from "linq";
import Query from "./query";
import * as Model from "../../../model"
import { Alert } from "react-native";
import { error } from "../../global/Messages";
import { getParseError, ComponentController } from "../../global/index";
export class Queue {

    private async addTask(name: string, object: LocalObject) {
        let task = new Task();
        task.taskName = name;
        task.localObject = object;
        task = await task.saveToLocal();
        return task;
    }

    private async addCustomTask(name: string, custom?: any) {
        let task = new Task();
        task.taskName = name;
        task.custom = custom;
        task = await task.saveToLocal();
        return task;
    }

    async save(object: LocalObject) {
        return this.addTask("save", object);
    }

    async destroy(object: LocalObject) {
        return this.addTask("destroy", object);
    }

    async logIn(login: string) {
        return this.addCustomTask("logIn", {
            login
        });
    }

    async logOut() {
        return this.addCustomTask("logOut");
    }

    async showTasks() {
        return new Query(Task).find();
    }

    async clearTasks() {
        let tasks = await new Query(Task).find();
        return Parse.Promise.when(tasks.select(m => m.destroyLocal()).toArray());
    }

    async uploadOneTask() {
        let condition = LocalAdapter.instance.condition;
        //let user = await Parse.User['currentAsync']();
        if (!condition)
            return;
        let tasks = await new Query(Task).find();
        tasks = tasks.orderBy(m => m.createdAt);
        let task = tasks.firstOrNull();
        if (task)
            task = await this.doTask(task);
        return task;

    }

    async upload(controller: ComponentController, onTitle?: (all: number, current: number) => any) {
        let condition = LocalAdapter.instance.condition;
        //let user = await Parse.User['currentAsync']();
        if (!condition)
            return;
        let tasks = await new Query(Task).find();
        let ids = tasks.select(m => m.id);
        //tasks = tasks.orderBy(m => m.createdAt);
        let readyTasks: Task[] = [];
        let i = 0;
        const cb = async () => {
            for (let id of ids.toArray()) {
                if (onTitle)
                    onTitle(ids.count(), ++i);
                let task = await new Query(Task).get(id);
                task = await this.doTask(task);
                readyTasks.push(task);
            }
        }
        if (!onTitle && ids.count() > 0) {
            await controller.tryLoad(cb, { title: `Выгрузка изменений` });
        } else
            await cb();
        return readyTasks;
    }

    async download(onTitle: (hash: 'fetch' | 'hashParse' | 'hashLocal' | 'hashCheck', all: number, current: number) => any) {
        let condition = LocalAdapter.instance.condition;
        if (!condition)
            return;
        onTitle('hashLocal', 100, 0);
        let hashLocal = await new Query("HashTable").findLocal();
        onTitle('hashParse', 100, 50);
        let hashParse = await new Query("HashTable").findParse(query => query.select('refClassName', 'refId', 'hash'));
        hashParse = hashParse.where((m, i, l) => {
            onTitle('hashCheck', l.count(), i + 1);
            let localHash = hashLocal.singleOrNull(k => k.get('refClassName') == m.get('refClassName') && k.get('refId') == m.get('refId'));
            if (localHash) {
                if (localHash.get('hash') == m.get('hash'))
                    return false;
                else
                    return true;
            } else {
                return true;
            }
            /*if (localHash && localHash.get('hash') == m.get('hash')) {
                return false;
            }
            return true;*/
        });
        // console.log('hashparse', hashParse.select(m => {
        //     return {
        //         refId: m.get('refId'),
        //         refClassName: m.get('refClassName'),
        //         hash: m.get('hash')
        //     }
        // }).toArray())
        let kv = hashParse.groupBy(m => m.get('refClassName')).toLinq();
        let i = 0;
        for (let m of kv.toArray()) {
            onTitle('fetch', kv.count(), ++i);
            let ids = LINQ.fromArray(m.value).select(m => m.get('refId')).toArray();
            let tests = [];
            for (let id of ids) {
                try {
                    let test = await new Query(m.key).getParseHashing(id);
                    tests.push(test);
                } catch (err) {
                }
            }
            //let test = await query.findParse(query => query.containedIn('objectId', ids));
        }
    }

    async downloadFake(onTitle: (hash: 'fetch' | 'hashParse' | 'hashLocal' | 'hashCheck', all: number, current: number) => any) {
        let ctors = LocalAdapter.ctors;
        let keys = Object.keys(ctors || {}) || [];
        let linq = LINQ.fromArray(keys);
        let i = 0;
        for (let m of linq.toArray()) {
            onTitle('fetch', linq.count(), ++i);
            await new Query(m).findParse();
        }
    }

    async firstDownload(onTitle: (all: number, current: number) => any) {
        let ctors = LocalAdapter.ctors;
        let keys = Object.keys(ctors || {}) || [];
        let linq = LINQ.fromArray(keys);
        let i = 0;
        for (let m of linq.toArray()) {
            onTitle(linq.count(), ++i);
            await new Query(m).findParse();
        }
    }

    /*async test() {
        let items = await new Query("Group").findParse();
        console.log('items', items.toArray())
        let groups = await new Query("Group").find();
        console.log('groups', groups.toArray())
        return items;
    }*/

    async renameTasks(oldId: string, newId: string) {
        let tasks = await new Query(Task).find();
        let readyTasks: Task[] = [];
        for (let task of tasks.toArray()) {
            task = await this.renameTask(task, oldId, newId);
            readyTasks.push(task);
        }
        return readyTasks;
    }

    private async renameTask(task: Task, oldValue: string, newValue: string) {
        if (task.taskName != 'save' && task.taskName != 'destroy')
            return task;
        let keyValueCollection = task.localObject.keyValueCollection;
        let json = JSON.stringify(keyValueCollection);
        json = json.replace(new RegExp(oldValue, 'g'), newValue);
        keyValueCollection = JSON.parse(json);
        task['refCollection'] = keyValueCollection;
        task = await task.saveToLocal();
        return task;
    }

    private async doTask(task: Task, attemp?: number): Promise<Task> {
        let condition = LocalAdapter.instance.condition;
        if (!condition)
            return task;
        attemp = attemp != null ? attemp : 0;
        if (!task)
            return null;
        try {
            let taskName = task.taskName;
            let cb = this[`do_${taskName}`] as Function;
            if (!cb)
                return null;
            console.log(`call ${`do_${taskName}`}`);
            let result = await cb(task);
            await task.destroyLocal();
        } catch (err) {
            console.log(err)
            // if (err.code == 101) {
            //     if (task.localObject && task.localObject.id != null) {
            //         task.localObject.id = null;
            //     }
            // }
            if (attemp < 2) {
                return await this.doTask(task, attemp + 1);
            }
            await task.destroyLocal();
        }
        return task;
    }

    private async do_save(task: Task) {
        let object = task.localObject;
        console.log('saveStart', object);
        object = await object.saveToParse();
        console.log('saveEnd', object);
        return object;
    }

    private async do_destroy(task: Task) {
        let object = task.localObject;
        return object.destroyParse();
    }

    private async do_logIn(task: Task) {
        let object = task.custom as { login: string };
        return parseAuthSession(object.login);
    }

    private async do_logOut(task: Task) {
        return await parseLogOutSession();
    }
}