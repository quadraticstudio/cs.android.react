import { StyleSheet, StyleProp, ViewStyle } from "react-native";

class Styles {
    private static _instance: Styles = null;

    static get instance() {
        if (this._instance == null)
            this._instance = new Styles();
        return this._instance;
    }

    border = {
        borderColor: 'red',
        borderStyle: 'solid',
        borderWidth: 3
    }
}



export function createStyle(init?: ViewStyle, string?: keyof Styles) {
    init = init || {} as any;
    if (!string)
        return init;
    return StyleSheet.flatten([init, Styles.instance[string]]) as ViewStyle;
}
