import { StackNavigator } from "react-navigation";
import Abstract from "./abstract";
import Controllers from "./mvc/index";
import LocalDB from "./LocalDbDashboard";

export default () => StackNavigator(Abstract.Routers.WithRoute(
    // Авторизация
    {
        name: 'AuthList',
        component: Controllers.Auth.List,
        headerTitle: 'Выбор пользователя'
    },
    {
        name: 'AuthForm',
        component: Controllers.Auth.Form,
        headerTitle: 'Авторизация'
    },
    {
        name: 'LocalDB',
        component: LocalDB,
        headerTitle: 'Локальная БД'
    },
    // Главная
    {
        name: 'Main',
        component: Controllers.Pages.Main,
        headerTitle: 'Главная'
    },
    // Главные формы документов
    {
        name: 'DocSaleSelect',
        component: Controllers.Doc.SaleController,
        headerTitle: 'Выбор операции'
    },
    {
        name: 'DocSaleForm',
        component: Controllers.Doc.Sale.Form,
        headerTitle: 'Детали продажи'
    },
    {
        name: 'DocSaleCash',
        component: Controllers.Doc.Sale.Cash,
        headerTitle: 'Проведение продажи'
    },

    {
        name: 'DocPurchaseForm',
        component: Controllers.Doc.Purchase.Form,
        headerTitle: 'Детали покупки'
    },
    {
        name: 'DocPurchaseCash',
        component: Controllers.Doc.Purchase.Cash,
        headerTitle: 'Проведение покупки'
    },

    {
        name: 'DocReturnSelect',
        component: Controllers.Doc.ReturnController,
        headerTitle: 'Выбор операции возврата'
    },
    {
        name: 'DocReturnForm',
        component: Controllers.Doc.Return.Form,
        headerTitle: 'Детали возврата'
    },
    {
        name: 'DocReturnCash',
        component: Controllers.Doc.Return.Cash,
        headerTitle: 'Проведение возврата'
    },

    {
        name: 'DocReturnOfPurchaseForm',
        component: Controllers.Doc.ReturnOfPurchase.Form,
        headerTitle: 'Детали возврата'
    },
    {
        name: 'DocReturnOfPurchaseCash',
        component: Controllers.Doc.ReturnOfPurchase.Cash,
        headerTitle: 'Проведение возврата'
    },

    {
        name: 'DocProviderInSelect',
        component: Controllers.Doc.ProviderIn.Select,
        headerTitle: 'Выбор для приемки'
    },
    {
        name: 'DocProviderInForm',
        component: Controllers.Doc.ProviderIn.Form,
        headerTitle: 'Детали приемки'
    },

    {
        name: 'DocProviderOutSelect',
        component: Controllers.Doc.ProviderOut.Select,
        headerTitle: 'Выбор для возврата'
    },
    {
        name: 'DocProviderOutForm',
        component: Controllers.Doc.ProviderOut.Form,
        headerTitle: 'Детали возврата'
    },

    {
        name: 'DocCleanSelect',
        component: Controllers.Doc.Clean.Select,
        headerTitle: 'Выбор для списания'
    },
    {
        name: 'DocCleanForm',
        component: Controllers.Doc.Clean.Form,
        headerTitle: 'Детали списания'
    },

    {
        name: 'DocInventorySelect',
        component: Controllers.Doc.Inventory.Select,
        headerTitle: 'Выбор для инвентаризации'
    },
    {
        name: 'DocInventoryList',
        component: Controllers.Doc.Inventory.List,
        headerTitle: 'Документы инвентаризаций'
    },

    {
        name: 'DocCashInForm',
        component: Controllers.Doc.CashIn.Form,
        headerTitle: 'Внесение наличных'
    },

    {
        name: 'DocCashOutForm',
        component: Controllers.Doc.CashOut.Form,
        headerTitle: 'Выплата наличных'
    },


    // Касса
    {
        name: 'CashboxMain',
        component: Controllers.Pages.Cashbox.Main,
        headerTitle: 'Касса'
    },
    {
        name: 'CashboxMoney',
        component: Controllers.Pages.Cashbox.Money,
        headerTitle: 'Наличные'
    },

    // Товары
    {
        name: 'GoodsMain',
        component: Controllers.Pages.Goods.Main,
        headerTitle: 'Товары'
    },
    {
        name: 'GoodsList',
        component: Controllers.Goods.List,
        headerTitle: 'Список товаров'
    },
    {
        name: 'GoodsForm',
        component: Controllers.Goods.Form,
        headerTitle: 'Форма товара'
    },
    {
        name: 'GoodsGroup',
        component: Controllers.Goods.Group,
        headerTitle: 'Форма группы'
    },
    // Отчеты
    {
        name: 'ReportMain',
        component: Controllers.Pages.Report.Main,
        headerTitle: 'Отчеты'
    },
    {
        name: 'ReportCashbox',
        component: Controllers.Pages.Report.Cashbox,
        headerTitle: 'Кассовые отчеты'
    },
    {
        name: 'ReportControl',
        component: Controllers.Pages.Report.Control,
        headerTitle: 'Управленческие отчеты'
    },
    {
        name: 'ReportConsolidate',
        component: Controllers.Report.Consolidate,
        headerTitle: 'Сводный отчет'
    },
    {
        name: 'ReportIncome',
        component: Controllers.Report.Income,
        headerTitle: 'Валовая прибыль'
    },
    {
        name: 'ReportMove',
        component: Controllers.Report.Move,
        headerTitle: 'Движения товаров'
    },
    {
        name: 'ReportProviderIn',
        component: Controllers.Report.ProviderIn,
        headerTitle: 'Отчет по закупкам'
    },
    {
        name: 'ReportResidue',
        component: Controllers.Report.Residue,
        headerTitle: 'Товарные остатки'
    },
    {
        name: 'ReportSale',
        component: Controllers.Report.Sale,
        headerTitle: 'Отчет по продажам'
    },
    {
        name: 'ReportDocuments',
        component: Controllers.Report.Documents.List,
        headerTitle: 'Журнал документов'
    },
    {
        name: 'ReportDocumentsForm',
        component: Controllers.Report.Documents.Form,
        headerTitle: 'Подробно о документе'
    },
    {
        name: 'ReportOperations',
        component: Controllers.Report.Operations,
        headerTitle: 'Журнал операций'
    },
    {
        name: 'ServiceMain',
        component: Controllers.Pages.Service,
        headerTitle: 'Сервис'
    },
    {
        name: 'HelpMain',
        component: Controllers.Pages.Help,
        headerTitle: 'Справка'
    },
    {
        name: 'SettingsMain',
        component: Controllers.Pages.Settings.Main,
        headerTitle: 'Настройки'
    },
    {
        name: "SettingsGeneral",
        component: Controllers.Pages.Settings.General,
        headerTitle: 'Основные настройки'
    },
    {
        name: "SettingsTurn",
        component: Controllers.Settings.Turn.View,
        headerTitle: 'Настройки смены'
    },
    {
        name: 'SettingsManual',
        component: Controllers.Pages.Settings.Manual,
        headerTitle: 'Справочники'
    },
    {
        name: 'SettingsManualPrices',
        component: Controllers.Settings.Manual.Prices.View,
        headerTitle: 'Типы цен'
    },
    {
        name: 'SettingsManualSales',
        component: Controllers.Settings.Manual.Sales.View,
        headerTitle: 'Скидки'
    },
    {
        name: 'AdditionalMain',
        component: Controllers.Pages.Additional,
        headerTitle: 'Дополнительно'
    }
));