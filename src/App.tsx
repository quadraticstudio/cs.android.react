import * as React from "react";
import { View, Text, Container } from 'native-base';
import createNavigator from "./routers"
import { AppLoading, Constants } from "expo";
import Abstract from "./abstract";
import { NavigationContainer } from "react-navigation";
import Controllers from "./mvc"

export interface ViewProps {
}

export default class ViewComponent extends React.Component<ViewProps, { Navigator: NavigationContainer }> {

    constructor(props) {
        super(props);
        this.state = {
            Navigator: null
        };
    }

    createNavigatorComponent = async () => {
        let Navigator = this.state.Navigator;
        Navigator = createNavigator();
        return new Promise<void>(resolve => this.setState({
            Navigator
        }, () => resolve()));
    }

    render() {
        let Navigator = this.state.Navigator;
        if (!Navigator)
            return <AppLoading startAsync={this.createNavigatorComponent} onFinish={() => { }} />
        return (
            <Container style={{ backgroundColor: '#1c3c51', flex: 1 }}>
                <Navigator />
            </Container>
        );
    }
}