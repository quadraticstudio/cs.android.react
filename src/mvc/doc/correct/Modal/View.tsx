import * as React from "react";
import Controller from "./Controller"
import Abstract from "../../../../abstract";
import { LINQ } from "linq";
import { StaticComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import { View, Spinner, Text } from "native-base";

interface ViewProps {
    controller: Controller;
}

export default class ViewComponent extends React.Component<ViewProps, {}>{
    constructor(props) {
        super(props)
    }

    renderTable = () => {
        let { items, isLoading } = this.props.controller.state;
        if (isLoading) {
            return <View style={{ flex: 1, width: '100%', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                <Spinner />
                <Text style={{ textAlign: 'center', width: '100%' }}>Идет загрузка</Text>
            </View>;
        }
        return <Abstract.General.ListCollection
            data={this.props.controller.state.items || []}
            onClick={(index, item) => this.props.controller.resolve(item)}
            header={[
                {
                    title: 'Номер',
                    render: (obj: Model.Document) => {
                        return obj.number;
                    }
                },
                {
                    title: 'Кассовый номер',
                    render: (obj: Model.Document) => {
                        return obj.numberCashbox;
                    }
                },
                {
                    title: 'Фискальный номер',
                    render: (obj: Model.Document) => {
                        return obj.numberFN;
                    }
                },
                {
                    title: 'Вид',
                    render: (obj: Model.Document) => {
                        return obj.title;
                    }
                },
                {
                    title: 'Итого',
                    render: (obj: Model.Document) => {
                        return obj.sum;
                    }
                },
                {
                    title: 'Дата',
                    render: (obj: Model.Document) => {
                        return obj.date;
                    }
                }
            ]}
        />
    }

    render() {
        return (
            <Abstract.General.ModalForm
                title="Добавить"
                resolve={this.props.controller.reject}
                reject={this.props.controller.reject}
            >
                <View style={{ width: '100%', height: '100%', minHeight: '100%', minWidth: '100%' }}>
                    {this.renderTable()}
                </View>
            </Abstract.General.ModalForm>
        );
    }
}