import * as React from "react";
import Abstract from "../../../../abstract";
import { LINQ } from "linq";
import { StaticComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import View from "./View";
import Query from "../../../../abstract/database/lib/query";

interface ControllerState {
    items: Model.Document[];
    isLoading: boolean;
}

export default class Controller extends React.Component<StaticComponentProps<Model.Document> & { type: string }, ControllerState>{
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoading: true
        };
        console.log(this.props.type)
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.load, { changeLoading: isLoading => this.setState({ isLoading }) });
    }

    load = async () => {
        let { items } = this.state;
        let linq = await new Query(Model.Document).find();
        if (this.props.type == 'income') {
            linq = linq.where(m => (m.type == Model.DocumentTypeEnum.tillSlip && (m.documentEntity as Model.Documents.TillSlipDocument).type == 'sale') || m.type == Model.DocumentTypeEnum.returnOfPurchase)
        } else {
            linq = linq.where(m => (m.type == Model.DocumentTypeEnum.tillSlip && (m.documentEntity as Model.Documents.TillSlipDocument).type == 'return') || m.type == Model.DocumentTypeEnum.purchase)
        }
        items = linq.toArray();
        this.setState({ items });
    }

    resolve = async (item: Model.Document) => {
        this.props.resolve(item);
    }

    reject = async () => {
        return this.props.reject();
    }

    render() {
        return (
            <View controller={this} />
        );
    }
}