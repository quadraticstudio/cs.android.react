import * as React from "react";
import View from "./View"
import Abstract from "../../../abstract"
import { ComponentProps } from "../../../abstract/global/ComponentController";
import * as Model from "../../../model"
import * as collection from "collection";
import { LINQ } from "linq";

interface Match {
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    type: string,
    base: string,
    typeCash: string,
    numberTillSlip: number;
    sum: number
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            type: null,
            base: null,
            typeCash: null,
            numberTillSlip: null,
            sum: 0
        };
    }

    submit = async () => {
        let params = this.props.controller.params;
        let { cashbox, company } = this.props.global;
        // let operation = new Model.CashOperation();
        // operation.cashbox = cashbox;
        // operation.type = Model.CashTypeEnum.output;
        // operation.description = this.state.description;
        // operation.from = this.state.from;
        // operation.sum = this.state.sum;
        // operation.outputType = this.state.expenditure;
        // operation = await operation.save();
        let doc = new Model.Document(null, Model.Documents.CorrectDocument);
        doc.documentEntity.type = this.state.type;
        doc.documentEntity.base = this.state.base;
        doc.documentEntity.cashType = this.state.typeCash;
        doc.documentEntity.numberTillSlip = this.state.numberTillSlip;
        doc.sum = this.state.sum;
        doc = await doc.save();
        this.props.controller.router.navigation.goBack();
    }

    // VIEw

    onSubmit = () => {
        this.props.controller.tryLoad(this.submit);
    }

    onChange = (state: ControllerState, path: string) => {
        this.setState(state);
    }

    onSearchDoc = (doc: Model.Document) => {
        let { base, numberTillSlip, typeCash, sum } = this.state;
        if (doc.type === Model.DocumentTypeEnum.tillSlip) {
            let entity = doc.documentEntity as Model.Documents.TillSlipDocument;
            if (entity.type == 'sale') {
                base = Model.Documents.CorrectBaseTypeEnum[Model.Documents.CorrectBaseTypeEnum.tillSlip];
            }
            else if (entity.type == 'return') {
                base = Model.Documents.CorrectBaseTypeEnum[Model.Documents.CorrectBaseTypeEnum.tillSlipReturn]
            }
            typeCash = entity.saleType || 'cash';
        } else if (doc.type == Model.DocumentTypeEnum.purchase) {
            let entity = doc.documentEntity as Model.Documents.PurchaseDocument;
            base = Model.Documents.CorrectBaseTypeEnum[Model.Documents.CorrectBaseTypeEnum.tillSlipReturn]
            typeCash = entity.saleType;
        } else {
            let entity = doc.documentEntity as Model.Documents.ReturnOfPurchaseDocument;
            base = Model.Documents.CorrectBaseTypeEnum[Model.Documents.CorrectBaseTypeEnum.tillSlip]
            typeCash = entity.saleType;
        }
        numberTillSlip = doc.number;
        sum = doc.sum;
        this.setState({
            base, numberTillSlip, typeCash, sum
        })
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}