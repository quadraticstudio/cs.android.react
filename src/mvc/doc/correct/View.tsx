import * as React from "react";
import { StyleSheet, Alert, ScrollView } from "react-native";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract"
import * as Model from "../../../model"
import Modal from "./Modal/Controller"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        let { onChange, state } = this.props.controller;
        return (
            <View style={{ flex: 1, width: '100%', padding: 10, backgroundColor: "black" }}>
                <ScrollView style={{ flex: 1, width: '100%' }}>
                    <Abstract.Special.Select.CorrectType
                        config={{
                            object: state,
                            path: 'type',
                            afterSet: onChange
                        }}
                        label="Тип коррекции"
                        type="light"
                    />
                    <Abstract.General.Button
                        onClick={async () => {
                            try {
                                let res = await this.props.controller.props.controller.createStaticComponent(Modal)({ type: state.type })
                                if (res) {
                                    this.props.controller.onSearchDoc(res);
                                }
                            } catch (err) {
                            }
                        }}
                        label="Выбрать документ"
                    />
                    <Abstract.Special.Select.CorrectBaseType
                        config={{
                            object: state,
                            path: 'base',
                            afterSet: onChange
                        }}
                        label="Основание"
                        type="light"
                    />
                    <Abstract.General.NumericInput
                        style={{ color: 'white' }}
                        config={{
                            object: state,
                            path: 'numberTillSlip',
                            afterSet: onChange
                        }}
                        label="№ Чека ККМ"
                    />
                    <Abstract.Special.Select.CashOutType
                        config={{
                            object: state,
                            path: 'typeCash',
                            afterSet: onChange
                        }}
                        label="Тип оплаты"
                        type="light"
                    />
                    <Abstract.General.NumericInput
                        style={{ color: 'white' }}
                        config={{
                            object: state,
                            path: 'sum',
                            afterSet: onChange
                        }}
                        label="Сумма"
                    />
                    <Abstract.General.Button
                        onClick={() => this.props.controller.onSubmit()}
                        label="Подтвердить"
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonsWrapper: {
        position: "absolute",
        bottom: 30,
        flex: 1,
        justifyContent: "center",
        flexDirection: "row"
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        width: "50%",
        justifyContent: "space-around"
    },
    emptyContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    emptyText: {
        textAlign: "center",
    },

});