import * as React from "react";
import Abstract from "../../abstract"
import { Tabs, Tab, Text, ScrollableTab, Header, Container } from "native-base";
import Sale from "./sale"
import Purchase from "./purchase"
import Correct from "./correct/Controller"
import { ComponentProps } from "../../abstract/global/ComponentController";

export interface ControllerProps extends ComponentProps {

}

export interface ControllerState {

}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <Tabs renderTabBar={() => <ScrollableTab tabsContainerStyle={{ backgroundColor: '#070d19' }} />}>
                <Tab heading="Продажа" tabStyle={{ backgroundColor: '#070d19' }} activeTabStyle={{ backgroundColor: '#070d19' }}>
                    <Sale.Select {...this.props} />
                </Tab>
                <Tab heading="Покупка" tabStyle={{ backgroundColor: '#070d19' }} activeTabStyle={{ backgroundColor: '#070d19' }}>
                    <Purchase.Select {...this.props} />
                </Tab>
                <Tab heading="Чек коррекции" tabStyle={{ backgroundColor: '#070d19' }} activeTabStyle={{ backgroundColor: '#070d19' }}>
                    <Correct {...this.props} />
                </Tab>
            </Tabs>
        );
    }
}