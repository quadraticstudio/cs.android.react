import * as React from "react";
import Abstract from "../../abstract"
import { Tabs, Tab, Text, ScrollableTab, Header, Container } from "native-base";
import Return from "./return"
import ReturnOfPurchase from "./returnOfPurchase"
import { ComponentProps } from "../../abstract/global/ComponentController";

export interface ControllerProps extends ComponentProps {

}

export interface ControllerState {

}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <Tabs renderTabBar={() => <ScrollableTab tabsContainerStyle={{ backgroundColor: '#070d19' }} />}>
                <Tab heading="Возврат продажи" tabStyle={{ backgroundColor: '#070d19' }} activeTabStyle={{ backgroundColor: '#070d19' }}>
                    <Return.Select {...this.props} />
                </Tab>
                <Tab heading="Возврат покупки" tabStyle={{ backgroundColor: '#070d19' }} activeTabStyle={{ backgroundColor: '#070d19' }}>
                    <ReturnOfPurchase.Select {...this.props} />
                </Tab>
            </Tabs>
        );
    }
}