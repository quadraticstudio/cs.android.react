import * as React from "react";
import { StyleSheet, Alert, ScrollView } from "react-native";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props);
        this.state = {
            path: null
        }
    }

    render() {
        let { onChange, state } = this.props.controller;
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <ScrollView style={{ flex: 1, width: '100%' }}>
                    <Abstract.General.Input
                        config={{
                            object: state,
                            path: 'count'
                        }}
                        disabled
                        label="Количество позиций"
                    />
                    <Abstract.General.NoNativeInput
                        config={{
                            object: state,
                            path: 'discountSum',
                            afterSet: onChange
                        }}
                        label="Скидка на чек"
                        onEdit={path => this.setState({ path })}
                    />
                    <Abstract.General.NoNativeInput
                        config={{
                            object: state,
                            path: 'discount',
                            afterSet: onChange
                        }}
                        label="Скидка %"
                        onEdit={path => this.setState({ path })}
                    />
                    <Abstract.General.Input
                        config={{
                            object: state,
                            path: 'sum',
                            afterGet: (m, p, v) => {
                                return `${m.sum - m.discountSum} руб.`;
                            }
                        }}
                        disabled
                        label="Итого к оплате"
                    />
                </ScrollView>
                <View style={{ flex: 1, width: '100%' }}>
                    <Abstract.General.Keyboard
                        header={[[
                            {
                                label: 'Оплатить',
                                onClick: () => {
                                    this.props.controller.props.controller.router.navigation.navigate('DocReturnOfPurchaseCash', {
                                        items: this.props.controller.props.controller.params.items,
                                        discountSum: this.props.controller.state.discountSum,
                                        goBack: this.props.controller.goBack
                                    })
                                }
                            }
                        ]]}
                        config={{
                            object: state,
                            path: this.state.path || 'discountSum',
                            afterSet: onChange
                        }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonsWrapper: {
        position: "absolute",
        bottom: 30,
        flex: 1,
        justifyContent: "center",
        flexDirection: "row"
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        width: "50%",
        justifyContent: "space-around"
    },
    emptyContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    emptyText: {
        textAlign: "center",
    },

});