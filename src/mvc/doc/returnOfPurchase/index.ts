import Form from "./form/Controller"
import Select from "./select/Controller"
import Cash from "./cash/Controller"

export default {
    Form, Select, Cash
}