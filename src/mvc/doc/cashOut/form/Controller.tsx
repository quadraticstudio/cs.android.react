import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";
import { LINQ } from "linq";

interface Match {
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    from: string;
    description: string;
    sum: number;
    expenditure: Model.Manual;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            from: null,
            description: null,
            sum: null,
            expenditure: null
        };
    }

    submit = async () => {
        let params = this.props.controller.params;
        let { cashbox, company } = this.props.global;
        if (!this.state.sum)
            throw new Error('Сумма должна быть больше 0')
        // let operation = new Model.CashOperation();
        // operation.cashbox = cashbox;
        // operation.type = Model.CashTypeEnum.output;
        // operation.description = this.state.description;
        // operation.from = this.state.from;
        // operation.sum = this.state.sum;
        // operation.outputType = this.state.expenditure;
        // operation = await operation.save();
        let doc = new Model.Document(null, Model.Documents.CashOutDocument);
        doc.company = company;
        doc.cashbox = cashbox;
        doc.documentEntity.description = this.state.description;
        doc.documentEntity.nds = 0;
        doc.documentEntity.expenditureType = this.state.expenditure && { objectId: this.state.expenditure.id };
        doc.sum = this.state.sum;
        doc.comment = this.state.from && `От: ${this.state.from}`;
        doc = await doc.save();
        this.props.controller.router.navigation.goBack();
    }

    // VIEw

    onSubmit = () => {
        this.props.controller.tryLoad(this.submit);
    }

    onChange = (state: ControllerState, path: string) => {
        this.setState(state);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}