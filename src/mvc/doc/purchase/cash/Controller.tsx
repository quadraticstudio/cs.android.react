import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";
import { ICollectionValue } from "../select/Controller";
import { LINQ } from "linq";

interface Match {
    items: collection.Dictionary<Model.ProductTemplate, ICollectionValue>;
    discountSum: number;
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    readySum: number;
    needSum: number;
    outSum: number;
    typeCash: string;
    email: string;
    phone: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        let params = this.props.controller.params;
        let sum = params.items.toLinq().sum(m => m.value.sum || 0);
        this.state = {
            readySum: 0,
            needSum: sum - (params.discountSum || 0),
            outSum: 0,
            typeCash: 'cash',
            email: null,
            phone: null
        };
    }

    sale = async () => {
        let { cashbox, company, shop, shopstore } = this.props.global;
        let params = this.props.controller.params;
        let items = params.items.toLinq();
        // let operation = new Model.ProductOperation();
        // let pos: Model.ProductOperationObject[] = [];
        // for (let m of items.toArray()) {
        //     let po = new Model.ProductOperationObject();
        //     po.discount = 0;
        //     po.discount = m.value.discount;
        //     po.product = m.key;
        //     po.count = m.value.count;
        //     po = await po.save();
        //     pos.push(po);
        // }
        // for (let key of pos) {
        //     operation.products.add(key);
        // }
        // operation.cashbox = this.props.global.cashbox;
        // operation.sum = this.state.readySum;
        // operation.type = Model.ProductOperationTypeEnum.return;
        // operation.typeReturn = Model.ProductReturnOperationTypeEnum.wocheck;
        // operation.shop = this.props.global.shop;
        // operation.storehouse = this.props.global.shop.shopstore && await this.props.global.shop.shopstore.fetch();
        // operation = await operation.save();
        let doc = new Model.Document(null, Model.Documents.PurchaseDocument);
        doc.company = company;
        doc.cashbox = cashbox;
        doc.sum = this.state.readySum;
        doc.toStorehouse = shopstore;
        doc.documentEntity.sumCard = 0;
        doc.documentEntity.sumCash = this.state.readySum;
        doc.documentEntity.shop = shop && { objectId: shop.id };
        doc.documentEntity.useNds = true;
        doc.documentEntity.saleType = this.state.typeCash;
        doc.documentEntity.email = this.state.email;
        doc.documentEntity.phone = this.state.phone;
        doc.documentEntity.elements = await Promise.all(params.items.toLinq().select(async m => {
            let product = m.key && await m.key.fetch();
            let args = m.value;
            let el = new Model.Documents.PurchaseDocumentElement();
            el.product = product && { objectId: product.id };
            el.count = args.count;
            el.discount = args.discount;
            el.sum = args.sum;
            el.countStore = await product.getAllCount();
            el.ndsKey = product.nds && { objectId: product.nds.id };
            el.ndsValue = (product.nds && product.nds.value || 0) * el.sum;
            el.price = await product.getPurchasePrice();
            return el;
        }).toArray());
        doc.documentEntity.nds = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.ndsValue);
        doc = await doc.save();
        this.props.controller.router.navigation.goBack();
        if (params.goBack)
            params.goBack();
    }

    // VIEw

    onChange = (state: ControllerState, path: string) => {
        state.outSum = state.needSum - state.readySum;
        state["readySumkeyboard"] = String(state.readySum);
        this.setState(state);
    }

    onAddSum = (sum: number) => {
        this.setState({
            "readySumkeyboard":String(this.state.readySum + sum),
            readySum: this.state.readySum + sum
        })
    }

    onSale = () => {
        this.props.controller.tryLoad(this.sale);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}