import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";
import { LINQ } from "linq";

interface Match {
    goBack: () => void;
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ICollectionValue {
    count: number;
    countStore: number;
}

export interface ControllerState {
    items: collection.Dictionary<Model.ProductTemplate, ICollectionValue>;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            items: new collection.Dictionary()
        };
    }

    onAdd = async (model: Model.ProductTemplate) => {
        let { items } = this.state;
        let linq = items.toLinq();
        if (linq.count(m => m.key.id === model.id) > 0) {
            let item = linq.first(m => m.key.id === model.id);
            item.value.count += 1;
        } else {
            linq = linq.concat(new collection.KeyValuePair(model, { count: 1, countStore: 0 }))
        }
        let temp = await Promise.all(linq.select(async m => {
            m.value.countStore = m.value.countStore || await m.key.getAllCount();
            return m;
        }).toArray());
        items = collection.Dictionary.fromArray(temp);
        this.setState({
            items
        })
    }

    submit = async () => {
        let { cashbox, company, shop, shopstore } = this.props.global;
        let params = this.props.controller.params;
        let items = this.state.items.toLinq();
        // let operation = new Model.ProductOperation();
        // let pos: Model.ProductOperationObject[] = [];
        // for (let m of items.toArray()) {
        //     let po = new Model.ProductOperationObject();
        //     po.discount = 0;
        //     po.product = m.key;
        //     po.countReal = m.value.countStore;
        //     po.count = m.value.count;
        //     po = await po.save();
        //     pos.push(po);
        // }
        // for (let key of pos) {
        //     operation.products.add(key);
        // }
        // operation.cashbox = this.props.global.cashbox;
        // operation.type = Model.ProductOperationTypeEnum.inventory;
        // operation.shop = this.props.global.shop;
        // operation.storehouse = this.props.global.shop.shopstore && await this.props.global.shop.shopstore.fetch();
        // operation = await operation.save();

        let doc = new Model.Document(null, Model.Documents.InventoryDocument);
        doc.company = company;
        doc.cashbox = cashbox;
        doc.fromStorehouse = shopstore;
        doc.documentEntity.elements = await Promise.all(this.state.items.toLinq().select(async m => {
            let product = m.key && await m.key.fetch();
            let args = m.value;
            let el = new Model.Documents.InventoryDocumentElement();
            el.product = product && { objectId: product.id };
            el.count = args.count;
            el.countStore = args.countStore;
            el.price = await product.getPurchasePrice();
            el.countDifference = el.count - el.countStore;
            el.sum = Math.ceil((el.countDifference * el.price) * 100) / 100;
            return el;
        }).toArray());

        doc.documentEntity.excess = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.sum > 0 ? m.sum : 0);
        doc.documentEntity.shortage = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.sum < 0 ? Math.abs(m.sum) : 0);
        doc.documentEntity.actualBalance = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.count);
        doc.documentEntity.accountingBalance = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.countStore);
        doc.sum = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.sum);
        doc = await doc.save();

        this.props.controller.router.navigation.goBack();
        if (params.goBack)
            params.goBack();
    }

    // VIEW

    onAddProduct = async (model: Model.ProductTemplate) => {
        this.props.controller.tryLoad(this.onAdd, null, model);
    }

    onSubmit = () => {
        this.props.controller.tryLoad(this.submit)
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}