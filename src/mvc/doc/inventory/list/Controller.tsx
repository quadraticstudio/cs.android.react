import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";
import Query from "../../../../abstract/database/lib/query";

export interface ControllerProps extends ComponentProps {

}

export interface ICollectionValue {
    count: number;
    countStore: number;
    discount: number;
    sum: number;
}

export interface ControllerState {
    items: Model.Document[];
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.onLoad);
    }

    onLoad = async () => {
        let { items } = this.state;
        let linq = await new Query(Model.Document).find();
        linq = linq.where(m => m.type == Model.DocumentTypeEnum.inventory);
        items = linq.toArray();
        this.setState({
            items
        })
    }

    // VIEW

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}