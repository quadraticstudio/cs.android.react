import * as React from "react";
import { StyleSheet, Alert } from "react-native";
import { View, Text } from 'native-base';
import Controller, { ICollectionValue } from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model"
import * as collection from "collection";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Abstract.General.ListCollection
                    data={this.props.controller.state.items}
                    header={[
                        {
                            title: 'Наименование',
                            render: (obj: Model.Document) => {
                                return `${obj.title} ${obj.number}`;
                            }
                        },
                        {
                            title: 'Статус',
                            render: (obj: Model.Document) => {
                                return obj.done;
                            }
                        },
                        {
                            title: 'Ответственный',
                            render: (obj: Model.Document) => {
                                return obj.user && obj.user.get('name') && obj.user.attributes && obj.user.attributes.name;
                            }
                        },
                        {
                            title: 'Дата',
                            render: (obj: Model.Document) => {
                                return obj.date;
                            }
                        }
                    ]}
                />
                <Abstract.General.Fab
                    onClick={() => this.props.controller.props.controller.router.navigation.navigate("DocInventorySelect", { goBack: this.props.controller.props.controller.router.navigation.goBack })}
                    icon="md-add"
                    position="bottomRight"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    absoluteWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    buttonsWrapper: {
        position: "absolute",
        bottom: 100,
        flex: 1,
        justifyContent: "center",
        flexDirection: "row"
    },
    saleWrapper: {
        position: 'absolute',
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        width: "50%",
        justifyContent: "space-around"
    },
    emptyContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    emptyText: {
        textAlign: "center",
    },
    saleText: {
        textAlign: 'center',
        fontSize: 20,
        color: '#3498db'
    }
});