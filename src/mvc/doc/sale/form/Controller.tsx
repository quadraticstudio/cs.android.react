import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";
import { ICollectionValue } from "../select/Controller";
import { LINQ } from "linq";

interface Match {
    items: collection.Dictionary<Model.ProductTemplate, ICollectionValue>;
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    count: number;
    discountSum: number;
    discount: number;
    sum: number;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        let params = this.props.controller.params;
        let linq = params.items && params.items.toLinq() || LINQ.fromArray<collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>>([]);
        this.state = {
            count: linq.count(),
            discountSum: 0,
            discount: 0,
            sum: linq.sum(m => m.value.sum || 0)
        };
    }

    // VIEw

    onChange = (state: ControllerState, path: string) => {
        if (path == 'discountSum') {
            if (state.sum)
                state.discount = (state.discountSum * 100 || 0) / state.sum;
        }
        else if (path == 'discount') {
            state.discountSum = state.sum * (state.discount>100?100:state.discount/100 || 0);
        }
        this.setState(state);
    }

    goBack = () => {
        let params = this.props.controller.params;
        this.props.controller.router.navigation.goBack();
        if (params.goBack)
            params.goBack();
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}