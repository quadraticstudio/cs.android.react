import * as React from "react";
import { StyleSheet, Alert } from "react-native";
import { View, Text } from 'native-base';
import Controller, { ICollectionValue } from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model"
import * as collection from "collection";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderButtons = () => {
        let count = this.props.controller.state.items.count();
        return (
            <View style={styles.absoluteWrapper}>
                <View style={styles.buttonsWrapper}>
                    <View style={styles.buttons}>
                        <Abstract.General.Fab
                            icon="md-trash"
                            backgroundColor="#01579B"
                            onClick={() => {
                                if (count)
                                    Alert.alert('Удаление операции', 'Удалить операцию?', [
                                        {
                                            text: 'Удалить',
                                            onPress: () => {
                                                this.props.controller.props.controller.router.navigation.goBack();
                                            }
                                        },
                                        {
                                            text: 'Отменить'
                                        }
                                    ])
                                else
                                    Alert.alert('Удаление операции', 'У Вас нет операций')
                            }}
                        />
                        <Abstract.General.Fab
                            icon="md-add"
                            backgroundColor="#4CAF50"
                            onClick={() => {
                                this.props.controller.props.controller.router.navigation.navigate('GoodsList', { onClickProduct: this.props.controller.onAddProduct })
                            }}
                        />
                        <Abstract.General.Fab
                            icon="md-barcode"
                            backgroundColor="#4FC3F7"
                            onClick={() => {
                                Alert.alert('Сканиерование штрихкода', 'Вы можете сканировать штрихкоды в данном окне')
                            }}
                        />
                    </View>
                </View>
                <View style={count <= 0 ? { display: 'none' } : styles.saleWrapper}>
                    <Text style={styles.saleText}>
                        {`${this.props.controller.state.items.toLinq().sum(m => m.value.sum || 0)} руб.`}
                    </Text>
                    <Abstract.General.Button
                        label="Оплатить"
                        onClick={() => {
                            this.props.controller.props.controller.router.navigation.navigate('DocSaleForm', { items: this.props.controller.state.items, goBack: this.props.controller.props.controller.router.navigation.goBack })
                        }}
                    />
                </View>
            </View>
        );
    }

    renderContent = () => {
        let { items } = this.props.controller.state;
        return (
            <View style={styles.emptyContainer}>
                {items.count() <= 0 ? this.renderEmptyContent() : this.renderListProducts()}
            </View>
        );
    }

    renderListProducts = () => {
        let { items } = this.props.controller.state;
        return (
            <Abstract.General.ListCollection
                data={items.asArray()}
                header={[
                    {
                        title: 'Название товара',
                        render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => obj.key.name
                    },
                    {
                        title: 'Номер',
                        render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => obj.key.number
                    },
                    {
                        title: 'Цена',
                        render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>, index) => {
                            return <Abstract.General.NumericInput
                                style={{ color: 'white' }}
                                config={{
                                    object: items.asArray(),
                                    path: `${index}.value.price`,
                                    afterSet: m => this.props.controller.onChange(collection.Dictionary.fromArray(m))
                                }}
                            />
                        }
                    },
                    {
                        title: 'Количество',
                        render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>, index) => {
                            return <Abstract.General.NumericInput
                                style={{ color: 'white' }}
                                config={{
                                    object: items.asArray(),
                                    path: `${index}.value.count`,
                                    afterSet: m => this.props.controller.onChange(collection.Dictionary.fromArray(m))
                                }}
                            />
                        }
                    },
                    {
                        title: 'Единица измерения',
                        render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => obj.key.unit && obj.key.unit.key || 'шт.'
                    }, {
                        title: 'Скидка',
                        render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>, index) => {
                            return <Abstract.General.NumericInput
                                style={{ color: 'white' }}
                                config={{
                                    object: items.asArray(),
                                    path: `${index}.value.discount`,
                                    afterGet: (m, p, v) => {
                                        let discount = Number(v) * 100
                                        return discount>100?100:discount;
                                    },
                                    beforeSet: (m, p, v) => {
                                        return Number(v) / 100;
                                    },
                                    afterSet: m => this.props.controller.onChange(collection.Dictionary.fromArray(m))
                                }}
                            />
                        }
                    },
                    {
                        title: 'Сумма',
                        render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => `${obj.value.sum || 0} руб.`
                    }
                ]}
            />
        );
        // return (
        //     <Abstract.General.TableBerish.LINQTable
        //         data={items.asArray()}
        //         header={[
        //             {
        //                 columns: [
        //                     {
        //                         title: 'Название товара',
        //                         render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => obj.key.name
        //                     },
        //                     {
        //                         title: 'Номер',
        //                         render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => obj.key.number
        //                     }
        //                 ]
        //             },
        //             {
        //                 columns: [
        //                     {
        //                         title: 'Количество',
        //                         render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>, index) => {
        //                             return <Abstract.General.NumericInput
        //                                 style={{ color: 'white' }}
        //                                 config={{
        //                                     object: items.asArray(),
        //                                     path: `${index}.value.count`,
        //                                     afterSet: m => this.props.controller.setState({
        //                                         items: collection.Dictionary.fromArray(m)
        //                                     })
        //                                 }}
        //                             />
        //                         }
        //                     },
        //                     {
        //                         title: 'Единица измерения',
        //                         render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => obj.key.unit && obj.key.unit.key || 'шт.'
        //                     }
        //                 ]
        //             },
        //             {
        //                 columns: [
        //                     {
        //                         title: 'Скидка',
        //                         render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>, index) => {
        //                             return <Abstract.General.NumericInput
        //                                 style={{ color: 'white' }}
        //                                 config={{
        //                                     object: items.asArray(),
        //                                     path: `${index}.value.discount`,
        //                                     afterSet: m => this.props.controller.setState({
        //                                         items: collection.Dictionary.fromArray(m)
        //                                     })
        //                                 }}
        //                             />
        //                         }
        //                     },
        //                     {
        //                         title: 'Сумма',
        //                         render: (obj: collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>) => `${obj.value.sum || 0} руб.`
        //                     }
        //                 ]
        //             }
        //         ]}
        //     />
        // );
    }

    renderEmptyContent = () => {
        return (
            <Text style={styles.emptyText}>
                Здесь пока пусто {"\n"}
                Для наполнения чека отсканируйте {"\n"}
                штрих-код или добавьте товар из базы
            </Text>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#070d19" }}>
                {this.renderContent()}
                {this.renderButtons()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    absoluteWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    buttonsWrapper: {
        position: "absolute",
        bottom: 100,
        flex: 1,
        justifyContent: "center",
        flexDirection: "row"
    },
    saleWrapper: {
        position: 'absolute',
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%'
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        width: "50%",
        justifyContent: "space-around"
    },
    emptyContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    emptyText: {
        textAlign: "center",
        color: "white"
    },
    saleText: {
        textAlign: 'center',
        fontSize: 20,
        color: '#3498db'
    }
});