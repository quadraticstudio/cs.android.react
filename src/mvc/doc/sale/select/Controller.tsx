import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";

export interface ControllerProps extends ComponentProps {

}

export interface ICollectionValue {
    price: number;
    count: number;
    discount: number;
    sum: number;
}

export interface ControllerState {
    items: collection.Dictionary<Model.ProductTemplate, ICollectionValue>;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            items: new collection.Dictionary()
        };
    }

    onAdd = async (model: Model.ProductTemplate) => {
        let { items } = this.state;
        let linq = items.toLinq();
        if (linq.count(m => m.key.id === model.id) > 0) {
            let item = linq.first(m => m.key.id === model.id);
            item.value.count += 1;
        } else {
            linq = linq.concat(new collection.KeyValuePair(model, { count: 1, discount: 0, sum: 0, price: 0 }))
        }
        let temp = await Promise.all(linq.select(async m => {
            m.value.price = m.value.price || await m.key.getPurchasePrice();
            m.value.sum = m.value.count * m.value.price * (1 - m.value.discount);
            return m;
        }).toArray());
        items = collection.Dictionary.fromArray(temp);
        this.setState({
            items
        })
    }

    // VIEW

    onAddProduct = async (model: Model.ProductTemplate) => {
        this.props.controller.tryLoad(this.onAdd, null, model);
    }

    onChange = (items: collection.Dictionary<Model.ProductTemplate, ICollectionValue>) => {
        items = collection.Dictionary.fromArray(items.toLinq().select(m => {
            m.value.sum = m.value.count * m.value.price * (1 - m.value.discount);
            return m;
        }).toArray());
        this.setState({ items });
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}