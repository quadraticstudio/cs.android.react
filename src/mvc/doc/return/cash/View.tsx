import * as React from "react";
import { StyleSheet, Alert } from "react-native";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model"
import { LINQ } from "linq";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props);
        this.state = {
            path: null
        }
    }

    render() {
        let { onChange, state } = this.props.controller;
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <Abstract.Special.Select.CashOutType
                    config={{
                        object: state,
                        path: 'typeCash',
                        afterSet: onChange
                    }}
                    label="Тип оплаты"
                />
                <Abstract.General.Input
                    config={{
                        object: state,
                        path: 'email'
                    }}
                    keyboardType="email-address"
                    label="E-mail"
                />
                <Abstract.General.Input
                    config={{
                        object: state,
                        path: 'phone'
                    }}
                    keyboardType="phone-pad"
                    label="Телефон"
                />
                <Abstract.General.Input
                    config={{
                        object: state,
                        path: 'needSum'
                    }}
                    disabled
                    label="ИТОГО К ОПЛАТЕ"
                />
                <Abstract.General.NoNativeInput
                    config={{
                        object: state,
                        path: 'readySum',
                        afterSet: onChange
                    }}
                    label="Принято"
                    onEdit={path => this.setState({ path })}
                />
                <Abstract.General.Input
                    config={{
                        object: state,
                        path: ' outSum',
                        afterGet: (m, p, v) => {
                            return `${v || 0} руб.`;
                        }
                    }}
                    disabled
                    label="Сдача"
                />
                <Abstract.General.Button
                    label="Оплатить"
                    onClick={() => {
                        this.props.controller.onSale();
                    }}
                />
                <Abstract.General.Keyboard
                    header={[
                        [
                            {
                                label: '10',
                                onClick: () => this.props.controller.onAddSum(10)
                            },
                            {
                                label: '500',
                                onClick: () => this.props.controller.onAddSum(500)
                            }
                        ], [
                            {
                                label: '50',
                                onClick: () => this.props.controller.onAddSum(50)
                            },
                            {
                                label: '1000',
                                onClick: () => this.props.controller.onAddSum(1000)
                            }
                        ], [
                            {
                                label: '100',
                                onClick: () => this.props.controller.onAddSum(100)
                            },
                            {
                                label: '5000',
                                onClick: () => this.props.controller.onAddSum(5000)
                            }
                        ], [
                            {
                                label: 'Без сдачи',
                                onClick: () => {
                                    onChange({
                                        ...this.props.controller.state,
                                        readySum: this.props.controller.state.needSum
                                    } as any, this.state.path)
                                }
                            }
                        ]
                    ]}
                    config={{
                        object: state,
                        path: this.state.path || 'readySum',
                        afterSet: onChange
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonsWrapper: {
        position: "absolute",
        bottom: 30,
        flex: 1,
        justifyContent: "center",
        flexDirection: "row"
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        width: "50%",
        justifyContent: "space-around"
    },
    emptyContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    emptyText: {
        textAlign: "center",
    },

});