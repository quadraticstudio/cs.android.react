import Sale from "./sale"
import Return from "./return"
import ProviderIn from "./providerIn"
import ProviderOut from "./providerOut"
import Clean from "./clean"
import Inventory from "./inventory"
import CashIn from "./cashIn"
import CashOut from "./cashOut"
import SaleController from "./SaleController";
import ReturnController from "./ReturnController";
import Purchase from "./purchase"
import ReturnOfPurchase from "./returnOfPurchase"

export default {
    Sale, Return, ProviderIn, ProviderOut, Clean, Inventory, CashIn, CashOut, SaleController, ReturnController, Purchase, ReturnOfPurchase
}