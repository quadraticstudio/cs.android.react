import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";
import { ICollectionValue } from "../select/Controller";
import { LINQ } from "linq";

interface Match {
    items: collection.Dictionary<Model.ProductTemplate, ICollectionValue>;
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    count: number;
    sum: number;
    partner: Model.Partner;
    description: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        let params = this.props.controller.params;
        let linq = params.items && params.items.toLinq() || LINQ.fromArray<collection.KeyValuePair<Model.ProductTemplate, ICollectionValue>>([]);
        this.state = {
            count: linq.count(),
            sum: linq.sum(m => m.value.providerOutPrice * m.value.count || 0),
            partner: null,
            description: null
        };
    }

    submit = async () => {
        let { cashbox, company, shop, shopstore } = this.props.global;
        let params = this.props.controller.params;
        let items = params.items.toLinq();
        // let operation = new Model.ProductOperation();
        // let pos: Model.ProductOperationObject[] = [];
        // for (let m of items.toArray()) {
        //     let po = new Model.ProductOperationObject();
        //     po.discount = 0;
        //     po.countReal = m.value.countStore;
        //     po.price = m.value.providerOutPrice;
        //     po.product = m.key;
        //     po.count = m.value.count;
        //     po = await po.save();
        //     pos.push(po);
        // }
        // for (let key of pos) {
        //     operation.products.add(key);
        // }
        // operation.cashbox = this.props.global.cashbox;
        // operation.sum = this.state.sum;
        // operation.type = Model.ProductOperationTypeEnum.providerOut;
        // operation.description = this.state.description;
        // operation.shop = this.props.global.shop;
        // operation.storehouse = this.props.global.shop.shopstore && await this.props.global.shop.shopstore.fetch();
        // operation = await operation.save();

        let doc = new Model.Document(null, Model.Documents.ProviderOutDocument);
        doc.company = company;
        doc.cashbox = cashbox;
        doc.partner = this.state.partner;
        doc.fromStorehouse = shopstore;
        doc.comment = this.state.description;
        doc.documentEntity.elements = await Promise.all(params.items.toLinq().select(async m => {
            let product = m.key && await m.key.fetch();
            let args = m.value;
            let el = new Model.Documents.ProviderOutDocumentElement();
            el.product = product && { objectId: product.id };
            el.count = args.count;
            el.price = args.providerOutPrice;
            el.sum = el.price * el.count;
            el.countStore = args.countStore;
            el.ndsKey = product.nds && { objectId: product.nds.id };
            el.ndsValue = (product.nds && product.nds.value || 0) * el.sum;
            return el;
        }).toArray());
        doc.documentEntity.nds = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.ndsValue);
        doc.sum = LINQ.fromArray(doc.documentEntity.elements).sum(m => m.sum);
        doc = await doc.save();

        this.props.controller.router.navigation.goBack();
        if (params.goBack)
            params.goBack();
    }

    // VIEw

    onSubmit = () => {
        this.props.controller.tryLoad(this.submit);
    }

    onChange = (state: ControllerState, path: string) => {
        this.setState(state);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}