import * as React from "react";
import View from "./View"
import Abstract from "../../../../abstract"
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import * as Model from "../../../../model"
import * as collection from "collection";

export interface ControllerProps extends ComponentProps {

}

export interface ICollectionValue {
    count: number;
    salePrice: number;
    countStore: number;
}

export interface ControllerState {
    items: collection.Dictionary<Model.ProductTemplate, ICollectionValue>;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            items: new collection.Dictionary()
        };
    }

    onAdd = async (model: Model.ProductTemplate) => {
        let { items } = this.state;
        let linq = items.toLinq();
        if (linq.count(m => m.key.id === model.id) > 0) {
            let item = linq.first(m => m.key.id === model.id);
            item.value.count += 1;
        } else {
            linq = linq.concat(new collection.KeyValuePair(model, { count: 1, countStore: 0, providerInPrice: 0, salePrice: 0 }))
        }
        let temp = await Promise.all(linq.select(async m => {
            m.value.salePrice = m.value.salePrice || await m.key.getPurchasePrice();
            m.value.countStore = m.value.countStore || await m.key.getAllCount();
            return m;
        }).toArray());
        items = collection.Dictionary.fromArray(temp);
        this.setState({
            items
        })
    }

    // VIEW

    onAddProduct = async (model: Model.ProductTemplate) => {
        this.props.controller.tryLoad(this.onAdd, null, model);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}