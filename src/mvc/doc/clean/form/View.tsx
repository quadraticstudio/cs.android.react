import * as React from "react";
import { StyleSheet, Alert } from "react-native";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props);
        this.state = {
            path: null
        }
    }

    render() {
        let { onChange, state } = this.props.controller;
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <Abstract.General.Input
                    config={{
                        object: state,
                        path: 'count'
                    }}
                    disabled
                    label="Количество позиций"
                />
                <Abstract.General.Input
                    config={{
                        object: state,
                        path: 'sum',
                        afterGet: (m, p, v) => {
                            return `${m.sum} руб.`;
                        }
                    }}
                    disabled
                    label="Итого"
                />
                <Abstract.General.Input
                    config={{
                        object: state,
                        path: 'description',
                        afterSet: onChange
                    }}
                    label="Основание"
                />
                <Abstract.General.Button
                    onClick={() => this.props.controller.onSubmit()}
                    label="Подтвердить"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonsWrapper: {
        position: "absolute",
        bottom: 30,
        flex: 1,
        justifyContent: "center",
        flexDirection: "row"
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        width: "50%",
        justifyContent: "space-around"
    },
    emptyContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    emptyText: {
        textAlign: "center",
    },

});