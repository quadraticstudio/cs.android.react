import * as React from "react";
import { StyleSheet, Alert } from "react-native";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        let { onChange, state } = this.props.controller;
        return (
            <View style={{ flex: 1, width: '100%', padding: 10, backgroundColor: "black" }}>
                <Abstract.General.Input
                    style={{ color: 'white' }}
                    type="light"
                    config={{
                        object: state,
                        path: 'from',
                        afterSet: onChange
                    }}
                    label="От кого"
                />
                <Abstract.General.Input
                    style={{ color: 'white' }}
                    type="light"
                    config={{
                        object: state,
                        path: 'description',
                        afterSet: onChange
                    }}
                    label="Основание"
                />
                <Abstract.General.NumericInput
                    style={{ color: 'white' }}
                    config={{
                        object: state,
                        path: 'sum',
                        afterSet: onChange
                    }}
                    label="Сумма"
                />
                <Abstract.General.Button
                    onClick={() => this.props.controller.onSubmit()}
                    label="Подтвердить"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonsWrapper: {
        position: "absolute",
        bottom: 30,
        flex: 1,
        justifyContent: "center",
        flexDirection: "row"
    },
    buttons: {
        flex: 1,
        flexDirection: "row",
        width: "50%",
        justifyContent: "space-around"
    },
    emptyContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    emptyText: {
        textAlign: "center",
    },

});