import * as React from "react";
import { View, Text, Button } from 'native-base';
import { ComponentProps } from "../../abstract/Global/index";
import { NavigationScreenOption } from "react-navigation";

export interface ViewProps extends ComponentProps {
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    static navigationOptions: NavigationScreenOption<{}> = {

    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
                <Text style={{ fontSize: 30, color: '#fff', textAlign: 'center' }}>Ведется разработка</Text>
                <Button onPressOut={() => {
                    this.props.controller.router.navigation.navigate("Home")
                }}><Text>123</Text></Button>
            </View>
        );
    }
}