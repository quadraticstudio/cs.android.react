import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../../abstract";
import { ComponentProps } from "../../../../abstract/global/index";

interface State {
    modal: boolean;
}

export default class Main extends React.Component<ComponentProps, State> {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        }
    }

    renderModalContent = () => {
        return (
            <View>
                <Abstract.General.Input
                    label="Наименование*"
                    config={{
                        object: this.state,
                        path: "name",
                        afterSet: m => this.setState(m)
                    }}
                />
                <Abstract.General.Select
                    label="Порядок округления"
                    values={[
                        "100",
                        "Нет",
                        "1",
                        "10",
                        "5"
                    ]}
                    config={{
                        object: this.state,
                        path: "okr",
                        afterSet: m => this.setState(m)
                    }}
                />
                <Abstract.General.Input
                    label="Наценка"
                    config={{
                        object: this.state,
                        path: "price",
                        afterSet: m => this.setState(m)
                    }}
                />
                <Abstract.General.Button
                    label="Ок"
                />
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Abstract.General.Table
                    items={[
                        [
                            <Text>Закупочная</Text>,
                            <Text>0.0%</Text>
                        ],
                        [
                            <Text>Розничная</Text>,
                            <Text>0.0%</Text>
                        ],
                        [
                            <Text>Оптовая</Text>,
                            <Text>0.0%</Text>
                        ]
                    ]}
                />
                <Abstract.General.Fab
                    icon="md-add"
                    onClick={() => this.setState({ modal: true })}
                    position="bottomRight"
                />
                <Abstract.General.Modal
                    visible={this.state.modal}
                    content={this.renderModalContent()}
                    title="Добавить новый тип"
                />
            </View>
        );
    }
}