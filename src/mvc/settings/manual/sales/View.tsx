import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../../abstract";
import { ComponentProps } from "../../../../abstract/global/index";

export default class Main extends React.Component<ComponentProps> {

    render() {
        return (
            <View>
                <Abstract.General.Table
                    items={[
                        [
                            <Text>Максимальная скидка на товар</Text>,
                            <Text>0%</Text>
                        ],
                        [
                            <Text>Максимальная скидка на чек</Text>,
                            <Text>0%</Text>
                        ],
                        [
                            <Text>Скидка по картам 100000-199999</Text>,
                            <Text>0%</Text>
                        ],
                        [
                            <Text>Скидка по картам 200000-299999</Text>,
                            <Text>0%</Text>
                        ],
                        [
                            <Text>Скидка по картам 300000-399999</Text>,
                            <Text>0%</Text>
                        ]
                    ]}
                />
            </View>
        );
    }
}