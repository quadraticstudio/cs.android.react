import Turn from "./turn"
import General from "./general"
import Manual from "./manual"

export default {
    Turn,
    General,
    Manual
}