import * as React from "react";
import { View, Text, Button } from "native-base";
import { StyleSheet, Alert } from "react-native";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";

export interface SettingsItem {
    title: string;
    config: any;
    divider?: boolean;
    switch?: boolean;
    modal?: boolean;
}

export default class Main extends React.Component<ComponentProps> {

    constructor (props) {
        super(props);
        this.state = {};
    }

    getItems = () => {
        return [
            { title: "Открытие смены", divider: true },
            { title: "Печать документа открытия см.", switch: true, config: {path: "eq1"} },
            { title: "Закрытие смены", divider: true },
            { title: "Печатать X-отчет (без гашения)", switch: true, config: {path: "eq2"} }, 
            { title: "Печатать Z-отчет (с гашением)", switch: true, config: {path: "eq3"} }, 
            { title: "Закрывать смену платежного терминала", switch: true, config: {path: "eq4"} },
            { title: "Выгрузка данных при закрытии смены" }
        ];
    }

    onClick = (index:number, item: SettingsItem) => {
        let { state } = this;
        if (item.switch) {
            state[item.config.path] = !state[item.config.path];
            this.setState(state);
        }
        if (item.modal) {
            
        }
    }

    renderRow = (index: number, item: SettingsItem) => {
        return (
            <View style={styles.item}>
                { item.switch ? <Abstract.General.Switch
                    label={item.title}
                    textStyle={{color: "white"}}
                    config={{
                        object: this.state,
                        path: item.config.path,
                        afterSet: (m) => this.setState(m)
                    }}
                /> : <Text style={{color: "white"}}>{item.title}</Text> }
            </View>
        );
    }

    render() {
        return (
            <Abstract.General.List
                items={this.getItems()}
                renderRow={this.renderRow}
                onClick={this.onClick}
            />
        );
    }
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between"
    }
});