import Form from "./form/Controller";
import List from "./list/Controller";
import Group from "./group/Controller";

export default {
    List, Form, Group
}