import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract";
import { LINQ } from "linq";
import { GoodListItem } from "../../../abstract/goods/GoodList";
import { StaticComponentProps } from "../../../abstract/global/ComponentController";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props)
        this.state = {
            modal: false
        };
    }
    render() {
        let { currentGroup, groups, items } = this.props.controller.state;
        return (
            <View style={{ flex: 1 }}>
                <Abstract.Goods.List
                    onClick={this.props.controller.onClick}
                    items={[
                        ...groups,
                        ...items
                    ]}
                />
                <Abstract.General.Fab
                    onClick={async () => {
                        try {
                            let result = await this.props.controller.props.controller.createStaticComponent(ProductModal)();
                            if (result == 'group')
                                this.props.controller.onClickGroup()
                            else if (result == 'product')
                                this.props.controller.onClickProduct();
                        } catch (err) {

                        }
                    }}
                    icon="md-add"
                    position="bottomRight"
                />
            </View>
        );
    }
}

class ProductModal extends React.Component<StaticComponentProps<'group' | 'product'>, {}>{
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Abstract.General.ModalForm
                title="Добавить"
                resolve={this.props.resolve}
                reject={this.props.reject}
            >
                <Abstract.General.Button
                    label="Группа"
                    props={{
                        full: true
                    }}
                    style={{marginBottom: 10}}
                    onClick={() => this.props.resolve('group')}
                />
                <Abstract.General.Button
                    label="Товар"
                    props={{
                        full: true
                    }}
                    onClick={() => this.props.resolve('product')}
                />
            </Abstract.General.ModalForm>
        );
    }
}