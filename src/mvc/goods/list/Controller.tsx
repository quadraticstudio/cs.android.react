import * as React from "react";
import View from "./View"
import * as Model from "../../../model"
import { ComponentProps, message } from "../../../abstract/global/index";
import QueryLINQ from "parse-query-to-linq";
import { GoodListItem } from "../../../abstract/goods/GoodList";
import { LINQ } from "linq";

interface Match {
    currentGroup: Model.Group;
    onClickProduct: (product: Model.ProductTemplate) => any;
    goBack: () => any;
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    currentGroup: Model.Group;
    groups: GoodListItem<Model.Group>[];
    items: GoodListItem<Model.Group>[];
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            currentGroup: null,
            groups: [],
            items: []
        };
    }

    componentDidMount() {
        //this.props.controller.tryLoad(this.load);
        this.load();
    }

    componentWillUnmount() {
        this.props.controller.stopExecute();
    }

    loadCurrentGroup = async (group?: Model.Group) => {
        let { company } = this.props.global;
        group = group || company.productGroup;
        group = group && await group.fetch();
        let rawGroups = await group.childs.find();
        let groupsRaw = rawGroups.where(m => m.type === Model.GroupItemTypeEnum.group).toArray();
        let itemsRaw = await Promise.all(rawGroups.where(m => m.type === Model.GroupItemTypeEnum.product).select(async m => {
            m.pointer = m.pointer && await m.pointer.fetch();
            return m;
        }).toArray());
        let groups = LINQ.fromArray(groupsRaw || []).select(m => {
            let obj: GoodListItem<Model.Group> = {
                object: m,
                title: m.name,
                isGroup: true
            }
            return obj;
        }).toArray();
        let items = await Promise.all(LINQ.fromArray(itemsRaw || []).select(async m => {
            let product = m.pointer;
            if (!product)
                return null;
            let count = await product.getAllCount();
            let price = await product.getPurchasePrice();
            let unit = product.unit && await product.unit.fetch();
            let obj: GoodListItem<Model.Group> = {
                object: m,
                title: product.name,
                number: product.number,
                count: count,
                price: price,
                unit: unit.key
            };
            return obj;
        }).toArray());
        items = LINQ.fromArray(items || []).notNull().toArray();
        this.setState({
            currentGroup: group, groups, items
        })
    }

    load = async () => {
        let params = this.props.controller.params;
        this.props.controller.tryLoad(this.loadCurrentGroup, null, params.currentGroup);
    }

    onUpdate = (group: Model.Group) => {
        this.load();
    }

    // VIEW

    onClick = async (index: number, object: GoodListItem) => {
        if (object.isGroup)
            return this.onClickGroup(object);
        return this.onClickProduct(object);
    }

    onClickGroup = async (group?: GoodListItem<Model.Group>) => {
        let params = this.props.controller.params;
        if (!group)
            return this.props.controller.router.navigation.navigate('GoodsGroup', { id: this.state.currentGroup.id, action: 'add', onUpdate: this.onUpdate })
        this.props.controller.router.navigation.navigate('GoodsList', { currentGroup: group.object, onClickProduct: params.onClickProduct, goBack: this.goBack });
        //this.load(group.object);
    }

    goBack = () => {
        let params = this.props.controller.params;
        this.props.controller.router.navigation.goBack();
        if (params.goBack)
            params.goBack();
    }

    onClickProduct = async (item?: GoodListItem<Model.Group>) => {
        let params = this.props.controller.params;
        if (!item)
            return this.props.controller.router.navigation.navigate('GoodsForm', { id: this.state.currentGroup.id, action: 'add', onUpdate: this.onUpdate })
        if (params.onClickProduct) {
            params.onClickProduct(item.object.pointer);
            this.goBack();
            return;
        }
        this.props.controller.router.navigation.navigate('GoodsForm', { id: item.object.pointer.id, action: 'edit', onUpdate: this.onUpdate });
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}