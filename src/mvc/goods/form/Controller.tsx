import * as React from "react";
import * as Parse from "parse";
import * as Model from "../../../model"
import View from "./View"
import { LINQ } from "linq";
import QueryLINQ from "parse-query-to-linq";
import * as collection from "collection";
import { ComponentProps } from "../../../abstract/global/index";
import Query from "../../../abstract/database/lib/query";

interface Match {
    id: string,
    action: 'add' | 'edit';
    onUpdate: (currentGroup: Model.Group) => any;
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    product: Model.ProductTemplate;
    defaultGroup: Model.Group;
    //defaultChildGroups: LINQ<Model.Group>;
    defaultChildGroups: LINQ<string>;
    currentGroup: Model.Group;
    productGroup: Model.Group;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            product: new Model.ProductTemplate(),
            defaultGroup: null,
            defaultChildGroups: LINQ.fromArray([]),
            currentGroup: null,
            productGroup: null
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.load);
    }

    load = async () => {
        let { action, id } = this.props.controller.params;
        let {
            product, currentGroup, defaultGroup, productGroup, defaultChildGroups
        } = this.state;
        let linq = await new Query(Model.Manual).find();
        let manuals = linq.where(m => m.type === Model.ManualTypeEnum.priceTypes).orderBy(m => m.number);
        if (action === 'add') {
            product = product || new Model.ProductTemplate();
            if (id)
                defaultGroup = await new Query(Model.Group).get(id);
            if (defaultGroup) {
                product.nds = defaultGroup.nds || await Model.Manual.getNoNds();
                product.markup = defaultGroup.markup;
                product.partner = defaultGroup.partner;
            }
            product.company = (await new Query(Model.Company).get(this.props.global.company.id)) as Model.Company;
            product.barcodes = [];
            product.unit = await Model.Manual.getDefaultUnit();

            /*product.prices = await Promise.all(LINQ.fromArray(product.prices || []).select(async m => {
                m.key = m.key && await m.key.fetch();
                return m;
            }).toArray());
            product.prices = manuals.select(m => {
                let kv = new collection.KeyValuePair(m, undefined);
                return kv;
            }).toArray();*/
        } else {
            product = (await new Query(Model.ProductTemplate).get(id)) as Model.ProductTemplate;
            let linqGroup = await new Query(Model.Group).find();
            productGroup = linqGroup.firstOrNull(m => (m.pointer && m.pointer.id == (product && product.id))) as Model.Group;
            defaultGroup = productGroup.parent && await productGroup.parent.fetch();

            product.barcodes = product.barcodes || [];
            let linq = LINQ.fromArray(product.prices || []);
            /*product.prices = await Promise.all(LINQ.fromArray(product.prices || []).select(async m => {
                m.key = m.key && await m.key.fetch();
                return m;
            }).toArray());
            product.prices = manuals.select(m => {
                let value = linq.firstOrNull(k => k.key.id === m.id)
                let kv = new collection.KeyValuePair(m, value && value.value);
                return kv;
            }).toArray();*/
        }
        currentGroup = defaultGroup;
        defaultChildGroups = defaultGroup && (LINQ.fromArray(defaultGroup.childs.ids));
        this.setState({
            product, currentGroup, defaultGroup, productGroup, defaultChildGroups
        })
    }

    save = async () => {
        let { action, id } = this.props.controller.params;
        let {
            product, productGroup, currentGroup, defaultGroup, defaultChildGroups
          } = this.state;
        /*product.prices = prices;
        product.products = products;*/
        product = await product.save();
        if (action === 'add') {
            productGroup = new Model.Group();
            productGroup.name = product.name;
            productGroup.type = Model.GroupItemTypeEnum.product;
            productGroup.pointer = product;
            productGroup = await productGroup.save();
        } else {
            productGroup.name = product.name;
            productGroup = await productGroup.save();
            defaultGroup.childs.remove(productGroup);
            defaultGroup = await defaultGroup.save();
        }
        currentGroup.childs.add(productGroup);
        defaultGroup = currentGroup = await currentGroup.save();
        //defaultChildGroups = defaultGroup && (await defaultGroup.childs.find()) as LINQ<Model.Group>;
        defaultChildGroups = defaultGroup && (LINQ.fromArray(defaultGroup.childs.ids));
        this.setState({
            product, productGroup, currentGroup, defaultGroup, defaultChildGroups
        })
        return product;
    }

    // VIEW

    onChange = (product: Model.ProductTemplate) => {
        this.setState({ product });
    }

    onSave = async () => {
        let result = await this.props.controller.trySave(this.save);
        this.props.controller.params.onUpdate(this.state.currentGroup);
        this.props.controller.router.navigation.goBack();
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}