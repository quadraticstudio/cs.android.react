import * as React from "react";
import { View, Text, Card, CardItem } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props)
        this.state = {

        };
    }

    render() {
        let onChange = this.props.controller.onChange;
        let { group } = this.props.controller.state;
        return (
            <Card style={{ height: "100%" }}>
                <CardItem style={{ flexDirection: "column" }}>
                    <View style={{ width: "100%", marginBottom: 25 }}>
                        <Abstract.General.Input
                            label="Наименование*"
                            config={{
                                object: group,
                                path: "name",
                                afterSet: onChange
                            }}
                        />
                        <Abstract.Special.Select.NdsType
                            controller={this.props.controller.props.controller}
                            config={{
                                object: group,
                                path: "nds",
                                afterSet: onChange
                            }}
                        />
                        <Abstract.Special.Select.GroupType
                            controller={this.props.controller.props.controller}
                            config={{
                                object: this.props.controller.state,
                                path: "currentGroup",
                                afterSet: m => this.props.controller.setState(m)
                            }}
                            exclude={this.props.controller.props.controller.params.action == 'add' ? null : this.props.controller.state.defaultChildGroups.toArray() || []}
                        />
                        <Abstract.General.Button
                            label="Сохранить"
                            onClick={() => this.props.controller.onSave()}
                        />
                    </View>
                </CardItem>
            </Card>
        );
    }
}