import * as React from "react";
import * as Parse from "parse";
import * as Model from "../../../model"
import View from "./View"
import { LINQ } from "linq";
import QueryLINQ from "parse-query-to-linq";
import * as collection from "collection";
import { ComponentProps } from "../../../abstract/global/index";
import Query from "../../../abstract/database/lib/query";

interface Match {
    id: string,
    action: 'add' | 'edit';
    onUpdate: (currentGroup: Model.Group) => any;
}

export interface ControllerProps extends ComponentProps<Match> {

}

export interface ControllerState {
    group: Model.Group;
    defaultGroup: Model.Group;
    defaultChildGroups: LINQ<string>;
    currentGroup: Model.Group;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            group: new Model.Group(),
            defaultGroup: null,
            defaultChildGroups: LINQ.fromArray([]),
            currentGroup: null
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.load)
    }

    load = async () => {
        let { company } = this.props.global;
        let { action, id } = this.props.controller.params;
        let {
          group, defaultGroup, currentGroup, defaultChildGroups
        } = this.state;
        if (action === 'add') {
            group = group || new Model.Group();
            if (id)
                defaultGroup = await new Query(Model.Group).get(id);
            else
                defaultGroup = company.productGroup;
        } else {
            group = (await new Query(Model.Group).get(id)) as Model.Group;
            defaultGroup = group.parent && await group.parent.fetch();
        }
        currentGroup = defaultGroup;
        defaultChildGroups = defaultGroup && LINQ.fromArray(defaultGroup.childs.ids || []);
        this.setState({
            group, defaultGroup, currentGroup, defaultChildGroups
        })
    }

    save = async () => {
        let { action, id } = this.props.controller.params;
        let {
            group, defaultGroup, currentGroup, defaultChildGroups
        } = this.state;
        group.type = Model.GroupItemTypeEnum.group;
        group = await group.save();
        if (action === 'edit') {
            defaultGroup.childs.remove(group);
            defaultGroup = await defaultGroup.save();
        }
        currentGroup.childs.add(group);
        defaultGroup = currentGroup = await currentGroup.save();
        defaultChildGroups = defaultGroup && LINQ.fromArray(defaultGroup.childs.ids || []);
        this.setState({
            group, defaultGroup, currentGroup, defaultChildGroups
        })
    }

    // VIEW

    onChange = (group: Model.Group) => {
        this.setState({ group });
    }

    onSave = async () => {
        let result = await this.props.controller.trySave(this.save);
        this.props.controller.params.onUpdate(this.state.currentGroup);
        this.props.controller.router.navigation.goBack();
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}