import * as React from "react";
import View from "./View"
import Abstract from "../../abstract"

export interface ControllerProps {

}

export interface ControllerState {

}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}