import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../abstract"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {
    render() {
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <Text>123</Text>
            </View>
        );
    }
}