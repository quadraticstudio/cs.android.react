import * as React from "react";
import View from "./View"
import * as Parse from "parse";
import { LINQ } from "linq";
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import { ComponentProps } from "../../../abstract/global/index";
import * as moment from "moment";
import Query from "../../../abstract/database/lib/query";

export interface ControllerProps extends ComponentProps {

}

export interface SaleReportModel {
    date: Date;
    price: number;
    count: number;
    sum: number;
    name: string;
    unit: string;
}

export interface ControllerState {
    datas: SaleReportModel[];
    filterEndPeriod: Date;
    filterStartPeriod: Date;
    filterName: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            filterEndPeriod: undefined,
            filterStartPeriod: undefined,
            filterName: undefined
        };
    }

    loadData = async () => {
        let linq = await new Query(Model.Document).find();
        let linqPT = await new Query(Model.ProductTemplate).find();
        let linqManual = await new Query(Model.Manual).find();

        linq = linq.where(m => m.type == Model.DocumentTypeEnum.tillSlip)
            .where(m => m.documentEntity.type == "sale")
            .orderByDescending(m => m.date);

        if (this.state.filterStartPeriod && this.state.filterEndPeriod) {
            linq = linq.where(m => m.date >= this.state.filterStartPeriod && m.date <= this.state.filterEndPeriod);
        }

        let d = linq.select(m => {
            let entity = m.documentEntity as Model.Documents.ProviderInDocument;
            let elements = LINQ.fromArray(entity.elements) as LINQ<Model.Documents.ProviderInDocumentElement>;
            return elements.select(e => {
                let product = linqPT.firstOrNull(m => m.id == e.product.objectId);
                let manual = linqManual.firstOrNull(m => m.id == product.unit.id);
                return {
                    date: new Date(m.date),
                    count: e.count,
                    sum: e.sum,
                    name: product.name,
                    price: e.price,
                    unit: manual.key
                }
            }).toArray();
        }).toArray();

        let datas = LINQ.fromArray(d).selectMany(m => m);

        this.setState({
            filterEndPeriod: datas.select(m => m.date).firstOrNull(),
            filterStartPeriod: datas.select(m => m.date).lastOrNull(),
        });
        this.setState({ datas: datas.toArray() });

        // let d = await Promise.all(linq.select(async m => {
        //     let linqP = await m.products.find() as LINQ<Model.ProductOperationObject>;
        //     if (this.state.filterName) {
        //         linqP = linqP.where(m => m.product.name.indexOf(this.state.filterName) !== -1);
        //     }
        //     return linqP.orderByDescending(m => m.get('date')).select(m => {
        //         return {
        //             date: m.get('date'),
        //             count: m.count,
        //             sum: m.price * m.count,
        //             name: m.product['name'],
        //             price: m.price,
        //             unit: m.product['unit']['key']
        //         }
        //     }).toArray();
        // }).toArray());
        // let datas = LINQ.fromArray(d).selectMany(m => m).distinct();
        // this.setState({ filterStartPeriod: new Date(datas.min(m => +m.date)) });
        // this.setState({ filterEndPeriod: new Date(datas.max(m => +m.date)) });            
        // this.setState({datas: datas.toArray()});
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.loadData);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}