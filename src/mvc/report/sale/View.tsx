import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract"
import * as moment from "moment";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {
    renderInfo() {
        return (
            <View style={{ paddingLeft: 10, paddingRight: 10, paddingBottom: 10, paddingTop: 10 }}>
                <Text style={{ color: "white" }}>Период:</Text>
                <Text style={{ color: "white" }}>с {moment(this.props.controller.state.filterStartPeriod || new Date()).format('DD.MM.YYYY')} по {moment(this.props.controller.state.filterEndPeriod || new Date()).format('DD.MM.YYYY')}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                {this.renderInfo()}
                <Abstract.General.ListCollection
                    data={this.props.controller.state.datas}
                    header={[
                        {
                            title: 'Товар',
                            render: (obj) => {
                                return obj.name;
                            }
                        },
                        {
                            title: 'Дата закупки',
                            render: (obj) => {
                                return obj.date;
                            }
                        },
                        {
                            title: 'Итого',
                            render: (obj) => {
                                return `${obj.sum.toFixed(2)} руб.`;
                            }
                        },
                        {
                            title: 'Количество',
                            render: (obj) => {
                                return `${obj.count} ${obj.unit || 'шт.'}`;
                            }
                        },
                        {
                            title: 'Цена',
                            render: (obj) => {
                                return `${obj.price.toFixed(2)} руб.`;
                            }
                        }
                    ]}
                />
            </View>
        );
    }
}