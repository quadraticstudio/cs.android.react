import * as React from "react";
import * as Parse from "parse";
import View from "./View"
import * as Model from "../../../../model/index";
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import QueryLINQ from "parse-query-to-linq";
import * as moment from "moment";
import * as collection from "collection";
import { LINQ } from "linq";
import Query from "../../../../abstract/database/lib/query";

export interface ControllerParams {
    doc: Model.Document;
}

export interface ControllerProps extends ComponentProps<ControllerParams> {

}

export interface ControllerState {
    items: collection.Dictionary<string, any>
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            items: new collection.Dictionary()
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.load);
    }

    load = async () => {
        let { items } = this.state;
        let params = this.props.controller.params;
        let doc = params.doc;
        if (doc.type == Model.DocumentTypeEnum.cashIn) {
            let entity = doc.documentEntity as Model.Documents.CashInDocument;
            items.add('Описание', entity.description);
            items.add('НДС', entity.nds);
        } else if (doc.type == Model.DocumentTypeEnum.cashOut) {
            let entity = doc.documentEntity as Model.Documents.CashOutDocument;
            let expenditureType = entity.expenditureType && await new Query(Model.Manual).get(entity.expenditureType.objectId);
            items.add('Описание', entity.description);
            items.add('НДС', entity.nds);
            items.add('Статья расходов', expenditureType.keyType);
        } else if (doc.type == Model.DocumentTypeEnum.clear) {
            let entity = doc.documentEntity as Model.Documents.ClearDocument;
        } else if (doc.type == Model.DocumentTypeEnum.inventory) {
            let entity = doc.documentEntity as Model.Documents.InventoryDocument;
        } else if (doc.type == Model.DocumentTypeEnum.providerIn) {
            let entity = doc.documentEntity as Model.Documents.ProviderInDocument;
        } else if (doc.type == Model.DocumentTypeEnum.providerOut) {
            let entity = doc.documentEntity as Model.Documents.ProviderOutDocument;
        } else if (doc.type == Model.DocumentTypeEnum.purchase) {
            let entity = doc.documentEntity as Model.Documents.PurchaseDocument;
        } else if (doc.type == Model.DocumentTypeEnum.returnOfPurchase) {
            let entity = doc.documentEntity as Model.Documents.ReturnOfPurchaseDocument;
        } else if (doc.type == Model.DocumentTypeEnum.tillSlip) {
            let entity = doc.documentEntity as Model.Documents.TillSlipDocument;
        }
        this.setState({
            items
        })
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}