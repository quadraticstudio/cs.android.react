import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model/index";
import * as moment from "moment"
import * as collection from "collection";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderInfo() {
        let { items } = this.props.controller.state;
        return items.toLinq().select(m => {
            return {
                title: m.key,
                render: (obj: collection.KeyValuePair<string, any>) => {
                    return obj.value
                }
            }
        }).toArray();
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                <Abstract.General.ListCollection
                    data={this.props.controller.state.items.asArray() || []}
                    header={this.renderInfo()}
                />
            </View>
        );
    }
}