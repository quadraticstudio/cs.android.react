import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../../abstract"
import * as Model from "../../../../model/index";
import * as moment from "moment"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderInfo() {
        return (
            <View style={{ paddingLeft: 10, paddingRight: 10, paddingBottom: 10, paddingTop: 10 }}>
                <Text style={{ color: "white" }}>Период:</Text>
                <Text style={{ color: "white" }}>с {moment(new Date(this.props.controller.state.filterStartPeriod || new Date())).format('DD.MM.YYYY')} по {moment(new Date(this.props.controller.state.filterEndPeriod || new Date())).format('DD.MM.YYYY')}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                {this.renderInfo()}
                <Abstract.General.ListCollection
                    data={this.props.controller.state.items || []}
                    header={[
                        {
                            title: 'Статус',
                            render: (obj: Model.Document) => {
                                return obj.done;
                            }
                        },
                        {
                            title: 'Смена',
                            render: (obj: Model.Document) => {
                                return obj.sessionNumber;
                            }
                        },
                        {
                            title: 'Номер',
                            render: (obj: Model.Document) => {
                                return obj.number;
                            }
                        },
                        {
                            title: 'Кассовый номер',
                            render: (obj: Model.Document) => {
                                return obj.numberCashbox;
                            }
                        },
                        {
                            title: 'Фискальный номер',
                            render: (obj: Model.Document) => {
                                return obj.numberFN;
                            }
                        },
                        {
                            title: 'Вид',
                            render: (obj: Model.Document) => {
                                return obj.title;
                            }
                        },
                        {
                            title: 'Итого',
                            render: (obj: Model.Document) => {
                                return obj.sum;
                            }
                        },
                        {
                            title: 'Открыт',
                            render: (obj: Model.Document) => {
                                return obj.createdAt;
                            }
                        },
                        {
                            title: 'Закрыт',
                            render: (obj: Model.Document) => {
                                return obj.date;
                            }
                        }
                    ]}
                />
            </View>
        );
    }
}