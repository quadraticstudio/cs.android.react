import * as React from "react";
import * as Parse from "parse";
import View from "./View"
import * as Model from "../../../../model/index";
import { ComponentProps } from "../../../../abstract/global/ComponentController";
import QueryLINQ from "parse-query-to-linq";
import * as moment from "moment";
import { LINQ } from "linq";
import Query from "../../../../abstract/database/lib/query";

export interface ControllerProps extends ComponentProps {

}

export interface ControllerState {
    items: Model.Document[];
    filterStartPeriod: Date;
    filterEndPeriod: Date;
    filterType: Model.DocumentTypeEnum[];
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            filterEndPeriod: undefined,
            filterStartPeriod: undefined,
            filterType: []
        };
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.load);
    }

    load = async () => {
        let { items } = this.state;
        let linq = await new Query(Model.Document).find();
        linq = linq
            .orderByDescending(m => m.date)

        if (this.state.filterStartPeriod && this.state.filterEndPeriod) {
            linq = linq.where(m => m.date >= this.state.filterStartPeriod && m.date <= this.state.filterEndPeriod);
        }

        if (this.state.filterType.length) {
            linq = linq.where(m => LINQ.fromArray(this.state.filterType).contains(m.type));
        }

        items = linq.toArray();

        this.setState({
            filterEndPeriod: linq.select(m => m.date).firstOrNull(),
            filterStartPeriod: linq.select(m => m.date).lastOrNull(),
        });
        this.setState({
            items
        })
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}