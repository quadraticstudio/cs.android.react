import * as React from "react";
import { View, Text } from 'native-base';
import Controller, { MoveReportModel } from "./Controller"
import Abstract from "../../../abstract"
import { LINQ } from "linq";
import * as moment from "moment"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderInfo () {
        let datas = LINQ.fromArray(this.props.controller.state.datas);
        let startResidue = datas.sum(m => m.startResidue);
        let income = datas.sum(m => m.income);
        let outcome = datas.sum(m => m.outcome);
        let endResidue = datas.sum(m => m.endResidue);
        return (
            <View style={{paddingLeft: 10, paddingRight: 10, paddingBottom: 10, paddingTop: 10}}>
                <Text style={{color: "white"}}>Период:</Text>
                <Text style={{color: "white"}}>с {moment(this.props.controller.state.filterStartPeriod || new Date()).format('DD.MM.YYYY')} по {moment(this.props.controller.state.filterEndPeriod || new Date()).format('DD.MM.YYYY')}</Text>
                <Text style={{color: "white"}}>Остаток на начало: {startResidue}</Text>
                <Text style={{color: "white"}}>Приход: {income}</Text>
                <Text style={{color: "white"}}>Расход: {outcome}</Text>
                <Text style={{color: "white"}}>Остаток на конец: {endResidue}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                {this.renderInfo()}
                <Abstract.General.ListCollection
                    data={this.props.controller.state.datas}
                    header={[
                        {
                            title: 'Товар',
                            render: (obj:MoveReportModel) => {
                                return obj.name;
                            }
                        },
                        {
                            title: 'Остаток на начало',
                            render: (obj:MoveReportModel) => {
                                return obj.startResidue;
                            }
                        },
                        {
                            title: 'Приход',
                            render: (obj:MoveReportModel) => {
                                return obj.income;
                            }
                        },
                        {
                            title: 'Расход',
                            render: (obj:MoveReportModel) => {
                                return obj.outcome;
                            }
                        },
                        {
                            title: 'Остаток на конец',
                            render: (obj:MoveReportModel) => {
                                return obj.endResidue;
                            }
                        }
                    ]}
                />
            </View>
        );
    }
}