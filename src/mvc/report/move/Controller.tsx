import * as React from "react";
import View from "./View"
import { ComponentProps } from "../../../abstract/global/index";
import * as Parse from "parse";
import { LINQ } from "linq";
import QueryLINQ from "parse-query-to-linq";
import * as Model from "../../../model";
import Query from "../../../abstract/database/lib/query";
import { Documents } from "../../../model";

export interface ControllerProps extends ComponentProps {

}

export interface MoveReportModel {
    name: string;
    startResidue: number;
    income: number;
    outcome: number;
    endResidue: number;
}

export interface ControllerState {
    datas: MoveReportModel[];
    filterEndPeriod: Date;
    filterStartPeriod: Date;
    filterName: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            filterEndPeriod: undefined,
            filterStartPeriod: undefined,
            filterName: undefined
        };
    }

    loadData = async (filterStartPeriod?: Date, filterEndPeriod?: Date) => {

        let linqDoc = await new Query(Model.Document).find();
        let linqStore = await new Query(Model.ProductStore).find();
        let linqProduct = await new Query(Model.ProductTemplate).find();

        if (filterStartPeriod) {
            linqDoc = linqDoc.where(m => m.date >= filterStartPeriod);
        }

        if (filterEndPeriod) {
            linqDoc = linqDoc.where(m => m.date <= filterEndPeriod);
        }

        linqDoc = linqDoc.orderByDescending(m => m.date);

        let linqDocIn = linqDoc.where(m => m.type == Model.DocumentTypeEnum.providerIn || m.type == Model.DocumentTypeEnum.return || m.type == Model.DocumentTypeEnum.purchase || (m.type == Model.DocumentTypeEnum.tillSlip && m.documentEntity.type == 'return'));
        let linqDocOut = linqDoc.where(m => m.type == Model.DocumentTypeEnum.clear || m.type == Model.DocumentTypeEnum.providerOut || m.type == Model.DocumentTypeEnum.returnOfPurchase || m.type == Model.DocumentTypeEnum.disposal || (m.type == Model.DocumentTypeEnum.tillSlip && m.documentEntity.type == 'sale'))

        let income = linqDocIn.selectMany(m => {
            let entity = m.documentEntity as Model.Documents.ProviderInDocument | Model.Documents.TillSlipDocument | Model.Documents.PurchaseDocument;
            let els: { product: Model.ProductTemplate, count: number, date: Date, type: string }[] = [];
            for (let el of entity.elements || []) {
                let product = linqProduct.firstOrNull(m => m.id == el.product.objectId);
                if (this.state.filterName) {
                    if (product.name.indexOf(this.state.filterName) == -1)
                        continue;
                }
                els.push({
                    product,
                    count: el.count,
                    date: new Date(m.date),
                    type: 'income'
                });
            }
            return els;
        })

        let outcome = linqDocOut.selectMany(m => {
            let entity = m.documentEntity as Model.Documents.ProviderOutDocument | Model.Documents.TillSlipDocument | Model.Documents.ClearDocument | Model.Documents.ReturnOfPurchaseDocument;
            let els: { product: Model.ProductTemplate, count: number, date: Date, type: string }[] = [];
            for (let el of entity.elements || []) {
                let product = linqProduct.firstOrNull(m => m.id == el.product.objectId);
                if (this.state.filterName) {
                    if (product.name.indexOf(this.state.filterName) == -1)
                        continue;
                }
                els.push({
                    product,
                    count: el.count,
                    date: new Date(m.date),
                    type: 'outcome'
                });
            }
            return els;
        })

        let datas = income.concat(outcome).groupBy(m => m.product).toLinq().select(m => {
            let els = LINQ.fromArray(m.value);
            let startResidue = 0;
            let income = els.where(m => m.type == 'income').sum(m => m.count);
            let outcome = els.where(m => m.type == 'outcome').sum(m => m.count);
            let endResidue = startResidue + income - outcome;
            return {
                name: m.key.name,
                income,
                outcome,
                startResidue,
                endResidue
            }
        })

        /*linqDoc = linqDoc.where(m => m.type == Model.DocumentTypeEnum.providerIn || m.type == Model.DocumentTypeEnum.providerOut)
            .orderByDescending(m => m.date);*/

        /*let d = await Promise.all(linqDoc.select(async m => {
            let entity = m.documentEntity as Documents.ProviderInDocument;
            let products = LINQ.fromArray(m.documentEntity.elements) as LINQ<Documents.ProviderInDocumentElement>;

            return await Promise.all(products.select(async p => {
                let product = linqProduct.firstOrNull(m => m.id == p.product.objectId);
                if (this.state.filterName) {
                    if (product.name.indexOf(this.state.filterName) == -1) return null;
                }
                return {
                    endResidue: await product.getAllCount(),
                    outcome: await product.getProviderPrice(),
                    name: product.name,
                    income: p.sum,
                    startResidue: 0
                }
            }).notNull().toArray());
        }).toArray());*/

        //let datas = LINQ.fromArray(d).selectMany(m => m);
        this.setState({
            filterEndPeriod: linqDoc.select(m => m.date).firstOrNull(),
            filterStartPeriod: linqDoc.select(m => m.date).lastOrNull(),
        });
        this.setState({ datas: datas.toArray() });
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.loadData);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}