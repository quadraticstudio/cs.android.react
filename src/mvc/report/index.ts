import Consolidate from "./consolidate/Controller"
import Income from "./income/Controller"
import Move from "./move/Controller"
import ProviderIn from "./providerIn/Controller"
import Residue from "./residue/Controller"
import Sale from "./sale/Controller"
import Operations from "./operation/Controller";
import Documents from "./documents";

export default {
    Consolidate, Income, Move, ProviderIn, Residue, Sale, Operations, Documents
}
