import * as React from "react";
import View from "./View"
import * as Parse from "parse";
import { LINQ } from "linq";
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import { ComponentProps } from "../../../abstract/global/index";
import * as moment from "moment";
import Query from "../../../abstract/database/lib/query";

export interface ControllerProps extends ComponentProps {

}

export interface ProviderInReportModel {
    date: Date;
    price: number;
    count: number;
    sum: number;
    name: string;
    unit: string;
}

export interface ControllerState {
    datas: ProviderInReportModel[];
    filterStartPeriod: Date;
    filterEndPeriod: Date;
    filterName: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            filterEndPeriod: undefined,
            filterStartPeriod: undefined,
            filterName: undefined
        };
    }

    loadData = async () => {
        let linq = await new Query(Model.Document).find();
        let linqPT = await new Query(Model.ProductTemplate).find();
        let linqManual = await new Query(Model.Manual).find();

        linq = linq.where(m => m.type == Model.DocumentTypeEnum.providerIn)
            .orderByDescending(m => m.createdAt);

        if (this.state.filterStartPeriod && this.state.filterEndPeriod) {
            linq = linq.where(m => m.createdAt >= this.state.filterStartPeriod && m.createdAt <= this.state.filterEndPeriod);
        }

        let d = linq.select(m => {
            let entity = m.documentEntity as Model.Documents.ProviderInDocument;
            let elements = LINQ.fromArray(entity.elements) as LINQ<Model.Documents.ProviderInDocumentElement>;
            return elements.select(e => {
                let product = linqPT.firstOrNull(m => m.id == e.product.objectId);
                let manual = linqManual.firstOrNull(m => m.id == product.unit.id);
                return {
                    date: new Date(m.createdAt),
                    count: e.count,
                    sum: e.sum,
                    name: product.name,
                    price: e.priceProviderIn,
                    unit: manual.key
                }
            }).toArray();
        }).toArray();

        let datas = LINQ.fromArray(d).selectMany(m => m);

        this.setState({
            filterEndPeriod: datas.select(m => m.date).firstOrNull(),
            filterStartPeriod: datas.select(m => m.date).lastOrNull(),
        });
        this.setState({ datas: datas.toArray() });
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.loadData);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}