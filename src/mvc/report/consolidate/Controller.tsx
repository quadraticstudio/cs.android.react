import * as React from "react";
import View from "./View"
import * as Parse from "parse";
import { LINQ } from "linq";
import QueryLINQ from "parse-query-to-linq"
import * as Model from "../../../model"
import { ComponentProps } from "../../../abstract/global/index";
import * as moment from "moment";
import Query from "../../../abstract/database/lib/query";
import { TillSlipDocument, TillSlipDocumentElement } from "../../../model/Documents/index";
import { Documents } from "../../../model";

export interface ControllerProps extends ComponentProps {

}

export interface ConsolidateReportModel {
    date: Date;
    cash: number;
    card: number;
    sum: number;
}

export interface ControllerState {
    datas: ConsolidateReportModel[];
    filterStartPeriod: Date;
    filterEndPeriod: Date;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            filterEndPeriod: undefined,
            filterStartPeriod: undefined
        };
    }

    loadData = async () => {
        let linq = await new Query(Model.Document).find();
        let linqPT = await new Query(Model.ProductTemplate).find();
        let linqManual = await new Query(Model.Manual).find();

        linq = linq.where(m => m.type == Model.DocumentTypeEnum.tillSlip)
            .where(m => m.documentEntity.type == "sale")
            .orderByDescending(m => m.createdAt);

        if (this.state.filterStartPeriod && this.state.filterEndPeriod) {
            linq = linq.where(m => m.createdAt >= this.state.filterStartPeriod && m.createdAt <= this.state.filterEndPeriod)
        }

        let d: LINQ<ConsolidateReportModel> = linq.select(m => {
            let entity = m.documentEntity;
            return {
                date: new Date(m.createdAt),
                card: entity.sumCard,
                cash: entity.sumCash,
                sum: entity.sumCard + entity.sumCash
            }
        });

        let datas = d.distinct();
        this.setState({
            filterEndPeriod: datas.select(m => m.date).firstOrNull(),
            filterStartPeriod: datas.select(m => m.date).lastOrNull(),
        });
        this.setState({ datas: datas.toArray() });
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.loadData);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}