import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract"
import { LINQ } from "linq";
import * as moment from "moment"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderInfo() {
        let cashSum = LINQ.fromArray(this.props.controller.state.datas).sum(m => m.cash);
        let cardSum = 0;
        let sum = cashSum + cardSum;
        return (
            <View style={{ paddingLeft: 10, paddingRight: 10, paddingBottom: 10, paddingTop: 10 }}>
                <Text style={{ color: "white" }}>Период:</Text>
                <Text style={{ color: "white" }}>с {moment(this.props.controller.state.filterStartPeriod || new Date()).format('DD.MM.YYYY')} по {moment(this.props.controller.state.filterEndPeriod || new Date()).format('DD.MM.YYYY')}</Text>
                <Text style={{ color: "white" }}>Наличными: {`${cashSum.toFixed(2)} руб.`}</Text>
                <Text style={{ color: "white" }}>Кредитной картой: {`${cardSum.toFixed(2)} руб.`}</Text>
                <Text style={{ color: "white" }}>Итого: {`${sum.toFixed(2)} руб.`}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                {this.renderInfo()}
                <Abstract.General.ListCollection
                    data={this.props.controller.state.datas}
                    header={[
                        {
                            title: 'Дата продажи',
                            render: (obj) => {
                                return obj.date;
                            }
                        },
                        {
                            title: 'Наличными',
                            render: (obj) => {
                                return `${obj.cash.toFixed(2)} руб.`;
                            }
                        },
                        {
                            title: 'Пластиковой картой',
                            render: (obj) => {
                                return `${obj.card.toFixed(2)} руб.`;
                            }
                        },
                        {
                            title: 'Итого',
                            render: (obj) => {
                                return `${obj.sum.toFixed(2)} руб.`;
                            }
                        }
                    ]}
                />
            </View>
        );
    }
}