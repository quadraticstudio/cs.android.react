import * as React from "react";
import * as Parse from "parse";
import View from "./View"
import * as Model from "../../../model/index";
import { ComponentProps } from "../../../abstract/global/ComponentController";
import QueryLINQ from "parse-query-to-linq";
import * as moment from "moment";
import { LINQ } from "linq";
import Query from "../../../abstract/database/lib/query";

export interface ControllerProps extends ComponentProps {

}

export interface IncomeReportModel {
    date: Date;
    sum: number;
    name: string;
    count: number;
    receipts: number;
    purchase: number;
    profit: number;
    unit: string;
}

export interface ControllerState {
    datas: IncomeReportModel[];
    filterStartPeriod: Date;
    filterEndPeriod: Date;
    filterName: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            filterEndPeriod: undefined,
            filterStartPeriod: undefined,
            filterName: undefined
        };
    }

    loadData = async () => {
        let linq = await new Query(Model.Document).find() as LINQ<Model.Document<Model.Documents.TillSlipDocument>>;
        let linqManual = await new Query(Model.Manual).find();
        let linqPT = await new Query(Model.ProductTemplate).find();

        linq = linq.where(m => m.type == Model.DocumentTypeEnum.tillSlip)
            .where(m => m.documentEntity.type == "sale")
            .orderByDescending(m => m.date);

        if (this.state.filterStartPeriod && this.state.filterEndPeriod) {
            linq = linq.where(m => m.date >= this.state.filterStartPeriod && m.date <= this.state.filterEndPeriod);
        }

        let d = await Promise.all(linq.select(async m => {
            let entity = m.documentEntity;
            let elements = LINQ.fromArray(m.documentEntity.elements);

            return await Promise.all(elements.select(async e => {
                let product = linqPT.firstOrNull(m => m.id == e.product.objectId);
                let unit = linqManual.firstOrNull(m => m.id == product.unit.id);
                return {
                    date: m.date,
                    dateFormatted: moment(m.date).format('DD.MM.YYYY'),
                    count: e.count,
                    name: product.name,
                    purchase: await product.getPurchasePrice(),
                    provider: await product.getProviderPrice(),
                    unit: unit.key
                }
            }).toArray());
        }).toArray());

        // let d = await Promise.all(linq.select(async m => {
        //     let p = await m.products.find() as LINQ<Model.ProductOperationObject>;

        //     p.orderByDescending(m => m.get('createdAt'));

        //     if (this.state.filterName) {
        //         p = p.where(p => p.product.name.indexOf(this.state.filterName) !== -1);
        //     }

        //     return await Promise.all(p.select(async p => {
        //         return {
        //             date: p.get('createdAt'),
        //             dateFormatted: moment(p.get('createdAt')).format('DD.MM.YYYY'),
        //             count: p.count,
        //             name: p.product.name,
        //             purchase: await p.product.getPurchasePrice(),
        //             provider: await p.product.getProviderPrice(),
        //             unit: linqManual.firstOrNull(m => m.id == p.product.unit.id).key
        //         }
        //     }).toArray());
        // }).toArray());

        let g = LINQ.fromArray(d).selectMany(m => m).groupBy(m => m.dateFormatted).toLinq();
        let datas = g.select(m => {
            let v = LINQ.fromArray(m.value);
            let count = v.sum(m => m.count);
            let receipt = count * v.first().provider;
            let purchase = count * v.first().purchase;
            return {
                date: new Date(v.max(m => +m.date)),
                name: v.first().name,
                count: count,
                receipts: receipt,
                purchase: purchase,
                profit: purchase - receipt,
                sum: purchase - receipt,
                unit: v.first().unit
            }
        });
        this.setState({
            filterEndPeriod: datas.select(m => m.date).firstOrNull(),
            filterStartPeriod: datas.select(m => m.date).lastOrNull(),
        });
        this.setState({ datas: datas.toArray() });
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.loadData);
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}