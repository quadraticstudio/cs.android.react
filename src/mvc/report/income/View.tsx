import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract"
import * as moment from "moment"
import { LINQ } from "linq";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderInfo() {
        let datas = LINQ.fromArray(this.props.controller.state.datas);
        let purchase = datas.sum(m => m.purchase);
        let profit = datas.sum(m => m.profit);
        let receipts = datas.sum(m => m.receipts);
        return (
            <View style={{ paddingLeft: 10, paddingRight: 10, paddingBottom: 10, paddingTop: 10 }}>
                <Text style={{ color: "white" }}>Период:</Text>
                <Text style={{ color: "white" }}>с {moment(this.props.controller.state.filterStartPeriod || new Date()).format('DD.MM.YYYY')} по {moment(this.props.controller.state.filterEndPeriod || new Date()).format('DD.MM.YYYY')}</Text>
                <Text style={{ color: "white" }}>Выручка: {`${purchase.toFixed(2)} руб.`}</Text>
                <Text style={{ color: "white" }}>Закупка: {`${receipts.toFixed(2)} руб.`}</Text>
                <Text style={{ color: "white" }}>Прибыль: {`${profit.toFixed(2)} руб.`}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                {this.renderInfo()}
                <Abstract.General.ListCollection
                    data={this.props.controller.state.datas}
                    header={[
                        {
                            title: 'Дата',
                            render: (obj) => {
                                return obj.date;
                            }
                        },
                        {
                            title: 'Итого',
                            render: (obj) => {
                                return `${obj.sum.toFixed(2)} руб.`;
                            }
                        },
                        {
                            title: 'Товар',
                            render: (obj) => {
                                return obj.name;
                            }
                        },
                        {
                            title: 'Количество',
                            render: (obj) => {
                                return `${obj.count} ${obj.unit || "шт."}`;
                            }
                        },
                        {
                            title: 'Выручка',
                            render: (obj) => {
                                return `${obj.purchase.toFixed(2)} руб.`;
                            }
                        },
                        {
                            title: 'Закупка',
                            render: (obj) => {
                                return `${obj.receipts.toFixed(2)} руб.`;
                            }
                        },
                        {
                            title: 'Прибыль',
                            render: (obj) => {
                                return `${obj.profit.toFixed(2)} руб.`;
                            }
                        }
                    ]}
                />
            </View>
        );
    }
}