import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract"
import * as Model from "../../../model/index";
import * as moment from "moment"

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderInfo() {
        return (
            <View style={{ paddingLeft: 10, paddingRight: 10, paddingBottom: 10, paddingTop: 10 }}>
                <Text style={{ color: "white" }}>Период:</Text>
                <Text style={{ color: "white" }}>с {moment(new Date(this.props.controller.state.filterStartPeriod || new Date())).format('DD.MM.YYYY')} по {moment(new Date(this.props.controller.state.filterEndPeriod || new Date())).format('DD.MM.YYYY')}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                {this.renderInfo()}
                <Abstract.General.ListCollection
                    data={this.props.controller.state.items || []}
                    header={[
                        {
                            title: 'Сообщение',
                            render: (obj: Model.Document) => {
                                return `Документ "${obj.title}" | ${obj.number || '-'}`;
                            }
                        },
                        {
                            title: 'Тип операции',
                            render: (obj: Model.Document) => {
                                return obj.title;
                            }
                        },
                        {
                            title: 'Пользователь',
                            render: (obj: Model.Document) => {
                                return (obj.user && obj.user.get && obj.user.get('name')) || (obj.user && obj.user.attributes && obj.user.attributes.name) || (obj.user && obj.user['name']) || '-';
                            }
                        },
                        {
                            title: 'Дата',
                            render: (obj: Model.Document) => {
                                return obj.date;
                            }
                        }
                    ]}
                />
            </View>
        );
    }
}