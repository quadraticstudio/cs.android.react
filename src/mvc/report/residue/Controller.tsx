import * as React from "react";
import View from "./View"
import * as Model from "../../../model"
import * as Parse from "parse";
import { ComponentProps } from "../../../abstract/global/index";
import QueryLINQ from "parse-query-to-linq";
import Query from "../../../abstract/database/lib/query";
import { LINQ } from "linq";

export interface ControllerProps extends ComponentProps {

}

export interface ResidueReportModel {
    name: string;
    count: number;
    price: number;
    unit: string;
}

export interface ControllerState {
    datas: ResidueReportModel[];
    filterStartPeriod: Date;
    filterEndPeriod: Date;
    filterName: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            filterEndPeriod: undefined,
            filterStartPeriod: undefined,
            filterName: undefined
        };
    }

    loadData = async () => {

        let queryStore = new Query(Model.ProductStore);
        let queryProduct = new Query(Model.ProductTemplate);
        let queryManual = new Query(Model.Manual);
        let linqStore = await queryStore.find();
        let linqProduct = await queryProduct.find();
        let linqManual = await queryManual.find();

        if (this.state.filterStartPeriod && this.state.filterEndPeriod) {
            linqStore = linqStore.where(m => m.get('createdAt') >= this.state.filterStartPeriod && m.get('createdAt') <= this.state.filterEndPeriod);
        }
        let datas: ResidueReportModel[] = await Promise.all(linqStore.select(async m => {
            let product = linqProduct.firstOrNull(t => t.id == m.product.id);
            if (!product)
                return null;
            let unit = product.unit.key;
            if (this.state.filterName) {
                if (product.name.indexOf(this.state.filterName) !== -1) return null;
            }
            return {
                name: m.product.name,
                count: await m.product.getAllCount(this.props.global.cashbox.shop.shopstore),
                price: await m.product.getPurchasePrice(),
                unit: unit
            }
        }).toArray());
        this.setState({ datas: LINQ.fromArray(datas).notNull().toArray() });
    }

    componentDidMount() {
        this.props.controller.tryLoad(this.loadData)
    }

    render() {
        return (
            <View
                controller={this}
            />
        );
    }
}