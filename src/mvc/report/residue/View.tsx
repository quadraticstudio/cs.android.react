import * as React from "react";
import { View, Text } from 'native-base';
import Controller from "./Controller"
import Abstract from "../../../abstract"
import { LINQ } from "linq";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    renderInfo () {
        let sum = LINQ.fromArray(this.props.controller.state.datas).sum(m => m.price * m.count);
        let residue = LINQ.fromArray(this.props.controller.state.datas).sum(m => m.count);
        return (
            <View style={{ paddingBottom: 12, paddingLeft: 12, paddingTop: 12, backgroundColor: "#070d19" }}>
                <Text style={{color: "white"}}>В ценах реализации</Text>
                <Text style={{color: "white"}}>Остаток: {residue.toFixed(2)}</Text>
                <Text style={{color: "white"}}>На сумму: {sum.toFixed(2)} руб.</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: "#070d19" }}>
                {this.renderInfo()}
                <Abstract.General.ListCollection
                    data={this.props.controller.state.datas}
                    header={[
                        {
                            title: 'Товар',
                            render: (obj) => {
                                return obj.name;
                            }
                        },
                        {
                            title: 'Цена',
                            render: (obj) => {
                                return `${obj.price.toFixed(2)} руб.`;
                            }
                        },
                        {
                            title: 'Количество',
                            render: (obj) => {
                                return `${obj.count} ${obj.unit || 'шт.'}`;
                            }
                        },
                        {
                            title: 'Сумма',
                            render: (obj) => {
                                return `${obj.count * obj.price.toFixed(2)} руб.`;
                            }
                        }
                    ]}
                />
            </View>
        );
    }
}