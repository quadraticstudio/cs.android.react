import * as React from "react";
import View from "./View"
import { ComponentProps } from "../../../abstract/global/index";
import { ToastAndroid, Alert } from "react-native";
import * as Model from "../../../model";
import Abstract from "../../../abstract";
import AuthForm from "../form/Controller"

import * as Parse from "parse";
import * as DbBerish from "db-berish";
import { LINQ } from "linq";
import LocalAdapter from "../../../abstract/database/lib/adapter";
import { setInterval } from "timers";
import { Constants, Util } from "expo";
import { toLocal } from "../../../abstract/database/lib/converter";

export interface ControllerProps extends ComponentProps {

}

export interface ControllerState {
    items: AuthListItem[];
    showForm: boolean;
}

export interface AuthListItem {
    name: string;
    role: string;
    email: string,
    objectId: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
            showForm: false
        };
    }

    async componentWillMount() {
        this.props.controller.tryLoad(this.load);
    }

    async getCashbox() {
        let json = await Parse.Cloud.run('cashbox:getCashbox', { serialNumber: Constants.deviceId });
        if (json.cashboxJSON) {
            json.cashboxJSON.className = 'Cashbox';
            let cashbox = Parse.Object.fromJSON(json.cashboxJSON, true) as Parse.Object;
            let localCashbox = await toLocal(cashbox, this.props.global.cashbox) as Model.Cashbox;
            localCashbox.shop = localCashbox.shop && await localCashbox.shop.saveToLocal();
            localCashbox.company = localCashbox.company && await localCashbox.company.saveToLocal();
            localCashbox = await localCashbox.saveToLocal();
            let cloudUsers = await Parse.Cloud.run('user:getSubUsers', { objectId: localCashbox.user.id });
            await this.props.controller.storage.update(m => {
                m.global.cashbox = localCashbox;
                m.global.users = cloudUsers && cloudUsers.users
            })
            return this.load();
        }
        this.setState({
            showForm: true
        })
    }

    load = async () => {
        let { cashbox, users } = this.props.global;
        // УБРАТЬ ПОТОМ
        /*this.props.controller.router.navigation.navigate("AuthForm");
        return;*/
        // УБРАТЬ ПОТОМ
        if (!cashbox) {
            let condition = LocalAdapter.instance.condition;
            if (!condition) {
                Alert.alert('Нет интернет соединения', 'При первом подключении необходимо интернет соединение',
                    [
                        {
                            text: 'Повторить',
                            onPress: () => this.props.controller.tryLoad(this.load)
                        }
                    ],
                    {
                        onDismiss: () => this.props.controller.tryLoad(this.load)
                    })
            } else
                return this.getCashbox();
            // Устройство не зарегистрировано и это первая авторизация
            //this.props.controller.router.navigation.navigate("AuthForm");
            return;
        } else {
            // Устройство зарегистрировано и уже есть список пользователей
            if (!(users && users.length > 0 && users[0].objectId != null)) {
                // Произошла непонтная причина, когда устройство зарегистрировано а пользоателей нет
                await this.props.controller.storage.update(m => {
                    m.global.users = [];
                    m.global.cashbox = null;
                    m.global.firstDownloadEnded = false;
                    m.global.company = null;
                    m.global.shop = null;
                    m.global.shopstore = null;
                });
                this.props.controller.router.navigation.navigate("AuthForm");
                throw new Error('Устройство зарегистрировано некорректно')
            }
            let condition = LocalAdapter.instance.condition;
            let anyUser = LINQ.fromArray(users || []).firstOrNull();
            if (condition && (anyUser && anyUser.objectId)) {
                let cloudUsers = await Parse.Cloud.run('user:getSubUsers', { objectId: anyUser.objectId });
                users = cloudUsers && cloudUsers.users;
            }
            let items = LINQ.fromArray(users).select(m => {
                return {
                    name: m.name,
                    role: m.roleName,
                    email: m.email,
                    objectId: m.objectId
                }
            }).toArray();
            await this.props.controller.storage.update(m => {
                m.global.users = users
            })
            this.setState({ items })
        }
    }

    onClick = async (index: number, data: AuthListItem) => {
        this.props.controller.router.navigation.navigate('AuthForm', { user: data })
    }

    render() {
        if (this.state.showForm)
            return <AuthForm {...this.props} />
        return (
            <View
                controller={this}
            />
        );
    }
}