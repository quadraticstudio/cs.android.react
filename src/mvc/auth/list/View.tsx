import * as React from "react";
import { Text, PixelRatio, StyleSheet, TouchableOpacity, ToastAndroid } from 'react-native';
import { Card, CardItem, Form, List, ListItem, StyleProvider, View } from "native-base";
import Abstract from "../../../abstract";
import Controller, { AuthListItem } from "./Controller"
import { Constants } from "expo";

const DebugStyles = Abstract.Debug.Styles.createStyle;

export interface ViewProps {
    controller: Controller;
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props);
        this.state = {};
    }

    renderRow = (index: number, data: AuthListItem) => {
        return (
            <View
                style={{
                    flexDirection: "column"
                }}>
                <Text style={{ color: "white", fontFamily: "Roboto", fontWeight: "bold", fontSize: 32 }}>{data.name}</Text>
                <Text style={{ fontSize: 18, color: "white" }}>{data.role}</Text>
            </View>
        );
    }

    render() {
        let items = this.props.controller.state.items;
        return (
            <View style={{ flex: 1, width: "100%" }}>
                <Abstract.General.List
                    onClick={this.props.controller.onClick}
                    renderRow={this.renderRow}
                    items={items}
                />
                <View style={styles.metaView}>
                    <Text style={styles.metaText}>Версия: {Constants.manifest.version}</Text>
                    <Text style={styles.metaText}>Идентификатор: {Constants.deviceId}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    metaView: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#070d19"
    },
    metaText: {
        fontSize: 10,
        color: '#00a5e7'
    }
});