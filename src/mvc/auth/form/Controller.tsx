import * as React from "react";
import View from "./View"
import * as Parse from "parse";
import * as Model from "../../../model"
import { ComponentProps } from "../../../abstract/global/index";
import Abstract from "../../../abstract"
import { generateId } from "db-berish/lib/util";
import { AuthListItem } from "../list/Controller";
import { auth } from "../../../abstract/database/lib/auth";
import LocalAdapter from "../../../abstract/database/lib/adapter";
import { notification, openSession } from "../../../abstract/global/Messages";
import { Alert } from "react-native";
import { Config } from "../../../model";
import { Constants, Util } from "expo";
import { LINQ } from "linq";
import DbAdapter from "db-berish/lib/adapter";
import Query from "../../../abstract/database/lib/query";

export interface ControllerProps extends ComponentProps {

}

export interface ControllerState {
    user: AuthListItem;
    login: string;
    password: string;
    title: string;
}

export default class ControllerComponent extends React.Component<ControllerProps, ControllerState> {

    constructor(props) {
        super(props);
        let params = this.props.controller.router.navigation.state['params'];
        this.state = {
            login: null,
            password: null,
            user: params && params.user,
            title: null
        };
    }

    async componentWillMount() {
        // УБРАТЬ ПОТОМ
        //this.props.controller.router.navigation.navigate('Main');
    }

    async checkCashbox() {
        if (!LocalAdapter.instance.condition)
            return true;
        try {
            let cashbox = await new Parse.Query("Cashbox").get(this.props.global.cashbox.id);
            return true;
        } catch (err) {
            return false;
        }
    }

    auth = async () => {
        let { login, password, user } = this.state;
        let localUser = await auth(user ? user.email : login.toLowerCase(), password);
        //let parseUser = await Parse.User.logIn(user ? user.email : login.toLowerCase(), password);
        if (!localUser)
            throw new Error('user logIn failed');
        if (this.props.global.cashbox && user) {
            // Устройство уже зарегано и была хоть раз авторизация
            let cashboxNormal = await this.checkCashbox();
            if (!cashboxNormal) {
                await this.props.controller.storage.update(m => {
                    m.global.users = [];
                    m.global.cashbox = null;
                    m.global.firstDownloadEnded = false;
                    m.global.company = null;
                    m.global.shop = null;
                    m.global.shopstore = null;
                });
                let ctors = LocalAdapter.ctors;
                let keys = Object.keys(ctors || {}) || [];
                let linq = LINQ.fromArray(keys);
                for (let m of linq.toArray()) {
                    let items = await new Query(m).find();
                    for (let item of items.toArray()) {
                        await item.destroyLocal();
                    }
                }
                await this.props.controller.testAlert('Устройство не привязано', 'Для работы необходимо заново привязать устройство, так как оно было отвязано из бэк-офса');
                Util.reload();
                throw new Error('Устройство не привязано')
            }
            await this.props.controller.storage.update(async m => {
                m.global = await Config.loadAuth();
            })
        } else {
            if (!LocalAdapter.instance.condition)
                throw new Error('Авторизация впервые на устройстве, требуется интернет соединение')
            // Происходит авторизация впервые на устройстве
            let cashbox = new Model.Cashbox();
            cashbox.serialNumber = Constants.deviceId;
            cashbox = await cashbox.saveToParse();
            this.props.global.cashbox = cashbox;
            let cloudUsers = await Parse.Cloud.run('user:getSubUsers', { objectId: localUser.id });
            this.props.global.users = cloudUsers && cloudUsers.users;
            await this.props.controller.storage.update();
        }
    }

    // VIEW

    onChange = (state: ControllerState) => {
        this.setState(state);
    }

    onAuth = async () => {
        await this.props.controller.tryLoad(async () => {
            await this.props.controller.tryLoad(this.auth, { allowException: true });
            await this.props.controller.tryLoad(() => openSession(title => this.setState({ title }), this.props.controller), { useLoading: false })
            this.props.controller.router.navigation.navigate('Main');
        }, { useLoading: false });
    }

    render() {
        return (
            <Abstract.General.Spin spinning={!!this.state.title} title={this.state.title}>
                <View
                    controller={this}
                />
            </Abstract.General.Spin>
        );
    }
}