import * as React from "react";
import { View, Text, ToastAndroid, StyleSheet, FlatList, SectionList, TouchableHighlight, Modal } from 'react-native';
import { Card, CardItem, Form, Button } from "native-base";
import Abstract from "../../../abstract";
import Controller from "./Controller"
import { Constants } from "expo";

export interface ViewProps {
    controller: Controller
}

export default class ViewComponent extends React.Component<ViewProps, any> {

    constructor(props) {
        super(props);
    }

    renderRow = (rowData, sectionId, rowId) => {
        return <Text>{rowData}</Text>
    }

    render() {
        let controller = this.props.controller;
        let state = controller.state;
        return (
            <View style={{ flex: 1, justifyContent: "flex-start", backgroundColor: "#070d19", alignItems: "center" }}>
                {/* <Abstract.General.Button
                    onClick={() => controller.props.controller.router.navigation.navigate('LocalDB')}
                    label="Локальная БД"
                /> */}
                <View style={{ height: 300, width: "95%" }}>
                    <Card style={{ marginTop: 20, marginBottom: 20 }}>
                        <CardItem style={{ flexDirection: "column" }}>
                            <View style={{ width: "100%", marginBottom: 25, paddingTop: 10 }}>
                                <Abstract.General.Input
                                    label={this.props.controller.state.user ? "Имя пользователя" : "Логин"}
                                    config={{
                                        object: state,
                                        path: "login",
                                        afterGet: (m, p, v) => {
                                            if (this.props.controller.state.user)
                                                return this.props.controller.state.user.name;
                                            return v;
                                        },
                                        afterSet: controller.onChange
                                    }}
                                    disabled={this.props.controller.state.user != null}
                                />
                                <Abstract.General.Input
                                    label="Пароль"
                                    secureTextEntry
                                    config={{
                                        object: state,
                                        path: "password",
                                        afterSet: controller.onChange
                                    }}
                                />
                            </View>
                            <Abstract.General.Button
                                onClick={() => controller.onAuth()}
                                label="Войти"
                            />
                        </CardItem>
                    </Card>
                </View>
                <View style={styles.metaView}>
                    <Text style={styles.metaText}>Версия: {Constants.manifest.version}</Text>
                    <Text style={styles.metaText}>Идентификатор: {Constants.deviceId}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    metaView: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#070d19"
    },
    metaText: {
        fontSize: 10,
        color: '#00a5e7'
    }
});