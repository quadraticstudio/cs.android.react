import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps, closeSession } from "../../../abstract/global/index";

export default class ReportCashbox extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                countAtRow={1}
                inverted
                items={[
                    { title: "X-отчет (без гашения)", onPress: () => this.props.controller.router.navigation.navigate('Cashbox') },
                    {
                        title: "Z-отчет (с гашением)", onPress: () => {
                            this.props.controller.tryLoad(closeSession, null, true)
                        }
                    }
                ]}
            />
        );
    }
}