import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";

export default class Report extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                countAtRow={1}
                inverted
                items={[
                    { title: "Кассовые отчеты", onPress: () => this.props.controller.router.navigation.navigate('ReportCashbox') },
                    { title: "Управленческие отчеты", onPress: () => this.props.controller.router.navigation.navigate('ReportControl') },
                    { title: "Журнал документов", onPress: () => this.props.controller.router.navigation.navigate('ReportDocuments') },
                    { title: "Журнал операций", onPress: () => this.props.controller.router.navigation.navigate('ReportOperations') }
                ]}
            />
        );
    }
}