import Main from "./main"
import Control from "./control"
import Cashbox from "./cashbox"

export default {
    Main, Cashbox, Control
}