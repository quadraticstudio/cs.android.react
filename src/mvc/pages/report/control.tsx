import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";

export default class ReportControl extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                countAtRow={1}
                inverted
                items={[
                    { title: "Товарные остатки в ценах реализации", onPress: () => this.props.controller.router.navigation.navigate('ReportResidue') },
                    { title: "Движения товаров", onPress: () => this.props.controller.router.navigation.navigate('ReportMove') },
                    { title: "Отчет по закупкам", onPress: () => this.props.controller.router.navigation.navigate('ReportProviderIn') },
                    { title: "Отчет по продажам", onPress: () => this.props.controller.router.navigation.navigate('ReportSale') },
                    { title: "Сводный отчет по продажам", onPress: () => this.props.controller.router.navigation.navigate('ReportConsolidate') },
                    { title: "Отчет по валовой прибыли", onPress: () => this.props.controller.router.navigation.navigate('ReportIncome') }
                ]}
            />
        );
    }
}