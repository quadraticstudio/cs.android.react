import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../abstract";
import { ComponentProps } from "../../abstract/global/index";
import { Alert, AppState, BackHandler } from "react-native";
import DbQuery from "db-berish/lib/query";
import { Version } from "../../model/index";
import Query from "../../abstract/database/lib/query";
import * as Model from "../../model"
import { Util } from "expo";
import { closeSession } from "../../abstract/global/Messages";

export default class Main extends React.Component<ComponentProps> {

    navigate = async (path: string) => {
        let result = await this.props.controller.checkLinking(this.props.controller.router.navigation.navigate, path);
        if (result)
            return result;
    }

    appStateListener = async (state: 'active' | 'inactive' | 'background') => {
        /*if (state == 'inactive' || state == 'background') {
            await parseLogOutSession();
            await logOut();
            Util.reload();
        }*/
    }

    async componentWillMount() {
        this.props.controller.goBackListener(type => this.props.controller.tryLoad(closeSession));
        AppState.addEventListener('change', this.appStateListener);
        await this.props.controller.checkLinking();
        await this.props.controller.checkSessionExpires();
    }

    async componentWillUnmount() {
        AppState.removeEventListener('change', this.appStateListener)
    }


    render() {
        return (
            <Abstract.Tiles
                items={[
                    { title: "Касса", iconName: "md-cash", onPress: () => this.navigate('CashboxMain') },
                    { title: "Товары", iconName: "md-basket", onPress: () => this.navigate('GoodsMain') },
                    { title: "Отчеты", iconName: "md-trending-up", onPress: () => this.navigate('ReportMain') },
                    { title: "Сервис", iconName: "md-build", onPress: () => this.props.controller.inDevelopment('Сервис') },
                    { title: "Справка", iconName: "md-help-circle", onPress: () => this.navigate('HelpMain') },
                    { title: "Настройки", iconName: "md-construct", onPress: () => this.navigate('SettingsMain') },
                    { title: "Дополнительно", iconName: "md-add-circle", onPress: () => this.navigate('AdditionalMain') },
                    { title: "Для разработчиков", iconName: "md-apps", onPress: () => this.props.controller.router.navigation.navigate('LocalDB') }
                ]}
            />
        );
    }
}