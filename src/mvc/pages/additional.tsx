import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../abstract";
import { ComponentProps } from "../../abstract/global/index";
import { IntentLauncherAndroid, WebBrowser } from "expo";

export default class Main extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                countAtRow={1}
                items={[
                    { title: "Настройки", iconName: "md-settings", onPress: () => IntentLauncherAndroid.startActivityAsync("android.settings.SETTINGS") },
                    { title: "Диспетчер файлов", iconName: "md-filing", onPress: () => IntentLauncherAndroid.startActivityAsync("android.intent.action.GET_CONTENT") },
                    { title: "Браузер", iconName: "md-calculator", onPress: () => {
                        WebBrowser.openBrowserAsync('https://google.com/')
                    } }
                ]}
            />
        );
    }
}