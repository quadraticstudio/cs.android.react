import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../abstract";
import { ComponentProps } from "../../abstract/global/index";

export default class Main extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                items={[
                    { title: "Касса", onPress: () => this.props.controller.router.navigation.navigate('Cashbox') },
                    { title: "Товары", onPress: () => this.props.controller.router.navigation.navigate('Goods') },
                    { title: "Отчеты", onPress: () => this.props.controller.router.navigation.navigate('Report') },
                    { title: "Сервис", onPress: () => this.props.controller.router.navigation.navigate('Service') },
                    { title: "Справка", onPress: () => this.props.controller.router.navigation.navigate('Help') },
                    { title: "Настройки", onPress: () => this.props.controller.router.navigation.navigate('Settings') },
                    { title: "Дополнительно", onPress: () => this.props.controller.router.navigation.navigate('Additional') },
                ]}
            />
        );
    }
}