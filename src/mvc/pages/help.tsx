import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../abstract";
import { ComponentProps } from "../../abstract/global/index";
import { WebView } from "react-native";

export default class Main extends React.Component<ComponentProps> {

    render() {
        return (
            <WebView
                source={{ uri: 'https://google.com/' }}
            />
        );
    }
}