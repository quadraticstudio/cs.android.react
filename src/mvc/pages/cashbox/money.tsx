import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";
import { Alert } from "react-native";

export default class CashboxMoney extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                inverted
                items={[
                    { title: "Внесение", iconSize: 50, iconName: "ios-card-outline", onPress: () => this.props.controller.router.navigation.navigate('DocCashInForm') },
                    { title: "Выплата", iconSize: 50, iconName: "ios-cash-outline", onPress: () => this.props.controller.router.navigation.navigate('DocCashOutForm') },
                    {
                        title: "Ящик", iconSize: 50, iconName: "logo-dropbox", onPress: () => {
                            Alert.alert('Ящик', 'Устройство не подключено')
                            this.props.controller.router.navigation.navigate('Service')
                        }
                    }
                ]}
            />
        );
    }
}