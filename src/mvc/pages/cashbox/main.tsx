import * as React from "react";
import * as Parse from "parse";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";
import { Alert } from "react-native";
import { closeSession } from "../../../abstract/global/Messages";

export default class Cashbox extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                inverted
                items={[
                    {
                        title: "Продажа", iconName: "ios-pricetag", onPress: () => {
                            this.props.controller.router.navigation.navigate('DocSaleSelect')
                        }
                    },
                    {
                        title: "Возврат", iconName: "ios-refresh-circle", onPress: () => {
                            this.props.controller.router.navigation.navigate('DocReturnSelect')
                        }
                    },
                    {
                        title: "Наличные", iconName: "ios-cash", onPress: () => {
                            this.props.controller.router.navigation.navigate('CashboxMoney')
                        }
                    },
                    {
                        title: "Терминал", iconName: "ios-card", onPress: () => {
                            Alert.alert('Терминал', 'Устройство не подключено')
                        }
                    },
                    {
                        title: "Закрытие смены", iconName: "ios-lock", onPress: () => {
                            this.props.controller.tryLoad(closeSession, null, true);
                            //this.props.controller.router.navigation.navigate('Main')
                        }
                    },
                ]}
            />
        );
    }
}