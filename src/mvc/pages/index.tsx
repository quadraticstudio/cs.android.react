import Main from "./main"
import Cashbox from "./cashbox/index"
import Goods from "./goods/index"
import Report from "./report/index"
import Service from "./service"
import Help from "./help"
import Settings from "./settings/index"
import Additional from "./additional"

export default {
    Main, Cashbox, Goods, Report, Service, Help, Settings, Additional
}