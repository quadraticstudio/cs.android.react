import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";

export default class Main extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                countAtRow={1}
                inverted
                items={[
                    { title: "Список товаров", iconName: "ios-pricetag", onPress: () => this.props.controller.router.navigation.navigate('GoodsList', { scene: 'GoodsMain' }) },
                    { title: "Инвентаризация", iconName: "ios-clipboard", onPress: () => this.props.controller.router.navigation.navigate('DocInventoryList') },
                    { title: "Приемка и переоценка", iconSize: 45, iconName: "ios-code-download-outline", onPress: () => this.props.controller.router.navigation.navigate('DocProviderInSelect') },
                    { title: "Возврат к поставщику", iconName: "ios-refresh-circle", onPress: () => this.props.controller.router.navigation.navigate('DocProviderOutSelect') },
                    { title: "Списание товара", iconName: "ios-trash", onPress: () => this.props.controller.router.navigation.navigate('DocCleanSelect') }
                ]}
            />
        );
    }
}