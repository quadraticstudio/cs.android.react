import Main from "./main"
import Manual from "./manual"
import General from "./general"

export default {
    Main, Manual, General
}