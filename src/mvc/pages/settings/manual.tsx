import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";

export default class Main extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                countAtRow={1}
                inverted
                items={[
                    { title: "Типы цен", iconName: "logo-usd", onPress: () => this.props.controller.router.navigation.navigate('SettingsManualPrices') },
                    //{ title: "Скидки", iconName: "ios-trending-down", onPress: () => this.props.controller.router.navigation.navigate('SettingsManualSales') },
                    { title: "Шаблоны штрихкодов", iconName: "ios-barcode"},
                    { title: "Шаблон чека ККМ", iconName: "ios-paper"}
                ]}
            />
        );
    }
}