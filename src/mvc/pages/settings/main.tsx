import * as React from "react";
import { View, Text, Button } from "native-base";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";

export default class Main extends React.Component<ComponentProps> {

    render() {
        return (
            <Abstract.Tiles
                countAtRow={1}
                inverted
                items={[
                    { title: "Основные", iconName: "ios-warning", onPress: () => this.props.controller.router.navigation.navigate('SettingsGeneral') },
                    { title: "Смена", iconName: "ios-timer", onPress: () => this.props.controller.router.navigation.navigate('SettingsTurn') },
                    { title: "Справочники", iconName: "ios-book", onPress: () => this.props.controller.router.navigation.navigate('SettingsManual') },
                    { title: "Оборудование", iconName: "ios-phone-portrait" }
                ]}
            />
        );
    }
}