import * as React from "react";
import { View, Text, Button } from "native-base";
import { StyleSheet, Alert } from "react-native";
import Abstract from "../../../abstract";
import { ComponentProps } from "../../../abstract/global/index";

export interface SettingsItem {
    title: string;
    config: any;
    divider?: boolean;
    switch?: boolean;
    modal?: boolean;
}

export default class Main extends React.Component<ComponentProps> {

    constructor (props) {
        super(props);
        this.state = {};
    }

    getItems = () => {
        return [
            { title: "Системные", divider: true },
            { title: "Одинаковые штрихкоды товаров", switch: true, config: {path: "eq1"} },
            { title: "Отрицательные товарные остатки", switch: true, config: {path: "eq2"} }, 
            { title: "Безналичный возврат без основания", switch: true, config: {path: "eq3"} }, 
            { title: "Отключать Bluetooth при выходе", switch: true, config: {path: "eq4"} },
            { title: "Документы", divider: true },
            { title: "Срок хранения документов", modal: true },
            { title: "Вести журнал операций", switch: true, config: {path: "eq5"} },
            { title: "Срок хранения журнала операций", modal: true },
            { title: "Налоги", divider: true },
            { title: "Налоговая ставка для позиций по своб." }
        ];
    }

    onClick = (index:number, item: SettingsItem) => {
        let { state } = this;
        if (item.switch) {
            state[item.config.path] = !state[item.config.path];
            this.setState(state);
        }
        if (item.modal) {
            
        }
    }

    renderRow = (index: number, item: SettingsItem) => {
        return (
            <View style={styles.item}>
                { item.switch ? <Abstract.General.Switch
                    textStyle={{color: "white"}}
                    label={item.title}
                    config={{
                        object: this.state,
                        path: item.config.path,
                        afterSet: (m) => this.setState(m)
                    }}
                /> : <Text style={{color: "white"}}>{item.title}</Text> }
            </View>
        );
    }

    render() {
        return (
            <Abstract.General.List
                items={this.getItems()}
                renderRow={this.renderRow}
                onClick={this.onClick}
            />
        );
    }
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between"
    }
});