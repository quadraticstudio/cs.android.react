import Blank from "./blank/index"
import Auth from "./auth"
import Pages from "./pages"
import Goods from "./goods"
import Settings from "./settings"
import Report from "./report"
import Doc from "./doc"

export default {
    Blank, Auth, Pages, Settings, Goods, Report, Doc
}