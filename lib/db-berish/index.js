﻿"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const adapter_1 = require("./lib/adapter");
exports.Adapter = adapter_1.default;
const object_1 = require("./lib/object");
exports.Object = object_1.default;
const query_1 = require("./lib/query");
exports.Query = query_1.default;
const chain_1 = require("./lib/chain");
exports.Chain = chain_1.default;
const Utils = require("./lib/util");
exports.Utils = Utils;
function initialize(config) {
    return adapter_1.default.init(config).instance;
}
exports.initialize = initialize;
//# sourceMappingURL=index.js.map