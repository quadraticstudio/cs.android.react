﻿"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class DbChain {
    constructor(prefix, storage) {
        this.current = null;
        this.path = null;
        this.storage = null;
        this.current = prefix;
        this.storage = storage;
    }
    get fullPath() {
        return `${this.path || ''}${this.path ? '/' : ''}${this.current}`;
    }
    chain(key) {
        let chain = new DbChain(key, this.storage);
        chain.path = this.fullPath;
        return chain;
    }
    value(defaultValue) {
        return __awaiter(this, void 0, void 0, function* () {
            let result = yield this.getItem(this.fullPath);
            let value = this.fromString(result);
            if (defaultValue == null)
                return value;
            if (value == null) {
                yield this.write(defaultValue);
                return defaultValue;
            }
            return value;
        });
    }
    write(value) {
        let result = this.fromObject(value);
        return this.setItem(this.fullPath, result);
    }
    remove() {
        return this.removeItem(this.fullPath);
    }
    fromObject(obj) {
        if (obj == null)
            return null;
        return JSON.stringify(obj);
    }
    fromString(str) {
        if (str == null)
            return null;
        return JSON.parse(str);
    }
    getItem(key) {
        return new Promise((resolve, reject) => {
            this.storage.getItem(key, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(result);
            });
        });
    }
    setItem(key, value) {
        return new Promise((resolve, reject) => {
            this.storage.setItem(key, value, err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }
    removeItem(key) {
        return new Promise((resolve, reject) => {
            this.storage.removeItem(key, err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }
}
exports.default = DbChain;
//# sourceMappingURL=chain.js.map