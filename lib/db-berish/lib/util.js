﻿"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function guid() {
    return shortGuid() + shortGuid() + '-' + shortGuid() + '-' + shortGuid() + '-' +
        shortGuid() + '-' + shortGuid() + shortGuid() + shortGuid();
}
exports.guid = guid;
function shortGuid() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}
exports.shortGuid = shortGuid;
function generateId() {
    return `${shortGuid()}${shortGuid()}`;
}
exports.generateId = generateId;
//# sourceMappingURL=util.js.map