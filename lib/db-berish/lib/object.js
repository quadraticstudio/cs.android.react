﻿"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const linq_1 = require("linq");
const adapter_1 = require("./adapter");
const util_1 = require("./util");
const query_1 = require("./query");
class DbObject {
    constructor(className) {
        this._collection = {};
        this.className = className || this.constructor.name;
    }
    get id() {
        return this.get('id');
    }
    set id(value) {
        this.set('id', value);
    }
    get className() {
        return this.get('className');
    }
    set className(value) {
        this.set('className', value);
    }
    get keyValueCollection() {
        return this._collection;
    }
    get(property) {
        let value = this._collection[property];
        if (typeof value == 'object' && value != null && '__id' in value && '__className' in value) {
            let ctor = adapter_1.default.ctors[value['__className']] || DbObject;
            let object = new ctor();
            object.id = value["__id"];
            return object;
        }
        else if (value instanceof Array && value.length > 0 && linq_1.LINQ.fromArray(value).all(m => typeof m == 'object' && '__id' in m && '__className' in m)) {
            return linq_1.LINQ.fromArray(value).select(m => {
                let ctor = adapter_1.default.ctors[m['__className']] || DbObject;
                let object = new ctor();
                object.id = m["__id"];
                return object;
            }).toArray();
        }
        else
            return value;
    }
    set(property, value) {
        this._collection[property] = value;
    }
    fetch() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.id)
                return this;
            return new query_1.default(this.className).get(this.id);
        });
    }
    prepareDBOBject(collection) {
        collection = collection || {};
        let newCollection = {};
        let keys = Object.keys(collection);
        let array = linq_1.LINQ.fromArray(keys).select(key => {
            let value = collection[key];
            if (value instanceof DbObject) {
                return {
                    key: key,
                    value: {
                        __id: value.id,
                        __className: value.className
                    }
                };
            }
            else if (value instanceof Array && value.length > 0 && linq_1.LINQ.fromArray(value).all(m => m instanceof DbObject)) {
                return {
                    key: key,
                    value: linq_1.LINQ.fromArray(value).select(m => {
                        return {
                            __id: m.id,
                            __className: m.className
                        };
                    }).toArray()
                };
            }
            else
                return {
                    key: key,
                    value: value
                };
        }).toArray();
        for (let key of array)
            newCollection[key.key] = key.value;
        return newCollection;
    }
    destroy() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.id)
                throw new Error(`can't destroy object without id`);
            return adapter_1.default.instance.clearItem(this.className, this.id);
        });
    }
    save() {
        return __awaiter(this, void 0, void 0, function* () {
            let classChain = yield adapter_1.default.instance.getClass(this.className);
            let hasId = !!this.id;
            let ids = yield classChain.value([]);
            this.id = this.id || util_1.generateId();
            let prepare = this.prepareDBOBject(this._collection);
            const _save = (id) => __awaiter(this, void 0, void 0, function* () {
                ids.push(id);
                yield classChain.write(ids);
                yield classChain.chain(id).write(prepare);
            });
            if (hasId) {
                let isExists = ids.indexOf(this.id) != -1;
                if (isExists) {
                    yield classChain.chain(this.id).write(prepare);
                }
                else {
                    yield _save(this.id);
                }
            }
            else {
                yield _save(this.id);
            }
            return this;
        });
    }
}
exports.default = DbObject;
//# sourceMappingURL=object.js.map