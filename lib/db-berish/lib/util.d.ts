﻿export declare function guid(): string;
export declare function shortGuid(): string;
export declare function generateId(): string;
