﻿import { LINQ } from "linq";
import DbObject from "./object";
import DbChain from "./chain";
export default class DbQuery<T extends DbObject> {
    protected ctor: typeof DbObject;
    protected name: string;
    protected classChain: Promise<DbChain>;
    constructor(className: new (...args) => T);
    constructor(className: string);
    constructor(className: (new (...args) => T) | string);
    get(id: string): Promise<T>;
    find(): Promise<LINQ<T>>;
}
