﻿import { AsyncStorage } from "react-native";
import DbObject from "./object";
import DbChain from "./chain";
export interface DbConfig {
    storage: AsyncStorage;
    dbPrefix?: string;
}
export default class DbAdapter {
    storage: AsyncStorage;
    prefix: string;
    private dbChain;
    static ctors: {
        [className: string]: typeof DbObject;
    };
    private static _instance;
    constructor(config: DbConfig);
    static init(config: DbConfig): typeof DbAdapter;
    static initDBObject(ctor: typeof DbObject, className?: string): typeof DbObject;
    static readonly instance: DbAdapter;
    getClass: (className: string) => Promise<DbChain>;
    clearDB: () => Promise<void>;
    clearClass: (className: string) => Promise<void>;
    clearItem: (className: string, id: string) => Promise<void>;
}
