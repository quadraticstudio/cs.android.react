﻿import { AsyncStorage } from "react-native";
export default class DbChain {
    private current;
    private path;
    private storage;
    private readonly fullPath;
    constructor(prefix: string, storage: AsyncStorage);
    chain(key: string): DbChain;
    value<T = any>(defaultValue?: T): Promise<T>;
    write<T = any>(value: T): Promise<void>;
    remove(): Promise<void>;
    private fromObject(obj);
    private fromString(str);
    private getItem(key);
    private setItem(key, value);
    private removeItem(key);
}
