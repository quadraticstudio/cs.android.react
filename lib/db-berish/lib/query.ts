import { LINQ } from "linq";
import DbObject from "./object";
import DbAdapter from "./adapter";
import DbChain from "./chain";

export default class DbQuery<T extends DbObject> {
    protected ctor: typeof DbObject = null;
    protected name: string = null;
    protected classChain: Promise<DbChain> = null;

    constructor(className: new (...args) => T);
    constructor(className: string);
    constructor(className: (new (...args) => T) | string)
    constructor(className: (new (...args) => T) | string) {
        this.ctor = typeof className == 'string' ? (DbAdapter.ctors[className] || DbObject) : className;
        this.name = typeof className == 'string' ? className : className.name;
        this.classChain = DbAdapter.instance.getClass(this.name);
    }

    async get(id: string) {
        let obj = await this.classChain.then(classChain => classChain.chain(id).value(new this.ctor()));
        let normalWithCtor = new this.ctor() as T;
        normalWithCtor['_collection'] = obj;
        return normalWithCtor;
    }

    async find() {
        let ids = await this.classChain.then(classChain => classChain.value<string[]>([]));
        let array = await Promise.all(LINQ.fromArray(ids).select(m => this.classChain.then(classChain => classChain.chain(m).value(new this.ctor()))).toArray())
        let list = LINQ.fromArray(array).select(m => {
            let obj = new this.ctor() as T;
            obj['_collection'] = m;
            return obj;
        });
        return list;
    }
}