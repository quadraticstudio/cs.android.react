import { LINQ } from "linq";
import DbAdapter from "./adapter";
import { generateId } from "./util";
import DbQuery from "./query";

export default class DbObject {
    private _collection: { [property: string]: any; } = {};

    constructor(className?: string) {
        this.className = className || this.constructor.name;
    }

    get id() {
        return this.get('id');
    }

    set id(value: string) {
        this.set('id', value);
    }

    get className() {
        return this.get('className');
    }

    set className(value: string) {
        this.set('className', value);
    }

    get keyValueCollection() {
        return this._collection;
    }

    get<T = any>(property: string) {
        let value = this._collection[property] as T;
        if (typeof value == 'object' && value != null && '__id' in value && '__className' in value) {
            let ctor = DbAdapter.ctors[value['__className']] || DbObject;
            let object = new ctor()
            object.id = value["__id"];
            return object as any as T;
        } else if (value instanceof Array && value.length > 0 && LINQ.fromArray(value).all(m => typeof m == 'object' && '__id' in m && '__className' in m)) {
            return LINQ.fromArray(value).select(m => {
                let ctor = DbAdapter.ctors[m['__className']] || DbObject;
                let object = new ctor();
                object.id = m["__id"];
                return object;
            }).toArray() as any as T;
        } else
            return value;
    }

    set<T = any>(property: string, value: T) {
        this._collection[property] = value;
    }

    async fetch() {
        if (!this.id)
            return this;
        return new DbQuery<this>(this.className).get(this.id);
        // if (item)
        //     this._collection = item._collection;
        // return this;
    }

    private prepareDBOBject(collection: { [property: string]: any }) {
        collection = collection || {};
        let newCollection: { [property: string]: any } = {};
        let keys = Object.keys(collection);
        let array = LINQ.fromArray(keys).select(key => {
            let value = collection[key];
            if (value instanceof DbObject) {
                return {
                    key: key,
                    value: {
                        __id: value.id,
                        __className: value.className
                    }
                }
            } else if (value instanceof Array && value.length > 0 && LINQ.fromArray(value).all(m => m instanceof DbObject)) {
                return {
                    key: key,
                    value: LINQ.fromArray(value).select(m => {
                        return {
                            __id: m.id,
                            __className: m.className
                        }
                    }).toArray()
                };
            } else
                return {
                    key: key,
                    value: value
                };
        }).toArray();
        for (let key of array)
            newCollection[key.key] = key.value;
        return newCollection;
    }

    async destroy() {
        if (!this.id)
            throw new Error(`can't destroy object without id`)
        return DbAdapter.instance.clearItem(this.className, this.id);
    }

    async save() {
        let classChain = await DbAdapter.instance.getClass(this.className);
        let hasId = !!this.id;
        let ids = await classChain.value<string[]>([]);
        this.id = this.id || generateId();
        let prepare = this.prepareDBOBject(this._collection);
        const _save = async (id: string) => {
            ids.push(id);
            await classChain.write(ids);
            await classChain.chain(id).write(prepare);
        }
        if (hasId) {
            let isExists = ids.indexOf(this.id) != -1;
            if (isExists) {
                await classChain.chain(this.id).write(prepare);
            } else {
                await _save(this.id);
            }
        } else {
            await _save(this.id);
        }
        return this;
    }
}