﻿"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const linq_1 = require("linq");
const object_1 = require("./object");
const adapter_1 = require("./adapter");
class DbQuery {
    constructor(className) {
        this.ctor = null;
        this.name = null;
        this.classChain = null;
        this.ctor = typeof className == 'string' ? (adapter_1.default.ctors[className] || object_1.default) : className;
        this.name = typeof className == 'string' ? className : className.name;
        this.classChain = adapter_1.default.instance.getClass(this.name);
    }
    get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let obj = yield this.classChain.then(classChain => classChain.chain(id).value(new this.ctor()));
            let normalWithCtor = new this.ctor();
            normalWithCtor['_collection'] = obj;
            return normalWithCtor;
        });
    }
    find() {
        return __awaiter(this, void 0, void 0, function* () {
            let ids = yield this.classChain.then(classChain => classChain.value([]));
            let array = yield Promise.all(linq_1.LINQ.fromArray(ids).select(m => this.classChain.then(classChain => classChain.chain(m).value(new this.ctor()))).toArray());
            let list = linq_1.LINQ.fromArray(array).select(m => {
                let obj = new this.ctor();
                obj['_collection'] = m;
                return obj;
            });
            return list;
        });
    }
}
exports.default = DbQuery;
//# sourceMappingURL=query.js.map