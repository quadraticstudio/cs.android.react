﻿export default class DbObject {
    private _collection;
    constructor(className?: string);
    id: string;
    className: string;
    readonly keyValueCollection: {
        [property: string]: any;
    };
    get<T = any>(property: string): T;
    set<T = any>(property: string, value: T): void;
    fetch(): Promise<this>;
    private prepareDBOBject(collection);
    destroy(): Promise<void>;
    save(): Promise<this>;
}
