import { AsyncStorage } from "react-native";

export default class DbChain {
    private current: string = null;
    private path: string = null;
    private storage: AsyncStorage = null;

    private get fullPath() {
        return `${this.path || ''}${this.path ? '/' : ''}${this.current}`;
    }

    constructor(prefix: string, storage: AsyncStorage) {
        this.current = prefix;
        this.storage = storage;
    }

    chain(key: string) {
        let chain = new DbChain(key, this.storage);
        chain.path = this.fullPath;
        return chain;
    }

    async value<T = any>(defaultValue?: T) {
        let result = await this.getItem(this.fullPath);
        let value = this.fromString(result) as T;
        if (defaultValue == null)
            return value;
        if (value == null) {
            await this.write(defaultValue);
            return defaultValue;
        }
        return value;
    }

    write<T = any>(value: T) {
        let result = this.fromObject(value);
        return this.setItem(this.fullPath, result);
    }

    remove() {
        return this.removeItem(this.fullPath);
    }

    private fromObject(obj: any) {
        if (obj == null)
            return null;
        return JSON.stringify(obj);
    }

    private fromString(str: string) {
        if (str == null)
            return null;
        return JSON.parse(str);
    }

    private getItem(key: string) {
        return new Promise<string>((resolve, reject) => {
            this.storage.getItem(key, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(result);
            })
        })
    }

    private setItem(key: string, value: string) {
        return new Promise<void>((resolve, reject) => {
            this.storage.setItem(key, value, err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            })
        })
    }

    private removeItem(key: string) {
        return new Promise<void>((resolve, reject) => {
            this.storage.removeItem(key, err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            })
        })
    }
}