﻿"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const chain_1 = require("./chain");
const linq_1 = require("linq");
class DbAdapter {
    constructor(config) {
        this.storage = null;
        this.prefix = null;
        this.dbChain = null;
        this.getClass = (className) => __awaiter(this, void 0, void 0, function* () {
            let chain = yield this.dbChain.value([]);
            let isExists = linq_1.LINQ.fromArray(chain).contains(className);
            if (!isExists)
                yield this.dbChain.write(chain.concat(className));
            return this.dbChain.chain(className);
        });
        this.clearDB = () => __awaiter(this, void 0, void 0, function* () {
            let classNames = yield this.dbChain.value([]);
            yield Promise.all(linq_1.LINQ.fromArray(classNames).select(m => this.clearClass(m)).toArray());
            yield this.dbChain.write(null);
        });
        this.clearClass = (className) => __awaiter(this, void 0, void 0, function* () {
            let classChain = yield this.getClass(className);
            let idsClass = yield classChain.value([]);
            yield Promise.all(linq_1.LINQ.fromArray(idsClass).select(m => classChain.chain(m).write(null)).toArray());
            yield classChain.write(null);
        });
        this.clearItem = (className, id) => __awaiter(this, void 0, void 0, function* () {
            let classChain = yield this.getClass(className);
            yield classChain.chain(id).write(null);
            let idsClass = yield classChain.value([]);
            idsClass = linq_1.LINQ.fromArray(idsClass).except([id]).toArray();
            yield classChain.write(idsClass);
        });
        this.storage = config.storage;
        this.prefix = config.dbPrefix || "localdb";
        this.dbChain = new chain_1.default(this.prefix, this.storage);
    }
    static init(config) {
        this._instance = new DbAdapter(config);
        return this;
    }
    static initDBObject(ctor, className) {
        let name = className || ctor.name;
        if (DbAdapter.ctors[name] == null)
            DbAdapter.ctors[name] = ctor;
        return ctor;
    }
    static get instance() {
        if (this._instance == null)
            throw new Error('instance DB not initialized');
        return this._instance;
    }
}
DbAdapter.ctors = {};
DbAdapter._instance = null;
exports.default = DbAdapter;
//# sourceMappingURL=adapter.js.map