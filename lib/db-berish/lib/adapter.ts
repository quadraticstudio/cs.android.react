import { AsyncStorage } from "react-native";
import DbObject from "./object";
import DbChain from "./chain";
import { LINQ } from "linq";

export interface DbConfig {
    storage: AsyncStorage,
    dbPrefix?: string;
}

export default class DbAdapter {
    public storage: AsyncStorage = null;
    public prefix: string = null;
    private dbChain: DbChain = null;
    public static ctors: { [className: string]: typeof DbObject } = {};
    private static _instance: DbAdapter = null;

    constructor(config: DbConfig) {
        this.storage = config.storage;
        this.prefix = config.dbPrefix || "localdb";
        this.dbChain = new DbChain(this.prefix, this.storage);
    }

    static init(config: DbConfig) {
        this._instance = new DbAdapter(config);
        return this;
    }

    static initDBObject(ctor: typeof DbObject, className?: string) {
        let name = className || ctor.name;
        if (DbAdapter.ctors[name] == null)
            DbAdapter.ctors[name] = ctor;
        return ctor;
    }

    public static get instance() {
        if (this._instance == null)
            throw new Error('instance DB not initialized')
        return this._instance;
    }

    getClass = async (className: string) => {
        let chain = await this.dbChain.value<string[]>([]);
        let isExists = LINQ.fromArray(chain).contains(className);
        if (!isExists)
            await this.dbChain.write(chain.concat(className));
        return this.dbChain.chain(className);
    }

    clearDB = async () => {
        let classNames = await this.dbChain.value<string[]>([]);
        //clear all classes
        await Promise.all(LINQ.fromArray(classNames).select(m => this.clearClass(m)).toArray());
        //clear db
        await this.dbChain.write(null);
    }

    clearClass = async (className: string) => {
        let classChain = await this.getClass(className);
        //remove all items in class
        let idsClass = await classChain.value<string[]>([]);
        await Promise.all(LINQ.fromArray(idsClass).select(m => classChain.chain(m).write(null)).toArray());
        //remove class
        await classChain.write(null);
    }

    clearItem = async (className: string, id: string) => {
        //remove item
        let classChain = await this.getClass(className);
        await classChain.chain(id).write(null);
        //remove id in ids list of class
        let idsClass = await classChain.value<string[]>([]);
        idsClass = LINQ.fromArray(idsClass).except([id]).toArray();
        await classChain.write(idsClass);
    }
}

