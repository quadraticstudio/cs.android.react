export function guid() {
    return shortGuid() + shortGuid() + '-' + shortGuid() + '-' + shortGuid() + '-' +
        shortGuid() + '-' + shortGuid() + shortGuid() + shortGuid();
}

export function shortGuid() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1)
}

export function generateId() {
    return `${shortGuid()}${shortGuid()}`
}