﻿import Adapter, { DbConfig } from "./lib/adapter";
import Object from "./lib/object";
import Query from "./lib/query";
import Chain from "./lib/chain";
import * as Utils from "./lib/util";
export declare function initialize(config: DbConfig): Adapter;
export { Adapter, Object, Query, Chain, Utils };
