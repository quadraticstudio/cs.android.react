﻿"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const linq_1 = require("linq");
function execute(query, queryOptions) {
    return __awaiter(this, void 0, void 0, function* () {
        let options = getOptions(queryOptions);
        let skip = 0;
        const limit = 1000;
        let objects = [];
        let count = yield query.count(options.count);
        let done = false;
        while (!done) {
            let temp = yield query.ascending('createdAt').limit(limit).skip(skip).find(options.find);
            objects = objects.concat(temp);
            skip += limit;
            if (skip > count)
                done = true;
            if (skip > 10000) {
                skip = 0;
                let createdAt = temp[temp.length - 1].createdAt;
                query = query.greaterThanOrEqualTo('createdAt', createdAt);
                if (objects.length >= count)
                    done = true;
            }
        }
        return linq_1.LINQ.fromArray(objects);
    });
}
exports.default = execute;
function getOptions(options) {
    let opt = {};
    if (options == null) {
        opt = {
            count: undefined,
            each: undefined,
            find: undefined,
            first: undefined,
            get: undefined
        };
    }
    else {
        let countOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let eachOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let findOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let firstOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let getOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        opt = {
            count: countOptions,
            each: eachOptions,
            find: findOptions,
            first: firstOptions,
            get: getOptions
        };
    }
    return opt;
}
//# sourceMappingURL=index.js.map