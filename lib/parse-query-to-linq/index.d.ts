﻿/// <reference types="parse" />
import { LINQ } from "linq";
import * as Parse from "parse";
export default function execute<T extends Parse.Object>(query: Parse.Query<T>, queryOptions?: ParseQueryOptions): Promise<LINQ<T>>;
export interface ParseQueryOptions {
    error?: Function;
    success?: Function;
    useMasterKey?: boolean;
    sessionToken?: string;
}
