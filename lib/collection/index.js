"use strict";
var linq_1 = require("linq");
var KeyValuePair = (function () {
    function KeyValuePair(key, value) {
        this.key = key;
        this.value = value;
    }
    return KeyValuePair;
}());
exports.KeyValuePair = KeyValuePair;
var Dictionary = (function () {
    function Dictionary() {
        var values = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            values[_i] = arguments[_i];
        }
        this.items = [];
        this.items = values;
    }
    Dictionary.prototype.add = function (key, value) {
        if (value != null)
            this.items.push(new KeyValuePair(key, value));
        else
            this.items.push(key);
    };
    Dictionary.prototype.containsKey = function (key) {
        return this.toLinq().count(function (m) { return m.key === key; }) !== 0;
    };
    Dictionary.prototype.count = function () {
        return this.items.length;
    };
    Dictionary.prototype.get = function (key) {
        return this.toLinq().single(function (m) { return m.key === key; }).value;
    };
    Dictionary.prototype.getByValue = function (value) {
        return this.toLinq().where(function (m) { return m.value === value; }).select(function (m) { return m.key; }).toArray();
    };
    Dictionary.prototype.remove = function (key) {
        var kv = this.toLinq().single(function (m) { return m.key === key; });
        this.items = this.toLinq().where(function (m) { return m !== kv; }).toArray();
        return kv.value;
    };
    Dictionary.prototype.keys = function () {
        return this.toLinq().select(function (m) { return m.key; });
    };
    Dictionary.prototype.values = function () {
        return this.toLinq().select(function (m) { return m.value; });
    };
    Dictionary.prototype.asArray = function () {
        return this.items;
    };
    Dictionary.prototype.toLinq = function () {
        return linq_1.LINQ.fromArray(this.items);
    };
    Dictionary.prototype.toJson = function () {
        return JSON.stringify(this.items);
    };
    Dictionary.prototype.fromJson = function (value) {
        return Dictionary.fromJson(value);
    };
    Dictionary.prototype.fromArray = function (array) {
        return Dictionary.fromArray(array);
    };
    Dictionary.prototype.fromDictionary = function (dict) {
        return Dictionary.fromDictionary(dict);
    };
    Dictionary.fromJson = function (value) {
        return new (Dictionary.bind.apply(Dictionary, [void 0].concat(JSON.parse(value).items)))();
    };
    Dictionary.fromArray = function (array) {
        return new (Dictionary.bind.apply(Dictionary, [void 0].concat(array)))();
    };
    Dictionary.fromDictionary = function (dict) {
        return new (Dictionary.bind.apply(Dictionary, [void 0].concat(dict.items)))();
    };
    return Dictionary;
}());
exports.Dictionary = Dictionary;
//export class Dictionary<TKey, TValue> implements IDictionary<TKey, TValue> {
//    private items: { [index: string]: TValue } = {};
//    private count: number = 0;
//    constructor();
//    constructor(...values: KeyValuePair<TKey, TValue>[]);
//    constructor(...values: KeyValuePair<TKey, TValue>[]) {
//        for (let val of values) {
//            this.Add(val.key, val.value);
//        }
//    }
//    public ContainsKey(key: TKey): boolean {
//        return this.items.hasOwnProperty(JSON.stringify(key));
//    }
//    public Count(): number {
//        return this.count;
//    }
//    public Add(key: TKey, value: TValue) {
//        this.items[JSON.stringify(key)] = value;
//        this.count++;
//    }
//    public Remove(key: TKey): TValue {
//        let k = JSON.stringify(key);
//        var val = this.items[k];
//        delete this.items[k];
//        this.count--;
//        return val;
//    }
//    public Get(key: TKey): TValue {
//        return this.items[JSON.stringify(key)];
//    }
//    public GetByValue(value: TValue): TKey[] {
//        let keySet: TKey[] = [];
//        for (let key in this.items) {
//            let val = this.items[key];
//            if (val === value)
//                keySet.push(<TKey>JSON.parse(key));
//        }
//        return keySet;
//    }
//    public Keys(): TKey[] {
//        var keySet: TKey[] = [];
//        for (var prop in this.items) {
//            if (this.items.hasOwnProperty(prop)) {
//                keySet.push(<TKey>JSON.parse(prop));
//            }
//        }
//        return keySet;
//    }
//    public Values(): TValue[] {
//        var values: TValue[] = [];
//        for (var prop in this.items) {
//            if (this.items.hasOwnProperty(prop)) {
//                values.push(this.items[prop]);
//            }
//        }
//        return values;
//    }
//    public AsArray(): Array<IKeyValuePair<TKey, TValue>> {
//        let array: Array<IKeyValuePair<TKey, TValue>> = new Array<IKeyValuePair<TKey, TValue>>();
//        for (var prop in this.items) {
//            if (this.items.hasOwnProperty(prop)) {
//                array.push(new KeyValuePair<TKey, TValue>(<TKey>JSON.parse(prop), this.items[prop]));
//            }
//        }
//        return array;
//    }
//    public FromArray(array: IKeyValuePair<TKey, TValue>[]) {
//        for (let prop of array) {
//            this.Add(prop.key, prop.value);
//        }
//        return this;
//    }
//} 
//# sourceMappingURL=index.js.map