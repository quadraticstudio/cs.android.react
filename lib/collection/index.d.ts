import { LINQ } from "linq";
export interface IKeyValuePair<TKey, TValue> {
    key: TKey;
    value: TValue;
}
export declare class KeyValuePair<TKey, TValue> implements IKeyValuePair<TKey, TValue> {
    key: TKey;
    value: TValue;
    constructor(key: TKey, value: TValue);
}
export declare class Dictionary<TKey, TValue> {
    private items;
    constructor();
    constructor(...values: KeyValuePair<TKey, TValue>[]);
    add(key: IKeyValuePair<TKey, TValue>): void;
    add(key: TKey, value: TValue): void;
    containsKey(key: TKey): boolean;
    count(): number;
    get(key: TKey): TValue;
    getByValue(value: TValue): TKey[];
    remove(key: TKey): TValue;
    keys(): LINQ<TKey>;
    values(): LINQ<TValue>;
    asArray(): KeyValuePair<TKey, TValue>[];
    toLinq(): LINQ<KeyValuePair<TKey, TValue>>;
    toJson(): string;
    fromJson(value: string): Dictionary<TKey, TValue>;
    fromArray(array: IKeyValuePair<TKey, TValue>[]): Dictionary<TKey, TValue>;
    fromDictionary(dict: Dictionary<TKey, TValue>): Dictionary<TKey, TValue>;
    static fromJson<TKey, TValue>(value: string): Dictionary<TKey, TValue>;
    static fromArray<TKey, TValue>(array: IKeyValuePair<TKey, TValue>[]): Dictionary<TKey, TValue>;
    static fromDictionary<TKey, TValue>(dict: Dictionary<TKey, TValue>): Dictionary<TKey, TValue>;
}
