import * as DbBerish from "db-berish";
import * as Parse from "parse";

export class Object {
    private _collection: { [property: string]: any; } = {};

    constructor(className?: string) {
    }

    save() {

    }

    destroy() {

    }

    fetch() {

    }

    getParseObject() {

    }

    getBerishObject() {

    }
}

export class Company extends Object {
    constructor() {
        super('Company');
    }
}

init(Company);

function ctorParse(ctor: typeof Object, className: string) {
    return class ParseObject extends Parse.Object {
        constructor() {
            super(className);
        }
    }
}

function ctorBerish(ctor: typeof Object, className: string) {
    return class BerishObject extends DbBerish.Object {
        constructor() {
            super(className);
        }
    }
}

function init(ctor: typeof Object, className?: string) {
    className = className || ctor.name;
    let parseCtor = ctorParse(ctor, className);
    let berishCtor = ctorBerish(ctor, className);
    DbBerish.Adapter.initDBObject(berishCtor, className);
    Parse.Object.registerSubclass(className, parseCtor);
}