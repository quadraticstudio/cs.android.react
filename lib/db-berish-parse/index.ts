import * as DbBerish from "db-berish";
import * as Parse from "parse";
import { DbConfig } from "db-berish/lib/adapter";

export interface IParseConfig {
    serverUrl: string;
    appId: string;
    jsKey: string;
}

export function initialize(berish: DbConfig, parse: IParseConfig) {
    DbBerish.Adapter.init(berish);
    Parse.initialize(parse.appId, parse.jsKey);
    Parse!.serverURL = parse.serverUrl;
}