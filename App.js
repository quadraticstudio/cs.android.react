import React from 'react';
import Main from "./build"
import { Root } from "native-base";
import { Util } from "expo";
import { Alert } from 'react-native';

export default class App extends React.Component {

  async componentWillMount() {
    Util.addNewVersionListenerExperimental(() => {
      Util.reload();
    })
  }

  render() {
    return (
      <Root>
        <Main />
      </Root>
    );
  }
}
